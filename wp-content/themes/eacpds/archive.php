<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package eacpds
 */

get_header();
?>


<main id="main" role="main" tabindex="-1">
    <?php get_template_part('template-parts/components/navigation/breadcrumbs'); ?>

    <?php
    while (have_posts()) :
        the_post();

        the_content();

    endwhile; // End of the loop.
    ?>

</main><!-- #main -->



<?php get_footer(); ?>
