<div class="w-full md:w-1/3 xl:w-1/4 p-10 md:p-10 xl:p-20 JS--career-card">
    <div class="bg-white p-20">
        <h3 class="font-title font-black text-h6 leading-11 mb-10 tracking-sm normal-case"><?php the_title(); ?></h3>
        <?php if (has_excerpt()) : ?>
            <div class="text-sm leading-18 tracking-sm">
                <?php echo get_the_excerpt(); ?>
            </div>
            <a href="<?php the_permalink(); ?>"
               class="c-btn--more pr-0 text-sm leading-175 tracking-sm uppercase text-blue font-bold relative"><?php _e('Read more', 'eacpds'); ?><?php echo get_file_icon_arrow_right('w-15 h-full absolute -right-30'); ?></a>
        <?php endif; ?>
    </div>
</div>