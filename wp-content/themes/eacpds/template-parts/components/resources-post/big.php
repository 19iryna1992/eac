<?php
$offsite_link = get_field('offsite_link');
$target_attr = $offsite_link ? '_blank' : '';
$post_url = $offsite_link ? $offsite_link : get_the_permalink();
$readTime = get_field('read_time');

$topics = wp_get_post_tags(get_the_ID(), ['fields' => 'names']);
$postAuthor = get_field('post_author');
?>

<div class="mb-40 lg:mb-0">
    <div class="relative">
        <a href="<?php echo $post_url; ?>" class="block max-h-550 overflow-hidden" target="<?php echo $target_attr; ?>">
            <?php if ($topics) : ?>
                <span class="max-w-150 lg:max-w-200 p-10 lg:px-20 lg:py-15 text-sm lg:text-md leading-11 font-title font-black trackung-sm bg-black text-white absolute top-20 left-20 inline-block"><?php echo $topics[0]; ?></span>
            <?php endif; ?>
            <?php the_post_thumbnail('full', ['class' => 'w-full object-cover']); ?>
        </a>
    </div>
    <div class="sm:-mt-40 sm:mx-20 py-10 sm:py-40 px-10 md:px-20 relative z-10 bg-white">
        <div class="mb-10 md:mb-20">
            <h3 class="mb-10 md:mb-20 text-sm md:text-base leading-11 font-title capitalize font-black tracking-sm"><a
                        href="<?php echo $post_url; ?>" target="<?php echo $target_attr; ?>"><?php the_title() ?></a>
            </h3>
            <div class="hidden md:block mb-20">
                <span class="text-md leading-12 font-roboto font-bold tracking-sm inline-block mr-20"><?php echo get_the_date('j F Y'); ?></span>
                <?php if ($readTime) : ?>
                    <span class="text-md leading-12 font-roboto font-bold tracking-sm inline-block text-blue-dark"><?php echo $readTime . ' '; ?><?php _e('read', 'eacpds'); ?></span>
                <?php endif; ?>
            </div>
            <?php if (has_excerpt()) : ?>
                <div class="text-xs-sm md:text-md leading-14 md:leading-18 font-roboto font-normal text-black-light tracking-sm">
                    <?php the_excerpt(); ?>
                </div>
            <?php endif; ?>
            <a href="<?php echo $post_url; ?>"
               class="c-btn--more hidden md:inline-block text-md pr-10 leading-18 tracking-sm uppercase text-blue font-bold relative"
               target="<?php echo $target_attr; ?>"><?php _e('Read More', 'eacpds'); ?><?php echo get_file_icon_arrow_right('w-15 h-full inline-block absolute -right-15'); ?></a>
        </div>

        <?php if ($postAuthor) : ?>
            <?php foreach ($postAuthor as $author) :
                $position = get_field('member_position', $author->ID);
                ?>
                <div class="flex items-center">
                    <?php if (has_post_thumbnail($author->ID)) : ?>
                        <?php echo get_the_post_thumbnail($author->ID, 'author-avatar', ['class' => 'rounded-full mr-20 max-w-40 md:max-w-80']); ?>
                    <?php endif; ?>

                    <div>
                        <h4 class="text-sm md:text-md leading-15 font-title font-black tracking-sm normal-case"><?php echo get_the_title($author->ID); ?></h4>
                        <?php if ($position) : ?>
                            <span class="text-xs-sm md:text-sm leading-15 font-roboto font-normal tracking-sm"><?php echo $position; ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
