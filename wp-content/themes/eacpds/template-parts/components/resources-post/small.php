<?php
$offsite_link = get_field('offsite_link');
$target_attr = $offsite_link ? '_blank' : '';
$post_url = $offsite_link ? $offsite_link : get_the_permalink();
$readTime = get_field('read_time');

$topics = wp_get_post_tags(get_the_ID(), ['fields' => 'names']);
$postAuthor = get_field('post_author');
?>

<div class="sm:flex -mx-10">
    <div class="relative px-10">
        <a href="<?php echo $post_url; ?>" class="min-w-220 block h-full overflow-hidden"
           target="<?php echo $target_attr; ?>">
            <?php if ($topics) : ?>
                <span class="max-w-150 md:max-w-120 text-sm md:text-extra-xs font-title font-black leading-12 tracking-sm bg-black text-white absolute top-20 left-20 inline-block p-10 md:p-5"><?php echo $topics[0]; ?></span>
            <?php endif; ?>
            <?php the_post_thumbnail('full', ['class' => 'w-full h-full object-cover']); ?>
        </a>
    </div>
    <div class="px-10 md:min-h-220 md:flex md:justify-around md:flex-col">
        <div class="mt-10">
            <h3 class="mb-10 text-sm md:text-md font-title font-black tracking-sm leading-10 normal-case"><a
                        href="<?php echo $post_url; ?>" target="<?php echo $target_attr; ?>"><?php the_title() ?></a>
            </h3>
            <div class="hidden md:block mb-10">
                <span class="text-xs-sm leading-12 font-roboto font-bold tracking-sm inline-block mr-10"><?php echo get_the_date('j F Y'); ?></span>
                <?php if ($readTime) : ?>
                    <span class="text-xs-sm leading-12 font-roboto font-bold tracking-sm text-blue-dark inline-block"><?php echo $readTime . ' '; ?><?php _e('read', 'eacpds'); ?></span>
                <?php endif; ?>
            </div>
            <?php if (has_excerpt()) : ?>
                <div class="mb-10 font-roboto text-xs-sm leading-14 md:leading-12 font-normal">
                    <?php the_excerpt(); ?>
                </div>
            <?php endif; ?>
            <a href="<?php echo $post_url; ?>"
               class="c-btn--more hidden md:inline-block text-xs-sm text-blue font-roboto pr-10 font-bold tracking-sm leading-15 uppercase relative"
               target="<?php echo $target_attr; ?>"><?php _e('Read More', 'eacpds'); ?><?php echo get_file_icon_arrow_right('w-15 h-full inline-block absolute -right-15'); ?></a>
        </div>
        <?php if ($postAuthor) : ?>
            <?php foreach ($postAuthor as $author) :
                $position = get_field('member_position', $author->ID);
                ?>
                <div class="flex items-center">
                    <?php if (has_post_thumbnail($author->ID)) : ?>
                        <?php echo get_the_post_thumbnail($author->ID, 'author-avatar', ['class' => 'max-w-40 mr-15 rounded-full']); ?>
                    <?php endif; ?>

                    <div>
                        <h4 class="text-sm leading-15 font-title font-black tracking-sm normal-case"><?php echo get_the_title($author->ID); ?></h4>
                        <?php if ($position) : ?>
                            <span class="text-xs-sm leading-15 font-roboto font-normal tracking-sm"><?php echo $position; ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
