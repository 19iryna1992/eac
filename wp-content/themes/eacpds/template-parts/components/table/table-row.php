<?php
$table_count_column = get_field('table_count_columns');

$type_cell_first = get_sub_field('type_cell_first', get_the_ID());
$type_cell_second = get_sub_field('type_cell_second', get_the_ID());
$type_cell_third = get_sub_field('type_cell_third', get_the_ID());
$type_cell_fourth = get_sub_field('type_cell_fourth', get_the_ID());
$type_cell_fifth = get_sub_field('type_cell_fifth', get_the_ID());


$cell_text_first = get_sub_field('cell_text_first', get_the_ID());
$cell_text_second = get_sub_field('cell_text_second', get_the_ID());
$cell_text_third= get_sub_field('cell_text_third', get_the_ID());
$cell_text_fourth = get_sub_field('cell_text_fourth', get_the_ID());
$cell_text_fifth = get_sub_field('cell_text_fifth', get_the_ID());

?>

    <td class="font-sm"> 
        <?php if ($type_cell_first == 'checked'): ?>
            <div >
                <?php echo get_check_icon_table('text-green w-20 h-15'); ?>
            </div>
        <?php elseif ($type_cell_first == 'text'): ?>
            <div>
                <?php if ($cell_text_first): ?>
                    <span> <?php echo $cell_text_first; ?></span>
                <?php endif; ?>
            </div>
        <?php elseif ($type_cell_first == 'empty'): ?>
            <div class="flex items-center justify-center">
                <span class="c-table-price__empty"></span>
            </div>
        <?php endif; ?>

    </td>
    <td>
        <?php if ($type_cell_second == 'checked'): ?>
            <div >
                <?php echo get_check_icon_table('text-green w-24 h-18'); ?>
            </div>
        <?php elseif ($type_cell_second == 'text'): ?>
            <div>
                <?php if ($cell_text_second): ?>
                    <span> <?php echo $cell_text_second; ?></span>
                <?php endif; ?>
            </div>
        <?php elseif ($type_cell_second == 'empty'): ?>
            <div class="flex items-center justify-center">
                <span class="c-table-price__empty"></span>
            </div>
        <?php endif; ?>
    </td>
    <td>
        <?php if ($type_cell_third == 'checked'): ?>
            <div >
                <?php echo get_check_icon_table('text-green w-24 h-18'); ?>
            </div>
        <?php elseif ($type_cell_third == 'text'): ?>
            <div>
                <?php if ($cell_text_third): ?>
                    <span> <?php echo $cell_text_third; ?></span>
                <?php endif; ?>
            </div>
        <?php elseif ($type_cell_third == 'empty'): ?>
            <div class="flex items-center justify-center">
                <span class="c-table-price__empty"></span>
            </div>
        <?php endif; ?>
    </td>
    <td>
        <?php if ($type_cell_fourth == 'checked'): ?>
            <div >
                <?php echo get_check_icon_table('text-green w-24 h-18'); ?>
            </div>
        <?php elseif ($type_cell_fourth == 'text'): ?>
            <div>
                <?php if ($cell_text_fourth): ?>
                    <span> <?php echo $cell_text_fourth; ?></span>
                <?php endif; ?>
            </div>
        <?php elseif ($type_cell_fourth == 'empty'): ?>
            <div class="flex items-center justify-center">
                <span class="c-table-price__empty"></span>
            </div>
        <?php endif; ?>
    </td>

<?php if ($table_count_column == 'five' ): ?>
    <td>
        <?php if ($type_cell_fifth == 'checked'): ?>
            <div >
                <?php echo get_check_icon_table('text-green w-24 h-18'); ?>
            </div>
        <?php elseif ($type_cell_fifth == 'text'): ?>
            <div>
                <?php if ($cell_text_fifth): ?>
                    <span> <?php echo $cell_text_fifth; ?></span>
                <?php endif; ?>
            </div>
        <?php elseif ($type_cell_fifth == 'empty'): ?>
            <div class="flex items-center justify-center">
                <span class="c-table-price__empty"></span>
            </div>
        <?php endif; ?>
    </td>
<?php endif; ?>

