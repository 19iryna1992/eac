<?php
$table_count_column = get_field('table_count_columns');

$type_cell_first = get_sub_field('type_cell_first', get_the_ID());
$type_cell_fourth = get_sub_field('type_cell_fourth', get_the_ID());


$cell_text_first = get_sub_field('cell_text_first', get_the_ID());
$cell_text_fourth = get_sub_field('cell_text_fourth', get_the_ID());

?>

    <td class="font-sm">
        <div> 
            <?php if ($type_cell_first == 'checked'): ?>
                <?php echo get_check_icon_table('text-green w-20 h-15'); ?>

            <?php elseif ($type_cell_first == 'text'): ?>
                <?php if ($cell_text_first): ?>
                    <span> <?php echo $cell_text_first; ?></span>
                <?php endif; ?>

            <?php elseif ($type_cell_first == 'empty'): ?>
                <div class="flex items-center justify-center">
                    <span class="c-table-price__empty"></span>
                </div>
            <?php endif; ?> 
        </div>
    </td>
     
     
    <td>
        <div class="py-20 sm:w-200 mx-auto">
            <?php if ($type_cell_fourth == 'checked'): ?>
                <?php echo get_check_icon_table('text-green w-24 h-18'); ?>

            <?php elseif ($type_cell_fourth == 'text'): ?>
                <?php if ($cell_text_fourth): ?>
                    <span> <?php echo $cell_text_fourth; ?></span>
                <?php endif; ?>

            <?php elseif ($type_cell_fourth == 'empty'): ?>
                <div class="flex items-center justify-center">
                    <span class="c-table-price__empty"></span>
                </div>
            <?php endif; ?>
        </div>
    </td> 

