<?php
$table_count_column = get_field('table_count_columns');

$type_cell_first = get_sub_field('type_cell_first', get_the_ID());
$type_cell_fifth = get_sub_field('type_cell_fifth', get_the_ID());


$cell_text_first = get_sub_field('cell_text_first', get_the_ID());
$cell_text_fifth = get_sub_field('cell_text_fifth', get_the_ID());

?>

<?php if ($table_count_column == 'five' ): ?>
    <td class="font-sm"> 
        <div>
            <?php if ($type_cell_first == 'checked'): ?>
                <?php echo get_check_icon_table('text-blue w-24 h-18'); ?>

            <?php elseif ($type_cell_first == 'text'): ?>

                <?php if ($cell_text_first): ?>
                    <span> <?php echo $cell_text_first; ?></span>
                <?php endif; ?>
                
            <?php elseif ($type_cell_first == 'empty'): ?>
                <div class="flex items-center justify-center">
                    <span class="c-table-price__empty"></span>
                </div>
            <?php endif; ?>
        </div>
    </td> 

    <td>
        <?php if ($type_cell_fifth == 'checked'): ?>
            <div class="py-20 sm:w-200 mx-auto">
                <?php echo get_check_icon_table('text-green w-24 h-18'); ?>
            </div>
        <?php elseif ($type_cell_fifth == 'text'): ?>
            <div>
                <?php if ($cell_text_fifth): ?>
                    <span> <?php echo $cell_text_fifth; ?></span>
                <?php endif; ?>
            </div>
        <?php elseif ($type_cell_fifth == 'empty'): ?>
            <div class="flex items-center justify-center">
                <span class="c-table-price__empty"></span>
            </div>
        <?php endif; ?>
    </td>
<?php endif; ?>

