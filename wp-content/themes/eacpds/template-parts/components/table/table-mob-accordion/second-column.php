<?php if (have_rows('table_accordion')): ?>
    <?php while (have_rows('table_accordion')) :
        the_row();
        $accordion_label = get_sub_field('table_accordion_btn_label');
        $price_col_second = get_sub_field('price_accordion_table_second');
        $table_btn_second = get_sub_field('table_btn_second');
        ?>

        <div class="c-accordion-table-price__wpapper JS-accordion-table-wpapper my-5">
            <table class="c-table-price c-table-price--accordion JS-accordion-table-tab w-full">
                <tbody>
                <tr class="c-table-price__tr-header JS-accordion-table-header cursor-pointer">
                    <th class="">
                        <div>
                            <?php if ($accordion_label): ?>
                                <?php echo $accordion_label ?>
                            <?php endif; ?>
                        </div>
                    </th>
                    <th class="relative w-1/12"><span
                                class="c-accordion-table__btn-toggle block absolute"></span></th>
                </tr>

                <?php if (have_rows('table_accordion_row')): ?>
                    <tr>
                        <th class="c-table-price__th-header" colspan="2">
                            <div>
                                <?php if ($price_col_second): ?>
                                    <?php echo $price_col_second ?>
                                <?php endif; ?>
                            </div>
                        </th>
                    </tr>

                    <?php while (have_rows('table_accordion_row')): the_row(); ?>
                        <tr>
                            <?php get_template_part('template-parts/components/table/table-row-mobile/second') ?>
                        </tr>
                    <?php endwhile; ?>

                    <tr class="c-table-price__tr-btn bg-white">
                        <td colspan="2">
                            <?php if ($table_btn_second): ?>
                                <a class="c-btn c-btn--ex-xs c-btn--secondary"
                                   href="<?php echo $table_btn_second['url'] ?>">
                                    <?php echo $table_btn_second['title'] ?>
                                </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    <?php endwhile; ?>
<?php endif; ?>