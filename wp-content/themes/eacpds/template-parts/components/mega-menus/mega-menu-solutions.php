<?php if( have_rows('solutions_menu', 'option') ): ?>
    <div class="c-text-menu laptop:block laptop:fixed">
        <div class="c-text-menu__close JS-close-sub-menu"></div>
    <?php while( have_rows('solutions_menu', 'option') ): the_row();
        $text = get_sub_field('solutions_primary_menu_item', 'option');
        ?>
        <?php if( have_rows('solutions_sub_menu') ): ?>
            <div class="c-text-menu__item has-child">
                <a href="<?php echo $text['url']; ?>" class="c-text-menu__link" target="<?php echo $text['target']; ?>">
                    <span><?php echo $text['title']; ?></span>
                    <span class="JS-sub-menu-toggle c-text-menu__toggle inline-block"></span>
                </a>                
                <div class="c-text-menu__submenu">
                
                <?php while( have_rows('solutions_sub_menu') ): the_row();
                    $sub_text = get_sub_field('menu_item');
                    ?>
                    <?php if( $sub_text ): ?>
                        <div class="c-text-menu__submenu-item">
                            <a href="<?php echo $sub_text['url']; ?>" class="c-text-menu__submenu-link" target="<?php echo $sub_text['target']; ?>">
                                <span><?php echo $sub_text['title']; ?></span>
                            </a>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
                
                </div>    
            </div>
        <?php else: ?> 
            <div class="c-text-menu__item">
                <a href="<?php echo $text['url']; ?>" class="c-text-menu__link" target="<?php echo $text['target']; ?>">
                    <span><?php echo $text['title']; ?></span>
                    <span class="JS-sub-menu-toggle c-text-menu__toggle inline-block"></span>
                </a>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
        <div class="c-text-menu__bg">
            
        </div>   
    </div>
<?php endif; ?>