<?php if( have_rows('resources_sub_menu', 'option') ): ?>
    <div class="c-resources-menu laptop:block laptop:fixed">
    <?php while( have_rows('resources_sub_menu', 'option') ): the_row(); 
        $image = get_sub_field('menu_item_img', 'option');
        $text = get_sub_field('menu_item', 'option');
        ?>
        <div class="c-resources-menu__item">
            <a href="<?php echo $text['url']; ?>" class="c-resources-menu__link" target="<?php echo $text['target']; ?>">
                <?php if( $image ): ?>
                    <?php echo wp_get_attachment_image( $image, 'resources-menu-img' ); ?>
                <?php endif; ?>
                <?php if( $text ): ?>
                    <span><?php echo $text['title']; ?></span>
                <?php endif; ?>
            </a>
        </div>
    <?php endwhile; ?>
    <div class="c-resources-menu__close JS-close-resources-menu"></div>
    </div>
<?php endif; ?>
