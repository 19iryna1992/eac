<?php if( have_rows('about_sub_menu', 'option') ): ?>
    <div class="c-about-menu laptop:block laptop:fixed">
    <?php while( have_rows('about_sub_menu', 'option') ): the_row(); 
        $image = get_sub_field('menu_item_img', 'option');
        $text = get_sub_field('menu_item', 'option');
        ?>
        <div class="c-about-menu__item">
            <a href="<?php echo $text['url']; ?>" class="c-about-menu__link" target="<?php echo $text['target']; ?>">
                <?php if( $image ): ?>
                    <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                <?php endif; ?>
                <?php if( $text ): ?>
                    <span><?php echo $text['title']; ?></span>
                <?php endif; ?>
            </a>
        </div>
    <?php endwhile; ?>
    <div class="c-about-menu__close JS-close-about-menu"></div>
    </div>
<?php endif; ?>

