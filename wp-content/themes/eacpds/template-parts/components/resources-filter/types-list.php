<?php

$types = get_terms([
    'taxonomy' => 'category',
    'exclude' => 1,
])

?>

<h5 class="text-md font-roboto mb-20 font-bold leading-11 uppercase"><?php _e('Type', 'eacpds'); ?></h5>

<ul class="list-none p-0 m-0 c-resources-filter__list">
    <?php foreach ($types as $type) :
        $icon = get_field('cat_img', $type);
        ?>
        <li class="">
            <label for="<?php echo $type->term_id; ?>" class="relative block">
                <?php if ($icon) : ?>
                    <span class="bg-blue absolute p-8 mb-3 top-1/2 transform -translate-y-1/2"
                          style="width: 30px;height: 30px;"><?php echo wp_get_attachment_image($icon['ID'], 'full', false, ['class' => 'm-auto']); ?></span>
                <?php endif; ?>
                <input type="checkbox" name="category_name[]" value="<?php echo $type->slug; ?>" class="invisible absolute opacity-0"
                       id="<?php echo $type->term_id; ?>"  <?php checked(isset($_GET['category_name']) && in_array($type->slug,$_GET['category_name'])) ?> >
                <span class="text-sm leading-11 ml-50 font-bold font-roboto underline block cursor-pointer py-10 px-20"><?php echo $type->name; ?></span>
            </label>
        </li>
    <?php endforeach; ?>
</ul>
