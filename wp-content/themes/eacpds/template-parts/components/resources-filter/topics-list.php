<?php

$topics = get_terms([
    'taxonomy' => 'post_tag',
    'exclude' => 1,
])

?>

<h5 class="text-md font-roboto mb-20 font-bold leading-11 uppercase"><?php _e('Type', 'eacpds'); ?></h5>

<ul class="list-none p-0 m-0 c-resources-filter__list xl:flex xl:max-h-180 xl:flex-col xl:flex-wrap xl:mr-100 xl:w-540">
    <?php foreach ($topics as $topic) : ?>
        <li class="xl:inline-block xl:mr-50">
            <label for="<?php echo $topic->term_id; ?>" class="block relative -mx-10">
                <input type="checkbox" name="tag__in[]" value="<?php echo $topic->term_id; ?>" class="invisible absolute opacity-0"
                       id="<?php echo $topic->term_id; ?>"  <?php checked(isset($_GET['tag__in']) && in_array($topic->term_id,$_GET['tag__in'])) ?> >
                <span class="text-sm leading-11 font-bold font-roboto underline block cursor-pointer py-10 px-10"><?php echo $topic->name; ?></span>
            </label>
        </li>
    <?php endforeach; ?>
</ul>
