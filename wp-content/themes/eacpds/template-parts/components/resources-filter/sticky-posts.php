<?php

$sticky_args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 1,
    'post__in' => get_option('sticky_posts'),
    'ignore_sticky_posts' => 1,
    'no_found_rows' => true
);


$big_sticky = new WP_Query($sticky_args);
$small_sticky = new WP_Query($args = array_merge($sticky_args, array('offset' => 1, 'posts_per_page' => 4)));

?>

<?php if ($big_sticky->have_posts() && !is_paged()) : ?>
    <div class="container">
        <div class="lg:flex -mx-10">
            <div class="w-full xl:w-3/5 px-10">
                <?php while ($big_sticky->have_posts()) : $big_sticky->the_post(); ?>
                    <?php get_template_part('template-parts/components/resources-post/big'); ?>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>
            <?php if ($small_sticky->have_posts()) : ?>
                <div class="w-full xl:w-2/5 px-10">
                    <?php while ($small_sticky->have_posts()) : $small_sticky->the_post(); ?>

                        <div class="mb-40 md:mb-20 last:mb-0">
                            <?php get_template_part('template-parts/components/resources-post/small'); ?>
                        </div>

                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
