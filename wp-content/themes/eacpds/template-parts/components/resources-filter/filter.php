<div class="py-20 md:py-45 c-resources-filter">

    <div class="container relative">
        <div class="JS-blog-filter">
            <h3 class="JS-blog-filter-heading text-md font-roboto mb-20 font-bold leading-11 uppercase inline-block relative"><?php _e('Filter By:', 'eacpds'); ?><?php echo get_check_icon('absolute text-blue -right-30 top-1/3 -translate-y-1/2'); ?></h3>
            <form action="<?php echo get_permalink(get_option('page_for_posts')); ?>" method="GET" class="JS-blog-form">
                <div class="mb-20 md:px-50 md:py-30 bg-grey-light">
                    <div class="md:flex">
                        <div class="mb-30 md:mb-0 md:mr-100 last:mr-0">
                            <?php get_template_part('template-parts/components/resources-filter/types-list') ?>
                        </div>
                        <div class="md:mr-100 last:mr-0">
                            <?php get_template_part('template-parts/components/resources-filter/topics-list') ?>
                        </div>
                    </div>
                </div>
                <div class="text-center md:text-left md:flex justify-between items-center flex-row md:mx-60">
                    <button type="submit"
                            class="c-btn c-btn--xs c-btn--primary inline-block py-8 px-30 text-center bg-blue text-white appearance-none outline-none"><?php _e('Apply', 'eacpds'); ?></button>
                    <div class="mb-20 md:mb-0">
                        <?php if ((isset($_GET['category_name']) && $_GET['category_name']) || (isset($_GET['tag__in']) && $_GET['tag__in'])) : ?>
                            <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>"
                               class="my-20 md:my-0 md:mr-60 block md:inline-block font-roboto font-medium text-xs-sm leading-11 uppercase underline hover:no-underline transition-all"><?php _e('Clear filters', 'eacpds'); ?></a>
                        <?php endif; ?>
                        <?php get_template_part('template-parts/components/resources-filter/sort-filter') ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>