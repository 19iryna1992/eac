<label for="orderby" class="font-roboyo font-bold text-sm leading-11 uppercase"><?php _e('Sort By:', 'eacpds'); ?></label>
<select name="orderby" class="font-roboto text-sm font-medium leading-11" id="orderby">
    <option value="date" <?php selected($_GET['orderby'], 'date'); ?> >
        <?php _e('Most recent', 'eacpds'); ?>
    </option>
    <option value="oldest" <?php selected($_GET['orderby'], 'oldest'); ?> >
        <?php _e('Oldest', 'eacpds'); ?>
    </option>
    <option value="title" <?php selected($_GET['orderby'], 'title'); ?> >
        <?php _e('By title', 'eacpds'); ?>
    </option>
</select>

<input type="hidden" name="order" id="order" value="<?php echo (isset($_GET['order']) && $_GET['order'] == 'ASC') ? 'ASC' : 'DESC'; ?>">