<?php

if (!defined('ABSPATH')) exit;

$menuBtn = get_field('menu_btn', 'option');
?>

<div class="JS-main-menu c-main-menu absolute w-screen laptop:w-auto laptop:static">
    <div class="c-main-menu__wrap">
        <?php wp_nav_menu([
            'theme_location' => 'primary-menu',
            'menu_id' => 'primary-menu',
            'container' => 'nav',
            'container_class' => 'c-main-menu__container',
            'menu_class' => 'c-main-menu__list flex laptop:items-center laptop:justify-center'
        ]); ?>
        <div class="menu-item laptop:hidden o-header__contact-btn">
            <?php if ($menuBtn) : ?>
                <a href="<?php echo $menuBtn['url']; ?>"
                    class=""
                    target="<?php echo $menuBtn['target']; ?>"><?php echo $menuBtn['title']; ?>
                    <span class="menu-item__icon JS--submenu-open"></span>
                </a>
            <?php endif; ?>
        </div>  
    </div>
</div>