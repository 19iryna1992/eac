<?php if (function_exists('yoast_breadcrumb')) : ?>
    <div class="bg-black py-16">
        <div class="container">
            <?php yoast_breadcrumb('<div id="breadcrumbs" class="c-breadcrumbs text-white text-xs-sm tracking-sm leading-13">', '</div>'); ?>
        </div>
    </div>


<?php endif; ?>