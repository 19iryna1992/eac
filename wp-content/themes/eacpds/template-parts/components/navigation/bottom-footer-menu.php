<div class="c-bottom-footer-menu">
    <?php wp_nav_menu([
        'theme_location' => 'bottom-footer-menu',
        'menu_id' => 'bottom-footer-menu',
        'container' => 'nav',
        'container_class' => 'c-bottom-footer-menu__container',
        'menu_class' => 'c-bottom-footer-menu__list flex flex-wrap m-0 p-0 list-none'
    ]); ?>
</div>