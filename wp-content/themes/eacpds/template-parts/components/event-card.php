<?php
//global $post;
$event_date = get_field('event_date', get_the_ID());
$event_url = get_field('event_url', get_the_ID());
$date = new DateTime($event_date);

?>
<div class="flex mobile-md:flex-wrap w-full py-30">
    <div class="w-full md:w-5/12 lg:w-4/12 md:pr-15 lg:pr-30 mobile-md:mb-20 text-center">
        <div class="inline-block relative h-180 md:h-240 lg:h-260 max-h-260 w-full h-full">
            <?php if ($event_date): ?>
                <div class="absolute left-0 mobile-md:top-0 md:bottom-0 w-80 md:w-140 h-80 md:h-full flex flex-col items-start md:items-center justify-center bg-blue-dark py-10 md:py-25 px-10 md:px-15">
                    <span class="font-title font-black text-h3 md:text-h2-md tracking-sm leading-11 text-white"><?php echo $date->format('j'); ?></span>
                    <span class="font-roboto font-bold text-extra-xs md:text-sm md:text-md tracking-sm leading-16 text-white"><?php echo $date->format('F'); ?></span>
                </div>
            <?php endif ?>
            <?php echo get_the_post_thumbnail(null, 'post-tab', array('class' => 'object-cover object-center h-full w-full wp-post-image')) ?>
        </div>
    </div>
    <div class="w-full md:w-7/12 lg:w-8/12 text-left pt-10">
        <div class="flex flex-col laptop:flex-row">
            <div class="lg:mr-30 xl:mr-56">
                <h3 class="font-title font-black mb-20 text-base lg:text-h3 text-black leading-14 tracking-sm">
                    <?php the_title(); ?>
                </h3>
                <div class="c-wysiwyg font-roboto leading-20 text-black-light">
                    <?php the_content(); ?>
                </div>
            </div>
            <?php if ($event_url): ?>
                <div class="flex sm:justify-start laptop:justify-center items-center flex-wrap">
                    <a class="c-btn c-btn--xs c-btn--more c-btn--primary xl:py-30 mobile-md:mt-20 " target="_blank"
                       href="<?php echo $event_url ?>">
                        <?php _e('Register', 'eacpds') ?>
                        <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                    </a>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>