<?php


$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

//

$postAuthor = get_field('post_author', get_the_ID());

// Custom author description
$authorDescription = get_field('post_author_description');
?>


<div class="acf-post-author relative pt-50 md:pt-80 pb-15 <?php echo $blockClass; ?> "
     id="<?php echo esc_attr($id); ?>">
    <div class="container flex justify-center">
        <?php if ($postAuthor) : ?>
            <?php foreach ($postAuthor as $author) :
                $position = get_field('member_position', $author->ID);
                ?>
                <div class="lg:w-11/12">
                    <div class="flex flex-wrap -mx-15">
                        <div class="w-full lg:w-5/12 p-15 flex mobile-md:flex-wrap column items-center">
                            <?php if (has_post_thumbnail($author->ID)) : ?>
                                <div class="rounded-full overflow-hidden mr-20 max-w-80 lg:max-w-120 mobile-md:my-10">
                                    <?php echo get_the_post_thumbnail($author->ID, 'author-avatar', ['class' => 'w-full']); ?>
                                </div>
                            <?php endif; ?> 
                            
                            <div class="">
                                <h3 class="block font-title font-bold text-h3 leading-15 tracking-sm normal-case color-black mb-20"><?php echo get_the_title($author->ID); ?></h3>

                                <?php if ($position) : ?>
                                    <span class="block font-roboto font-normal text-base leading-15 tracking-sm color-black"><?php echo $position; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="w-full lg:w-7/12 p-15">
                            <div class="text-sm leading-18 font-normal color-black-light">
                                <?php if ($authorDescription) : ?>
                                    <?php echo $authorDescription; ?>
                                <?php else : ?>
                                    <?php echo apply_filters('the_content', get_the_content(null, null, $author->ID)); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>



