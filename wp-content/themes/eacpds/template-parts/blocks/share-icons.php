<?php


$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

//ACF vars
$section_title = get_field('social_icons_title');

?>

<?php /*if (is_singular('post')): */?>
    <div class="acf-share-icons relative pt-30 pb-50 md:pb-100 <?php echo $blockClass; ?>"
         id="<?php echo esc_attr($id); ?>">
        <div class="container flex justify-center">
            <div class="w-full lg:w-11/12">
                <div class="c-share-icons flex flex-wrap items-center">
                    <h3 class="block mr-20 my-15 md:mr-40 font-title font-bold text-h3 leading-15 tracking-sm normal-case color-black"><?php echo ($section_title) ? $section_title : __('Share this','eacpds'); ?></h3>
                    
                    <div class="a2a_kit a2a_kit_size_80 a2a_default_style">
                        <a class="a2a_button_facebook mr-20 md:mr-40"></a>
                        <a class="a2a_button_twitter mr-20 md:mr-40"></a>
                        <a class="a2a_button_linkedin"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php /*endif; */?>