<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('title');
?>

<div class="acf-logos <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="md:flex">
            <?php if ($title) : ?>
                <div class="md:w-1/2 mb-20 md:mb-0">
                    <h2 class="font-title font-black text-h5 tracking-sm md:text-h3-2lg lg:text-h2 leading-12 lg:leading-15 normal-case"><?php echo $title; ?></h2>
                </div>
            <?php endif; ?>
            <?php if (have_rows('logos')): ?>
                <div class="md:w-1/2 flex justify-center items-center flex-wrap">
                    <?php while (have_rows('logos')): the_row();
                        $logo = get_sub_field('logo');
                        $url = get_sub_field('url');
                        ?>
                        <?php if ($logo) : ?>
                            <div class="w-2/4 md:w-1/3 px-20 xl:px-30 py-15">
                                <?php echo ($url) ? '<a href="' . $url['url'] . '" target="' . $url['target'] . '">' : null; ?>
                                <?php echo wp_get_attachment_image($logo, 'full', false, ['class' => 'm-auto']) ?>
                                <?php echo ($url) ? '</a>' : null; ?>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>