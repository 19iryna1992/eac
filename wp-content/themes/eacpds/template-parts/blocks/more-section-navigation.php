<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('title');
?>

<div class="acf-more-section-navigation bg-blue-alice py-30 lg:py-45 <?php echo $blockClass; ?>"
     id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="text-h3 font-title font-black leading-175 tracking-sm text-black uppercase mb-40 md:mb-60"><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if (have_rows('nav_blocks')): ?>
            <div class="flex mobile-lg:justify-center flex-wrap -mx-15 xl:-mx-20">
                <?php while (have_rows('nav_blocks')): the_row();
                    $image = get_sub_field('image');
                    $url = get_sub_field('url');
                    ?>
                    <?php if ($image) : ?>
                        <div class="w-full sm:w-1/2 lg:w-1/4 px-15 xl:px-20  my-15 xl:my-20">
                            <?php echo ($url) ? '<a class="block" href="' . $url['url'] . '" target="' . $url['target'] . '">' : null; ?>
                                <div class="w-full h-240 p-20 flex justify-center items-center bg-blue-dark " >
                                    <?php echo wp_get_attachment_image($image, 'logo-nav') ?>
                                </div>
                            <?php echo ($url) ? '</a>' : null; ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>