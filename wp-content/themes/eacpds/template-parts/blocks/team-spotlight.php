<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('teams_title');
$teams = get_field('teams');

//$btn = get_field('teams_link'); field commented also in Block Class
?>

<div class="acf-team-spotlight pb-40 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="text-h3 font-title font-black leading-12 md:leading-175 tracking-sm uppercase mb-16 md:mb-60"><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if ($teams) : ?>
            <div class="flex flex-wrap -mx-20 xl:-mx-30">
                <?php foreach ($teams as $team) :
                    $position = get_field('member_position', $team->ID);
                    $linkedin = get_field('member_linkedin', $team->ID);
                    $twitter = get_field('member_twitter', $team->ID);
                    ?>
                    <div class="md:w-1/2 lg:w-1/3 mb-40 md:mb-60 px-20 xl:px-30"> 
                        <?php if (has_post_thumbnail($team->ID)) : ?>
                            <?php echo get_the_post_thumbnail($team->ID, 'team-member', ['class' => 'mx-auto']); ?>
                        <?php endif; ?>
                        <div>
                            <div class="md:relative mt-15 md:-mt-40 mb-20 md:mb-10 bg-white md:mx-10 md:p-20">
                                <h5 class="font-title font-black text-md md:text-base tracking-sm leading-12"><?php echo get_the_title($team->ID); ?></h5>
                                <?php if ($position) : ?>
                                    <span class="block mt-10 font-default font-normal text-sm md:text-md leading-15 tracking-sm"><?php echo $position; ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="font-default leading-16 md:leading-18 font-normal text-sm md:text-md tracking-sm">
                                <?php echo apply_filters('the_content', get_the_content(null, null, $team->ID)); ?>
                            </div>
                            <ul class="flex mt-20 md:mt-30 m-0 p-0 list-none">
                                <?php if ($linkedin) : ?>
                                    <li class="mr-20">
                                        <a href="<?php echo $linkedin; ?>" target="_blank" class="text-blue-light transition-opacity duration-02 hover:opacity-80 block"><?php echo get_linkedin_icon(); ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($twitter) : ?>
                                    <li>
                                        <a href="<?php echo $twitter; ?>" target="_blank" class="text-blue-lighter transition-opacity duration-02 hover:opacity-80 block"><?php echo get_twitter_icon(); ?></a>
                                    </li>
                                <?php endif; ?>
                            </ul> 
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

       <!-- <div class="text-center mt-20 md:mt-40 mb-40 md:mb-60">
            <a href="#" class="c-btn-outline c-btn-outline--lg c-btn-outline--primary">{{Show more}}</a>
        </div>-->
    </div>
</div>