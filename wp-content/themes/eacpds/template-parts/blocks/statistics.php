<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$variations = get_field('statistic_variations');

$main_title = get_field('title');
$main_description = get_field('statistics_description');
?>

<div class="acf-statistics overflow-hidden py-60 xl:py-100 <?php echo $blockClass; ?>"
     id="<?php echo esc_attr($id); ?>">
    <div class="container">

        <?php if ($main_title || $main_description) : ?>
            <div class="mb-60 lg:mb-100">
                <?php if ($main_title) : ?>
                    <h2 class="font-title uppercase font-black tracking-sm text-h3 leading-12 md:leading-175 mb-20 md:mb-40"><?php echo $main_title; ?></h2>
                <?php endif; ?>
                <?php if ($main_description) : ?>
                    <div class="text-md font-normal leading-16 lg:leading-20 md:text-base text-black-light">
                        <?php echo $main_description; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if ($variations === 'large'): ?>
            <?php
            $statisticCounter = 1;
            if (have_rows('blocks')): ?>
                <div>
                    <?php while (have_rows('blocks')): the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('statistics_content'); 
                        $bgNumberCounter = $statisticCounter < 10 ? '' . $statisticCounter : $statisticCounter;
                        ?>
                        <div class="flex md:w-11/12 m-auto mb-40 md:mb-60 xl:mb-100 overflow-hidden sm:overflow-visible last:mb-0 <?php echo ($statisticCounter % 2 == 0) ? 'justify-end' : null; ?>">
                            <div class="sm:min-h-280 lg:pl-100 md:w-2/3 xl:w-1/2 relative z-10">
                                <?php if ($title) : ?>
                                    <h3 class="mb-20 lg:mb-40 font-title tracking-sm font-bold uppercase text-base leading-11 md:leading-175 md:text-h3 relative z-10"><?php echo $title; ?></h3>
                                <?php endif; ?>
                                <?php if ($description) : ?>
                                    <div class="min-h-200 text-sm font-normal leading-20 relative z-10 text-black-light <?php echo ($statisticCounter % 2 == 0) ? 'md:w-11/12' : null; ?>">
                                        <?php echo $description; ?>
                                    </div>
                                <?php endif; ?>
                                <span class="c-large-symbols font-normal left-0 top-20 sm:top-0 lg:-top-30 absolute text-h1-lg lg:text-h1-xl font-title leading-9 z-0">
                                    <p class="JS-counter JS-counter-step"
                                       data-counter-end="<?php echo $statisticCounter; ?>"><?php echo $bgNumberCounter; ?></p>
                                </span>
                            </div>
                        </div>
                        <?php $statisticCounter++; endwhile; ?>
                </div>
            <?php endif; ?>


        <?php elseif ($variations === 'columns') : ?>

            <?php if (have_rows('blocks')): ?>
                <div class="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-40 sm:gap-45 lg:gap-76">
                    <?php while (have_rows('blocks')): the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('statistics_content');
                        $number = get_sub_field('number');
                        $BgBeforeNumbSymbol = get_sub_field('bg_before_number_symbol');
                        $BgNumber = get_sub_field('bg_number');
                        $BgAfterNumbSymbol = get_sub_field('bg_after_number_symbol');
                        ?>
                        <div>
                            <?php if ($number) : ?>
                                <p class="text-h3-xl md:text-h2-lg leading-14 md:leading-9 text-blue font-title"><?php echo $number; ?></p>
                            <?php endif; ?>
                            <div class="min-h-200 relative z-10 overflow-hidden">
                                <?php if ($title) : ?>
                                    <h4 class="font-title font-black tracking-sm text-h6 md:text-base leading-20 normal-case"><?php echo $title; ?></h4>
                                <?php endif; ?>
                                <?php if ($description) : ?>
                                    <div class="text-sm font-normal leading-20 text-black-light">
                                        <?php echo $description; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($BgNumber) : ?>
                                    <span class="c-large-symbols font-normal absolute text-h1-md leading-font-blacktle top-0 z-0">
                                        <?php if ($BgBeforeNumbSymbol) : ?>
                                            <span> <?php echo $BgBeforeNumbSymbol ?> </span>
                                        <?php endif; ?>

                                        <p class="JS-counter" data-counter-end="<?php echo $BgNumber; ?>"><?php echo $BgNumber; ?></p>

                                        <?php if ($BgAfterNumbSymbol) : ?>
                                            <span> <?php echo $BgAfterNumbSymbol ?> </span>
                                        <?php endif; ?>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

        <?php elseif ($variations === 'rows') : ?>
            <?php if (have_rows('blocks')): ?>
                <div class="md:w-11/12 lg:w-3/5 m-auto -mt-40">
                    <?php while (have_rows('blocks')): the_row();
                        $title = get_sub_field('title');
                        $description = get_sub_field('statistics_content');
                        $number = get_sub_field('number');
                        $BgBeforeNumbSymbol = get_sub_field('bg_before_number_symbol');
                        $BgNumber = get_sub_field('bg_number');
                        $BgAfterNumbSymbol = get_sub_field('bg_after_number_symbol');
                        ?>

                        <div class="mb-60 pt-40 relative z-10 md:flex md:items-center md:justify-between min-h-220">
                            <?php if ($number) : ?>
                                <span class="mr-30 font-bold tracking-sm text-h3-xl leading-14 md:text-h2-lg md:leading-9 text-blue font-title"><?php echo $number; ?></span>
                            <?php endif; ?>
                            <div class="flex-1 relative">
                                <?php if ($title) : ?>
                                    <h4 class="font-title tracking-sm text-h5 leading-12 font-bold normal-case"><?php echo $title; ?></h4>
                                <?php endif; ?>
                                <?php if ($description) : ?>
                                    <div class="text-sm font-normal leading-20 text-black-light">
                                        <?php echo $description; ?>
                                    </div>
                                <?php endif; ?>
                                <span class="w-4/5 absolute border-b border-black-light border-opacity-20 -bottom-50 left-0 block"></span>
                            </div>
                            <?php if ($BgNumber) : ?>
                                <span class="c-large-symbols absolute left-0 top-0 xl:top-1/2 xl:transform xl:-translate-y-2/4 z-0 flex font-normal text-h1-md leading-9 font-title">
                                    <?php if ($BgBeforeNumbSymbol) : ?>
                                        <span> <?php echo $BgBeforeNumbSymbol ?> </span>
                                    <?php endif; ?>

                                    <p class="JS-counter"
                                       data-counter-end="<?php echo $BgNumber; ?>"><?php echo $BgNumber; ?></p>
                                    <?php if ($BgAfterNumbSymbol) : ?>
                                        <span> <?php echo $BgAfterNumbSymbol ?> </span>
                                    <?php endif; ?>
                                </span>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
