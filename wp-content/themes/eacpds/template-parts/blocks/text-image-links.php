<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$variation = get_field('cards_variation');
$sectionTitle = get_field('title');
?>

<div class="acf-text-image-links <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($variation === 'overlap_card_style') : ?>
            <?php if ($sectionTitle) : ?>
                <h2 class="font-title text-h3 md:text-h2 md:text-center uppercase leading-11 mb-40 md:mb-60"><?php echo $sectionTitle; ?></h2>
            <?php endif; ?>

            <?php if (have_rows('blocks')): ?>
                <div class="JS-cards-slider lg:flex lg:flex-wrap -mx-20 xl:-mx-15 xl:-mx-40">
                    <?php while (have_rows('blocks')): the_row();
                        $image = get_sub_field('image');
                        $title = get_sub_field('title');
                        $subtitle = get_sub_field('subtitle');
                        $content = get_sub_field('collapses_content');
                        $btn = get_sub_field('button');
                        ?>
                        <div class="flex flex-col justify-center xl:px-40 mb-20 lg:w-1/3 px-20">
                            <?php if ($image) : ?>
                                <div class="h-330 lg:h-400 overflow-hidden bg-blue-alice">
                                    <?php echo wp_get_attachment_image($image, 'card-overlap-image', false, ['class' => 'object-cover w-full h-full']); ?>
                                </div>
                            <?php endif; ?>
                            <div class="flex-grow bg-white relative lg:mx-20 lg:-mt-40 p-15">
                                <?php if ($title) : ?>
                                    <h5 class="text-center font-title text-sm md:text-p-sm md:text-md leading-11 font-bold mb-30"><?php echo $title; ?></h5>
                                <?php endif; ?>
                                <?php if ($subtitle) : ?>
                                    <h6 class="mb-20 font-title leading-12 text-xs-sm md:text-sm md:leading-18 text-blue font-black"><?php echo $subtitle; ?></h6>
                                <?php endif; ?>
                                <?php if ($content) : ?>
                                    <div class="mb-20 text-xs-sm leading-16 md:text-sm md:leading-18 font-normal">
                                        <?php echo $content; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php if ($btn) : ?>
                                <div class="text-center">
                                    <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--xs c-btn--primary"
                                       target="<?php echo $btn['target']; ?>"><?php echo $btn['title']; ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>

                </div>

            <?php endif; ?>


        <?php elseif ($variation === 'related_documents_style'): ?>
            <?php if ($sectionTitle) : ?>
                <h2 class="mb-40 md:mb-60 font-title text-bold text-h3 leading-10 md:leading-175 uppercase font-black"><?php echo $sectionTitle; ?></h2>
            <?php endif; ?>
            <?php if (have_rows('blocks')): ?>
                <div class="flex flex-wrap -mx-20 justify-center">

                    <?php while (have_rows('blocks')): the_row();
                        $image = get_sub_field('image');
                        $title = get_sub_field('title');
                        $subtitle = get_sub_field('subtitle');
                        $content = get_sub_field('collapses_content');
                        $btn = get_sub_field('button');
                        ?>
                        <div class="mb-20 w-full md:w-1/2 lg:w-1/3 flex flex-col justify-center px-20">
                            <?php if ($image) : ?>
                                <div class="max-h-300 lg:max-h-400 h-full overflow-hidden flex justify-center items-start bg-blue-alice py-30 md:py-40 px-40">
                                    <?php echo wp_get_attachment_image($image, 'middle-square', false, ['class' => 'object-cover']); ?>
                                </div>
                            <?php endif; ?>
                            <div class="mx-10 bg-white px-15 pt-20 md:py-35 -mt-20 md:-mt-35 flex-grow flex flex-col justify-center">
                                <?php if ($title) : ?>
                                    <h5 class="text-center font-title text-h6 leading-12 mb-30 md:mb-40 flex-grow font-bold"><?php echo $title; ?></h5>
                                <?php endif; ?>
                                <?php if ($subtitle) : ?>
                                    <h6 class="mb-20 font-title leading-12 text-xs-sm md:text-sm md:leading-18 text-blue font-black"><?php echo $subtitle; ?></h6>
                                <?php endif; ?>
                                <?php if ($content) : ?>
                                    <div class="mb-20 text-xs-sm leading-16 md:text-sm md:leading-18 font-normal">
                                        <?php echo $content; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($btn) : ?>
                                    <div class="text-center">
                                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--md c-btn--primary"
                                           target="<?php echo $btn['target']; ?>"><?php echo $btn['title']; ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>