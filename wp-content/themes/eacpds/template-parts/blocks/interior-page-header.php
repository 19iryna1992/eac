<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('title');
$marquee_title = get_field('marquee_title');
$description = get_field('description');

$media_type = get_field('media_type');

$image = get_field('image');

//video

$video_img = get_field('video_image');
$video_source = get_field('video_source');
$video_mp4 = get_field('video_mp4');
$video_youtube = get_field('video_url_youtube');
$video_vimeo = get_field('video_url_vimeo');
$video_link = get_field('video_link');
?>

<div class="acf-interior-page-header <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">

    <section class="JS-bg-anim overflow-hidden s-anim-grad relative pt-40 lg:pt-60 pb-100 md:pb-20">
        <?php if ($marquee_title) : ?>
            <div class="c-moving-text__wrap flex opacity-30 w-full absolute">
                <div class="JS-moving-text c-moving-text flex animate-movingText">
                    <h2 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h2>
                    <h2 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h2>
                </div>
                <div class="JS-moving-text c-moving-text flex animate-movingText">
                    <h2 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h2>
                    <h2 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h2>
                </div>
            </div>
        <?php endif; ?>  

        <div class="container relative flex flex-wrap md:flex-nowrap md:items-end">
            <?php if ( ($video_link) || ($video_mp4) || ($image) ) : ?>
                <div class="w-full md:w-3/6 order-1 md:order-2">
                    <div class="JS-interior-media ">
                        <div class="relative max-w-280 lg:max-w-500 ml-auto xl:-mr-40 text-right">
                            <?php if ($media_type === 'video') : ?>

                                <?php if ($video_source === 'local') : ?>
                                    <a class="open-popup-video inline-block relative ml-auto" href="#id-<?= $video_mp4['ID']; ?>">
                                        <?php echo wp_get_attachment_image($video_img, 'interior-img', false, ['class' => 'w-full']) ?> 
                                        <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                            <span class="c-video__play-span absolute z-2"></span>
                                            <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                        </div>
                                    </a>
                                    <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                        <div class="w-full relative c-video__wrap">
                                            <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                                <div class="absolute top-0 left-0 w-full h-full">
                                                    <div class="overflow-hidden relative">
                                                        <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                                data-setup='{
                                                                    "controls": false,
                                                                    "responsive" : false,
                                                                    "fluid": true,
                                                                    "playsinline": true
                                                                    }'
                                                                tabindex="-1"
                                                                muted="muted"
                                                                autoplay="true"
                                                                loop="true">

                                                                <source src="<?= $video_mp4['url']; ?>"
                                                                type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                        </video>

                                                        <div class="c-video__icon is-active js-video-btn"
                                                            data-id="video-<?= $id; ?>">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if ($video_source === 'video_url_youtube') : ?>
                                    <a class="popup-youtube inline-block relative ml-auto" href="<?= $video_link; ?>">
                                        <?php echo wp_get_attachment_image($video_img, 'interior-img', false, ['class' => 'w-full']) ?>
                                        <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                            <span class="c-video__play-span absolute z-2"></span>
                                            <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                        </div>
                                    </a>
                                <?php endif; ?>

                                <?php if ($video_source === 'video_url_vimeo') : ?>
                                    <a class="popup-vimeo inline-block relative ml-auto" href="<?= $video_link; ?>">
                                        <?php echo wp_get_attachment_image($video_img, 'interior-img', false, ['class' => 'w-full']) ?>
                                        <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                            <span class="c-video__play-span absolute z-2"></span>
                                            <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                        </div>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if ($image) : ?>
                                <?php echo wp_get_attachment_image($image, 'interior-img', false, ['class' => 'w-full ml-auto']) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?> 
            <div class="w-full md:w-3/6 order-2 md:order-1">
                <div class="max-w-380 lg:max-w-full pt-30 pb-76 pr-15">
                    <?php if ($title) : ?>
                        <h1 class="text-white uppercase text-h3-lg md:text-h2-md font-title tracking-sm mb-16 lg:mb-45"><?php echo $title; ?></h1>
                    <?php endif; ?>

                    <?php if ($description) : ?>
                        <div class="text-white uppercase text-sm md:text-md font-title tracking-sm font-bold leading-12"><?php echo $description; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
    </section>
</div>

