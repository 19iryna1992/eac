<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$bg_image = get_field('nl_bg_img') ? get_field('nl_bg_img') : get_field('nl_bg_img', 'option');
$title = get_field('nl_title') ? get_field('nl_title') : get_field('nl_title', 'option');
$description = get_field('nl_description') ? get_field('nl_description') : get_field('nl_description', 'option');
$form = get_field('nl_form_shortcode') ? get_field('nl_form_shortcode') : get_field('nl_form_shortcode', 'option');
?>

<div class="acf-newsletter-signup bg-local bg-center bg-no-repeat bg-cover shadow-inner md:min-h-600 2xl:min-h-800 py-40 md:py-60 lg:py-100 xl:py-160 <?php echo $blockClass; ?>"
     id="<?php echo esc_attr($id); ?>" style="background-image: url('<?= $bg_image ? $bg_image['url'] : ' ' ?>')">
    <div class="container">
        <div class="md:pl-20 xl:pl-40 mb-20 md:mb-45 md:max-w-4/5">
            <?php if ($title): ?>
                <h2 class="text-h3 md:h2 2xl:text-h2-2lg font-title font-black text-white uppercase leading-12 mb-20 md:mb-40 tracking-sm">
                    <?php echo $title ?>
                </h2>
            <?php endif ?>
            <?php if ($description): ?>
                <div class="2xl:max-w-860 text-md md:text-h3-sm font-default text-white font-medium md:font-bold leading-12 mb:leading-16 tracking-sm">
                    <?php echo $description ?>
                </div>
            <?php endif ?>
            <div class="2xl:max-w-860 text-md md:text-h3-sm font-default text-white font-medium md:font-bold leading-12 mb:leading-16 tracking-sm">
                {Form}
            </div>
        </div>
    </div>
</div>