<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$section_title = get_field('title');
$rows = get_field('tabs');

?>

<div class="acf-team-spotlight <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($section_title) : ?>
            <h2 class="mb-20 md:mb-40 text-h4 md:text-h3 font-title font-black leading-11 md:leading-175 tracking-sm uppercase"><?php echo $section_title; ?></h2>
        <?php endif; ?>
        <?php if ($rows): ?>
            <div class="c-tab c-tab--cards JS-tab-сards">
                <div class="c-tab__list-wrap relative">
                    <ul class="c-tab__list JS-tabs flex flex-wrap md:justify-center items-stretch mb-20 md:mb-30 -mx-3 md:-mx-5 text-center">
                        <?php while (have_rows('tabs')): the_row();
                            $tab_title = get_sub_field('tab_title');
                            ?>
                            <li class="relative px-3 md:px-5 mt-10 w-4/12">
                        <span class="<?php echo (get_row_index() == '1') ? 'bg-blue text-white border-blue' : 'bg-transparent text-black border-black'; ?> hover:bg-blue hover:text-white hover:border-blue break-all sm:break-normal flex justify-center items-center h-full py-10 md:py-30 px-5 md:px-8 border text-xs-lg md:text-md leading-10 font-bold cursor-pointer"
                            data-tab-trigger="<?php echo get_row_index(); ?>">
                            <?php echo $tab_title; ?>
                        </span>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>

                <div class="w-full">
                    <?php while (have_rows('tabs')): the_row();
                        $orientation = get_sub_field('orientation');
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $image = get_sub_field('image');
                        $btn = get_sub_field('button');
                        $textOnly = get_sub_field('text_only');
                        $useBg = get_sub_field('use_bg') ? ' bg-grey-light' : null;
                        $textClass = get_sub_field('text_only') ? 'text-sm md:text-base font-title font-black max-w-860 m-auto leading-25 md:leading-20 mb-24' : ' text-xs-sm md:text-sm font-default text-black font-normal leading-20 mb-4 md:mb-24';
                        ?>
                        <div class="JS-tab-content <?php echo (get_row_index() == '1') ? 'visible' : 'invisible'; ?> <?= $textOnly == 0 ? $useBg . ' md:bg-transparent' : '' ?>"
                             data-tab-content="<?php echo get_row_index(); ?>">

                            <div class="md:flex <?php echo $orientation; ?> <?php echo $useBg; ?> <?= $textOnly == 1 ? 'py-20' : 'py-20 md:p-20 lg:p-40' ?>">
                                <?php if ($orientation == 'left'): ?>
                                    <?php if ($image): ?>
                                        <?php echo wp_get_attachment_image($image, 'cta', false, ['class' => 'max-h-180 sm:max-h-280 object-cover mb-20 md:mb-0']); ?>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php if ($image): ?>
                                        <?php echo wp_get_attachment_image($image, 'cta', false, ['class' => 'max-h-180 sm:max-h-320 object-cover mb-20 md:mb-0']); ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if ($textOnly == 1): ?>
                                    <div class="my-30 mx-20 md:mx-30 lg:mx-auto md:my-20 lg:my-76">
                                        <div class="tracking-sm <?php echo $textClass; ?>">
                                            <?php echo $description; ?>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="md:w-8/12 <?= $orientation == 'left' ? 'md:pl-30' : 'md:relative md:pr-60 md:pb-80' ?> px-10 md:px-0">
                                        <?php if ($title): ?>
                                            <h3 class="mb-10 md:mb-20 text-md md:text-h3 font-title font-black text-black normal-case leading-13 md:leading-175 tracking-sm">
                                                <?php echo $title; ?>
                                            </h3>
                                        <?php endif; ?>
                                        <div class="leading-2 tracking-sm <?php echo $textClass; ?>">
                                            <?php echo $description; ?>
                                        </div>
                                        <?php if ($btn): ?>
                                            <a href="<?php echo $btn['url']; ?>"
                                               class="c-btn c-btn--sm c-btn--primary <?= $orientation == 'left' ? '' : 'md:absolute bottom-0' ?>"
                                               target="<?php echo $btn['target']; ?>">
                                                <?php echo $btn['title']; ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>

                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>