<?php
$controller = \Inc\Blocks\InnerBlock::getInstance();
$allowed_blocks = $controller->allowedBlocks();

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';


?>
<div class="acf-inner-block relative <?php echo $blockClass; ?> <?php echo $controller->getBgColor(); ?>" id="<?php echo esc_attr($id); ?>">
    <?php if ($controller->getBgType() === 'image') : ?>
        <?php get_template_part('template-parts/blocks/partials/background-image', null, ['imageID' => __($controller->getBgImageID()), 'position' => __($controller->getBgImagePosition())]); ?>
    <?php endif; ?>
    <div class="container relative">
        <?php if (!empty($allowed_blocks)) : ?>
            <InnerBlocks allowedBlocks="<?php echo esc_attr(wp_json_encode($allowed_blocks)); ?>" />
        <?php else : ?>
            <InnerBlocks />
        <?php endif; ?>
    </div>
</div>