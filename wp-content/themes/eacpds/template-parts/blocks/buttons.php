<?php
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$layout = get_field('buttons_position');

?>

<div class="acf-buttons <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
       <?php if($layout == 'one_row') : ?>

           <?php  if( have_rows('buttons') ): ?>
            <div class="flex flex-wrap laptop:flex-nowrap justify-center items-stretch -mx-10 xl:-mx-20">
               <?php while( have_rows('buttons') ): the_row();
                   $btn = get_sub_field('button');
                   ?>

                    <div class="w-full md:w-1/2 px-10 xl:px-20 text-center py-20">
                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--xl c-btn--more c-btn--secondary leading-11 h-full flex items-center justify-center" target="<?php echo $btn['target']; ?>"><?php echo $btn['title']; ?><?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?></a>
                    </div>

               <?php endwhile; ?>
            </div>   
           <?php endif; ?>

        <?php endif; ?>



        <?php if($layout == 'two_rows') : ?>
            <?php  if( have_rows('buttons') ): ?>
            <div class="flex flex-wrap justify-center items-center laptop:w-3/5 -mx-10 laptop:m-auto">
                <?php while( have_rows('buttons') ): the_row();
                    $btn = get_sub_field('button');
                    ?>

                    <div class="w-full md:w-1/2 px-10 flex items-center justify-center py-20">
                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--sm c-btn--more c-btn--secondary" target="<?php echo $btn['target']; ?>"><?php echo $btn['title']; ?><?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?></a>
                    </div>

                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        <?php endif; ?>


    </div>
</div>

