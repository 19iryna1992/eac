<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('title');
$description = get_field('description');
?>

<div class="acf-icon-columns-grid <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="mb-40 font-title tracking-sm  text-h3 uppercase leading-12 md:leading-175 font-black"><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if ($description) : ?>
            <div class="mb-40 md:mb-100 text-h6 font-normal md:text-h5 md:leading-20 leading-16 lg:w-3/4">
                <?php echo $description; ?>
            </div>
        <?php endif; ?>
        <?php if (have_rows('columns')): ?>
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-40 xl:gap-x-76 xl:gap-y-100">
                <?php while (have_rows('columns')): the_row();
                    $icon = get_sub_field('icon');
                    $column_title = get_sub_field('title');
                    $column_description = get_sub_field('description');
                    ?>
                    <div class="max-w-356">
                        <div class="flex items-center relative">
                            <?php if ($icon) : ?>
                            <div class="w-56 h-56 flex justify-center items-center bg-blue mr-10 md:mr-20 pt-8 pr-8 pb-8 pl-8">
                                <?php echo wp_get_attachment_image($icon,'full',false,['class'=>'max-w-40 max-h-40']); ?>
                            </div>
                            <?php endif; ?>
                            <?php if ($column_title) : ?>
                                <h4 class="flex-1 font-title text-h5 leading-12 font-black normal-case"><?php echo $column_title; ?></h4>
                            <?php endif; ?>
                        </div>
                        <?php if ($column_description) : ?>
                            <div class="font-normal pt-10 md:pt-0 md:pl-76 leading-15 md:leading-20"><?php echo $column_description; ?></div>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>