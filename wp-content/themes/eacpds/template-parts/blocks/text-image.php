<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$variations = get_field('variations');
$textSize = get_field('text_size');
$content_columns = get_field('content_columns');

$title = get_field('title');
$image = get_field('image');
$caption = get_field('caption');
$content = get_field('content');
$content_second = get_field('content_two_column');
$btn = get_field('button');

$illustration_image = get_field('illustration_image');

//video
$media_type = get_field('media_type');
$video_img = get_field('video_image');
$video_source = get_field('video_source');
$video_mp4 = get_field('video_mp4');
$video_youtube = get_field('video_url_youtube');
$video_vimeo = get_field('video_url_vimeo');
$video_link = get_field('video_link');
?>

<div class="acf-text-image overflow-hidden <?php echo ($variations === 'media_right_text_left_illustration' && $illustration_image) ? 'pb-100 md:pb-150' : 'pb-40 lg:pb-80'; ?> <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">

        <?php if ($variations === 'media_left_text_right') : ?>
            <?php if ($title) : ?>
                <h3 class="text-h3 text-black uppercase font-black"><?php echo $title; ?></h3>
            <?php endif; ?>
            <div class="relative flex items-center flex-wrap -mx-20 xl:-mx-30 py-30">
                <div class="relative w-full md:w-1/2 px-20 xl:px-30 order-1 mobile-md:mb-20">
                    <?php if ($media_type === 'image' && $image) : ?>                    
                        <figure>
                            <?php echo wp_get_attachment_image($image, 'big-image', false, ['class' => 'JS-parallax-qk w-full max-h-330 md:max-h-960 object-cover object-top']); ?>
                            <?php if ($caption) : ?>
                                <figcaption
                                        class="font-default font-bold text-base text-grey mt-15 md:mt-40"><?php echo $caption; ?></figcaption>
                            <?php endif; ?>
                        </figure>
                    <?php endif; ?>
                    <?php if ($media_type === 'video') : ?>

                        <?php if ($video_source === 'local') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="open-popup-video block relative ml-auto h-full" href="#id-<?= $video_mp4['ID']; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full  animate-videoBtn">
                                        <span class="c-video__play-span"></span>
                                    </div>
                                </a>
                            </div>
                            <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                <div class="w-full relative c-video__wrap">
                                    <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                        <div class="absolute top-0 left-0 w-full h-full">
                                            <div class="overflow-hidden relative">
                                                <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                        data-setup='{
                                                            "controls": false,
                                                            "responsive" : false,
                                                            "fluid": true,
                                                            "playsinline": true
                                                            }'
                                                        tabindex="-1"
                                                        muted="muted"
                                                        autoplay="true"
                                                        loop="true">

                                                        <source src="<?= $video_mp4['url']; ?>"
                                                        type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                </video>

                                                <div class="c-video__icon is-active js-video-btn"
                                                    data-id="video-<?= $id; ?>">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_youtube') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-youtube block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_vimeo') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-vimeo block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if ($content): ?>
                    <div class="relative w-full md:w-1/2 px-20 xl:px-30 order-2">
                        <div class="text-grey <?php echo $textSize; ?>">
                            <?php echo $content; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($btn) : ?>
                    <div class="relative mt-100 w-full order-3 text-center">
                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--xl c-btn--more c-btn--secondary"
                           target="<?php echo $btn['target']; ?>">
                            <?php echo $btn['title']; ?>
                            <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

        <?php elseif ($variations === 'media_right_text_left_illustration') : ?>

            <?php if ($title) : ?>
                <h3 class="relative z-10 text-h3 text-black uppercase font-black"><?php echo $title; ?></h3>
            <?php endif; ?>
            <div class="relative flex items-stretch flex-wrap -mx-20 xl:-mx-30 pt-30 pb-30 md:pb-100">

                <?php if ($illustration_image) : ?>
                    <div class="absolute hidden md:block -left-10% xl:-left-1/4 -bottom-150 bg-center bg-no-repeat bg-cover md:h-540 xl:h-800 w-3/5"
                         style="background-image: url(<?php echo $illustration_image['url']; ?>)"></div>
                <?php endif; ?>
                <div class="relative w-full md:w-1/2 px-20 xl:px-30 order-1 md:order-2 mobile-md:mb-20">
                    <?php if ($media_type === 'image' && $image) : ?> 
                        <figure>
                            <?php echo wp_get_attachment_image($image, 'big-image', false, ['class' => 'JS-parallax-md w-full max-h-330 md:max-h-640 object-cover object-top']); ?>
                            <?php if ($caption) : ?>
                                <figcaption
                                        class="font-default font-bold text-base text-grey mt-15 md:mt-40"><?php echo $caption; ?></figcaption>
                            <?php endif; ?>
                        </figure>
                    <?php endif; ?>
                    <?php if ($media_type === 'video') : ?>

                        <?php if ($video_source === 'local') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="open-popup-video block relative ml-auto h-full" href="#id-<?= $video_mp4['ID']; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                            <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                <div class="w-full relative c-video__wrap">
                                    <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                        <div class="absolute top-0 left-0 w-full h-full">
                                            <div class="overflow-hidden relative">
                                                <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                        data-setup='{
                                                            "controls": false,
                                                            "responsive" : false,
                                                            "fluid": true,
                                                            "playsinline": true
                                                            }'
                                                        tabindex="-1"
                                                        muted="muted"
                                                        autoplay="true"
                                                        loop="true">

                                                        <source src="<?= $video_mp4['url']; ?>"
                                                        type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                </video>

                                                <div class="c-video__icon is-active js-video-btn"
                                                    data-id="video-<?= $id; ?>">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_youtube') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-youtube block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_vimeo') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-vimeo block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if ($content) : ?>
                    <div class="relative w-full md:w-1/2 px-20 xl:px-30 order-2 md:order-1">
                        <div class="text-grey <?php echo $textSize; ?>">
                            <?php echo $content; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($btn) : ?>
                    <div class="relative mt-100 w-full order-3 text-center mobile-md:px-20">
                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--xl c-btn--more c-btn--secondary"
                           target="<?php echo $btn['target']; ?>">
                            <?php echo $btn['title']; ?>
                            <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

        <?php elseif ($variations === 'small_media_text') : ?>

            <?php if ($title) : ?>
                <h3 class="text-h3 text-black uppercase font-black"><?php echo $title; ?></h3>
            <?php endif; ?>
            <div class="flex justify-center items-stretch flex-wrap -mx-20 xl:-mx-30 py-30">
                <div class="w-full md:w-2/5 px-20 xl:px-30 order-1 mobile-md:mb-20">
                    <?php if ($media_type === 'image' && $image) : ?>
                        <figure>
                            <?php echo wp_get_attachment_image($image, 'small-media', false, ['class' => 'JS-parallax-sl w-full height-full max-w-520 max-h-330 md:max-h-640 mobile-md:mx-auto md:ml-auto object-cover object-top']); ?>
                            <?php if ($caption) : ?>
                                <figcaption
                                        class="font-default font-bold text-base text-grey mt-15 md:mt-40"><?php echo $caption; ?></figcaption>
                            <?php endif; ?>
                        </figure>
                    <?php endif; ?>
                    <?php if ($media_type === 'video') : ?>

                        <?php if ($video_source === 'local') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="open-popup-video block relative ml-auto h-full" href="#id-<?= $video_mp4['ID']; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                            <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                <div class="w-full relative c-video__wrap">
                                    <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                        <div class="absolute top-0 left-0 w-full h-full">
                                            <div class="overflow-hidden relative">
                                                <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                        data-setup='{
                                                            "controls": false,
                                                            "responsive" : false,
                                                            "fluid": true,
                                                            "playsinline": true
                                                            }'
                                                        tabindex="-1"
                                                        muted="muted"
                                                        autoplay="true"
                                                        loop="true">

                                                        <source src="<?= $video_mp4['url']; ?>"
                                                        type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                </video>

                                                <div class="c-video__icon is-active js-video-btn"
                                                    data-id="video-<?= $id; ?>">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_youtube') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-youtube block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_vimeo') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-vimeo block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php  if ( $content_columns == 'one_column') :?>
                <?php if ($content) : ?>
                    <div class="w-full md:w-2/5 px-20 xl:px-30 order-2">
                        <div class="c-wysiwyg text-black-light">
                            <div class="<?php echo $textSize; ?>">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php elseif($content_columns == 'two_column') :?>
                    <div class="w-full md:w-3/5 px-20 xl:px-30 order-2">
                        <div class="md:flex">
                            <?php if ($content) : ?>
                            <div class="c-wysiwyg text-black-light md:w-1/2">
                                <div class="<?php echo $textSize; ?>">
                                    <?php echo $content ?>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if ($content_second) : ?>
                            <div class="c-wysiwyg text-black-light md:w-1/2">
                                <div class="<?php echo $textSize; ?>">
                                    <?php echo $content_second ?>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($btn) : ?>
                    <div class="relative mt-100 w-full order-3 text-center">
                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--xl c-btn--more c-btn--secondary"
                           target="<?php echo $btn['target']; ?>">
                            <?php echo $btn['title']; ?>
                            <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

        <?php elseif ($variations === 'media_bullets') : ?>

            <div class="flex justify-center items-start flex-wrap -mx-20 xl:-mx-30 py-30">
                <?php if ($title) : ?>
                    <div class=" w-full md:w-4/5 mb-40 px-20 xl:px-30">
                        <h3 class="font-title font-black text-h3 leading-13 tracking-sm text-black uppercase"><?php echo $title; ?></h3>
                    </div>
                <?php endif; ?>
                <div class="w-full md:w-2/5 px-20 xl:px-30 order-1 mobile-md:mb-20">
                    <?php if ($media_type === 'image' && $image) : ?>
                        <figure>
                            <?php echo wp_get_attachment_image($image, 'big-image', false, ['class' => 'JS-parallax-md w-full height-full max-w-520 max-h-330 md:max-h-640 mobile-md:mx-auto md:ml-auto object-cover object-top']); ?>
                            <?php if ($caption) : ?>
                                <figcaption
                                        class="font-default font-bold text-base text-grey mt-15 md:mt-40"><?php echo $caption; ?></figcaption>
                            <?php endif; ?>
                        </figure>
                    <?php endif; ?>
                    <?php if ($media_type === 'video') : ?>

                        <?php if ($video_source === 'local') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="open-popup-video block relative ml-auto h-full" href="#id-<?= $video_mp4['ID']; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                            <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                <div class="w-full relative c-video__wrap">
                                    <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                        <div class="absolute top-0 left-0 w-full h-full">
                                            <div class="overflow-hidden relative">
                                                <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                        data-setup='{
                                                            "controls": false,
                                                            "responsive" : false,
                                                            "fluid": true,
                                                            "playsinline": true
                                                            }'
                                                        tabindex="-1"
                                                        muted="muted"
                                                        autoplay="true"
                                                        loop="true">

                                                        <source src="<?= $video_mp4['url']; ?>"
                                                        type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                </video>

                                                <div class="c-video__icon is-active js-video-btn"
                                                    data-id="video-<?= $id; ?>">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_youtube') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-youtube block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_vimeo') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-vimeo block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="w-full md:w-2/5 px-20 xl:px-30 order-2">
                    <?php if (have_rows('bullets_list')): ?>
                        <ul class="с-list c-list--square text-black-light">
                            <?php while (have_rows('bullets_list')): the_row();
                                $title = get_sub_field('title');
                                $description = get_sub_field('description');
                                ?>
                                <li class="mb-20">
                                    <?php if ($title) : ?>
                                        <h5 class="font-title font-black text-h5 leading-12 tracking-sm text-gray mb-5 text-inherit"><?php echo $title; ?></h5>
                                    <?php endif; ?>
                                    <?php if ($description) : ?>
                                        <div class="text-inherit"><?php echo $description; ?></div>
                                    <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>

        <?php elseif ($variations === 'full_width_media') : ?>

            <?php if ($title) : ?>
                <h3 class="text-h3 text-black uppercase font-black"><?php echo $title; ?></h3>
            <?php endif; ?>
            <div class="flex justify-center items-start flex-wrap -mx-20 xl:-mx-30 py-30">
                <div class="px-20 xl:px-30 order-1 <?php echo ($content) ? 'mb-40 md:mb-100' : null; ?>">
                    <?php if ($media_type === 'image' && $image) : ?>
                        <div class="w-full">
                            <?php echo wp_get_attachment_image($image, 'full', false, ['class' => 'JS-parallax-sl w-full max-h-330 md:max-h-550 mx-auto object-cover object-top']); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($media_type === 'video') : ?>

                        <?php if ($video_source === 'local') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="open-popup-video block relative ml-auto h-full" href="#id-<?= $video_mp4['ID']; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                            <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                <div class="w-full relative c-video__wrap">
                                    <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                        <div class="absolute top-0 left-0 w-full h-full">
                                            <div class="overflow-hidden relative">
                                                <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                        data-setup='{
                                                            "controls": false,
                                                            "responsive" : false,
                                                            "fluid": true,
                                                            "playsinline": true
                                                            }'
                                                        tabindex="-1"
                                                        muted="muted"
                                                        autoplay="true"
                                                        loop="true">

                                                        <source src="<?= $video_mp4['url']; ?>"
                                                        type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                </video>

                                                <div class="c-video__icon is-active js-video-btn"
                                                    data-id="video-<?= $id; ?>">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_youtube') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-youtube block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ($video_source === 'video_url_vimeo') : ?>
                            <div class="c-slideshow-header__media-block">
                                <a class="popup-vimeo block relative ml-auto h-full" href="<?= $video_link; ?>">
                                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                        <span class="c-video__play-span absolute z-2"></span>
                                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php if ($content) : ?>
                    <div class="w-full md:w-4/5 order-2 mb-40 px-20 xl:px-30">
                        <div class="c-wysiwyg">
                            <?php echo $content; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($btn) : ?>
                    <div class="relative mt-100 w-full order-3 text-center">
                        <a href="<?php echo $btn['url']; ?>" class="c-btn c-btn--xl c-btn--more c-btn--secondary"
                           target="<?php echo $btn['target']; ?>">
                            <?php echo $btn['title']; ?>
                            <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

        <?php endif; ?>

    </div>
</div>