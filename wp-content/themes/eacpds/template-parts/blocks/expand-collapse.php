<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('collapse_title');
$subtitle = get_field('collapse_subtitle');
$description = get_field('collapse_description');

?>

<div class="acf-expand-collapse <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="text-black text-h3 lg:text-h1 font-title uppercase leading-11 tracking-normal mb-30 lg:mb-40"><?php echo $title ?></h2>
        <?php endif; ?>
        <?php if ($subtitle) : ?>
            <h3 class="text-black-dark text-base lg:text-h3 font-bold mb-18 lg:mb-20 leading-15 uppercase"><?php echo $subtitle ?></h3>
        <?php endif; ?>
        <?php if ($description) : ?>
            <div class="text-black-dark text-sm lg:text-base leading-20 mb-20 lg:mb-76"><?php echo $description ?></div>
        <?php endif; ?>
        <?php if (have_rows('collapses')): ?>
            <div class="JS-accordion max-w-500 m-auto lg:max-w-860">
                <?php while (have_rows('collapses')): the_row();
                    $title = get_sub_field('title');
                    $content = get_sub_field('collapses_content');
                    $image = get_sub_field('collapses_image');
                    $link = get_sub_field('collapses_link');
                    ?>
                    <div class="JS-accordion-tab accordion-tab accordion-tab--sm relative pb-25 lg:pb-35">
                        <div class="JS-accordion-title cursor-pointer accordion-title accordion-title--sm relative text-md lg:text-base font-bold leading-12 pt-20 lg:pt-35 pr-25 font-title">
                            <?php echo $title; ?> <span class="block absolute right-0 top-24 lg:top-35"></span></div>
                        <div class="JS-accordion-body accordion-body accordion-body--img accordion-body--sm">
                            <div class="accordion-body__wrap pr-10 flex flex-wrap lg:flex-nowrap lg:pl-2">
                                <div class="c-wysiwyg flex-1 order-2 lg:order-1">
                                    <?php echo $content; ?>

                                    <?php if ($link) : ?>
                                        <a href="<?php echo $link['url']; ?>" class="text-blue text-h6 underline font-bold" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                                    <?php endif; ?>
                                </div>
                                <?php if ($image) : ?>
                                    <div class="accordion-image w-full lg:max-w-260 order-1 lg:order-2 text-center mb-25 lg:mb-0 lg:ml-40">
                                        <?php echo wp_get_attachment_image($image, 'accordion-image', false, ['class' => 'inline-block']); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>