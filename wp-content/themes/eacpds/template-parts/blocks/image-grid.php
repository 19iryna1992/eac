<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$main_image = get_field('main_section_image');
$main_title = get_field('section_title');
$main_description = get_field('section_description');
?>

<div class="acf-image-grid py-45 lg:py-60 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="relative flex items-start flex-wrap -mx-20">
            <?php if ($main_image) : ?>
                <div class="relative w-full lg:w-1/2 px-20 my-15 md:my-30">
                    <?php echo wp_get_attachment_image($main_image, 'grid-image', false, ['class' => 'w-full h-330 md:h-400 lg:h-540 object-cover object-center']); ?>
                </div>
            <?php endif; ?>
            <div class="relative w-full lg:w-1/2 px-20 my-15 md:my-30">
                <?php if ($main_title)  : ?>
                    <h3 class="font-title font-black text-h3 leading-11 lg:leading-13 tracking-sm text-black uppercase mb-20"><?php echo $main_title; ?></h3>
                <?php endif; ?>
                <?php if ($main_description)  : ?>
                    <div class="font-normal text-md md:text-base text-black-light leading-16 md:leading-21">
                        <?php echo $main_description; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (have_rows('images')): ?>
            <div class="relative flex items-stretch flex-wrap -mx-15 xl:-mx-40">
                <?php while (have_rows('images')): the_row();
                    $image = get_sub_field('image');
                    $title = get_sub_field('image_title');
                    $description = get_sub_field('image_description');
                    ?>
                    <div class="w-full md:w-1/2 px-15 xl:px-40 my-15 md:my-30">
                        <?php if ($image) : ?>
                            <div class="c-gallery relative h-330 md:h-400 lg:h-540">
                                <?php echo wp_get_attachment_image($image, 'grid-image', false, ['class' => 'w-full h-full object-cover object-center']); ?>
                                <?php if ($title || $description) : ?>
                                    <div class="c-gallery__desk absolute top-20 left-20 p-40 bg-blue text-white">
                                        <?php if ($title) : ?>
                                            <h5 class="font-title font-black text-h5 leading-12 tracking-sm uppercase  text-inherit mb-20"><?php echo $title; ?></h5>
                                        <?php endif; ?>
                                        <?php if ($description) : ?>
                                            <div class="font-base font-medium text-md leading-12 text-inherit">
                                                <?php echo $description ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>