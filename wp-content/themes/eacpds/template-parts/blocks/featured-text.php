<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$illPos = get_field('illustration_position');
$title = get_field('title');
$description = get_field('description');
$description_size_class = get_field('description_size');

$illustration_img = get_field('illustration_img');

$illustration_class = $illPos === 'right' ? 'right-0 object-right-top' : 'left-0  object-left-top';
?>

<div class="acf-featured-text py-40 md:py-100 relative overflow-hidden <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>"> 
    <?php if ($illustration_img) : ?>
        <div>
            <?php echo wp_get_attachment_image($illustration_img, 'full', false, ['class' => 'JS-parallax-sl absolute top-0 object-contain h-540 lg:h-700 ' . $illustration_class]); ?>
        </div>
    <?php endif; ?> 
    <div class="container relative">
        <div class="w-full md:py-40 lg:py-100 <?php echo ($illPos === 'right') ? 'md:w-11/12 2xl:w-10/12 mx-auto' : 'md:w-10/12 2xl:w-9/12 lg:ml-100'; ?>">
            <?php if ($title) : ?>
                <h2 class="font-title font-black text-h3 md:text-h2 lg:text-h1 leading-12 tracking-sm text-black uppercase mb-20 md:mb-60"><?php echo $title; ?></h2>
            <?php endif; ?>
            <?php if ($description) : ?>
                <div class="text-gray <?php echo $description_size_class; ?>">
                    <?php echo $description; ?>
                </div>
            <?php endif; ?>
        </div>
    </div> 
</div>