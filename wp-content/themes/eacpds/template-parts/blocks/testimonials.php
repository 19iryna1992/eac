<?php
$controller = \Inc\Blocks\Testimonials::getInstance();
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$testimonialsType = get_field('testimonials_type');

$testimonials = get_field('testimonials');
$section_title = get_field('title');
$btn = get_field('testimonials_btn');
?>

<div class="acf-testimonials <?php echo $blockClass; ?> "
     id="<?php echo esc_attr($id); ?>">
        <?php if ($testimonialsType === 'small'): ?>
            <?php if ($testimonials) : ?>
                <div class="bg-gradient-to-b lg:bg-gradient-to-l from-blue-dark to-blue">
                    <div class="container">
                        <div class="pt-40 pb-60 md:py-80 lg:py-180">
                            <?php if ($section_title) : ?>
                                <h2 class="mb-40 md:mb-60 text-white text-h3 md:text-h2 leading-11 font-title font-black normal-case tracking-sm text-center"><?php echo $section_title; ?></h2>
                            <?php endif; ?>
                            <div class="sm:flex flex-wrap mb-40 justify-center md:mb-30 lg:mb-60 sm:-mx-20 md:-mx-10 md:-mx-15 xl:mx-0">
                                <?php foreach ($testimonials as $testimonial) :
                                    $rating = get_field('rating', $testimonial->ID);
                                    $quote = get_field('quote', $testimonial->ID);
                                    $role = get_field('role', $testimonial->ID);
                                    ?>
                                    <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 text-center mb-40 sm:px-25 md:px-10 lg:px-15 xl:px-50 flex flex-col">
                                        <?php if ($quote) : ?>
                                            <blockquote
                                                    class="mb-10 font-roboto text-white text-xs-sm md:text-extra-xs leading-20 font-medium flex-grow">
                                                <?php echo $quote; ?>
                                            </blockquote>
                                        <?php endif; ?>
                                        <?php if ($rating) : ?>
                                            <div class="mb-10 md:mb-20">
                                                <?php echo $controller->getStarRating($rating); ?>
                                            </div>
                                        <?php endif; ?>
                                        <p class="text-white text-xs-sm md:text-extra-xs leading-20 font-black"><?php echo get_the_title($testimonial->ID); ?> <?php echo ($role) ? ', '.$role :null; ?></p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
        
                            <?php if ($btn) : ?>
                                <div class="text-center">
                                    <a href="<?php echo $btn['url']; ?>" target="<?php echo $btn['target']; ?>"
                                       class="c-btn c-btn--xl c-btn--more c-btn--secondary"><?php echo $btn['title']; ?><?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php elseif ($testimonialsType === 'single') : ?>
            <?php if ($testimonials) : ?>
                <?php
                $countTestimonials = 1;
                foreach ($testimonials as $testimonial) :
                    $rating = get_field('rating', $testimonial->ID);
                    $quote = get_field('quote', $testimonial->ID);
                    if ($countTestimonials === 1) :
                        ?>
                        <div class="bg-gradient-to-b lg:bg-gradient-to-l from-blue-dark to-blue">
                            <div class="container">
                                <div class="md:w-3/4 lg:w-4/5 m-auto py-80 md:py-160 text-white">
                                    <div class="relative">
                                        <span class="text-h1-3xs lg:text-h1-2lg leading-9 absolute -left-5 -top-50 lg:-left-20 lg:-top-150">“</span>
                                        <?php if ($quote) : ?>
                                            <blockquote
                                                    class="text-base md:text-h3-lg leading-20 md:leading-13 tracking-sm font-bold">
                                                <?php echo $quote; ?>
                                            </blockquote>
                                        <?php endif; ?>
                                        <span class="text-h1-3xs lg:text-h1-2lg leading-9 absolute right-0 lg:-right-20 -bottom-35 lg:-bottom-150 transform -rotate-180">“</span>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    <?php endif; ?>
                    <?php $countTestimonials++; endforeach; ?>
            <?php endif; ?>

        <?php elseif ($testimonialsType === 'slideshow') : ?>

            <?php if ($testimonials) : ?>
                <div class="bg-gradient-to-t lg:bg-gradient-to-l from-blue-dark to-blue">
                    <div class="container">
                        <div class="JS-testimonials-slider-wrapper pt-50 pb-20">
        
        
                            <div class="JS-testimonials-slider -mx-10 pb-20 md:pb-40 xl:pb-100">
                                <?php foreach ($testimonials as $testimonial) :
                                    $rating = get_field('rating', $testimonial->ID);
                                    $quote = get_field('quote', $testimonial->ID);
                                    $role = get_field('role', $testimonial->ID);
                                    ?>
                                    <div class="lg:flex md:items-center px-10 overflow-hidden">
                                        <div class="w-full lg:max-w-480 mb-20 md:mb-0 md:pr-60 m-x-auto relative overflow-hidden">
                                            <?php if (has_post_thumbnail($testimonial->ID)) : ?>
                                                <div class="max-w-290 mb-20 md:mb-40 lg:mb-60 p-20 m-auto lg:m-0">
                                                    <?php echo get_the_post_thumbnail($testimonial->ID, 'full', ['class' => 'w-full']); ?>
                                                </div>
                                            <?php endif; ?>
                                            <h5 class="text-white font-roboto font-bold text-base lg:text-h3 leading-15"><?php echo get_the_title($testimonial->ID); ?></h5>
                                            <?php if ($role) : ?>
                                                <div class="text-white font-roboto font-normal text-md lg:text-base">
                                                    <p><?php echo $role; ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="relative text-white py-40 lg:py-100 flex-1 items-center">
                                            <?php if ($quote) : ?>
                                                <span class="text-h1-3xs lg:text-h1-2lg leading-9 absolute -left-5 lg:left-50 top-0 lg:-top-20">“</span>
                                                <blockquote
                                                        class="text-h3 md:text-h3-sm lg:text-h3-xl xl:text-h2-md leading-14 tracking-sm font-bold">
                                                    <?php echo $quote; ?>
                                                </blockquote>
                                                <span class="text-h1-3xs lg:text-h1-2lg leading-9 absolute right-0 lg:right-80 bottom-0 lg:-bottom-20 transform -rotate-180">“</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
        
        
                            <div class="mt-5 flex justify-start md:justify-end items-center">
                                <div class="relative min-w-180 md:min-w-280 mr-40 relative">
                                    <div class="JS-testimonials-slider-dots flex justify-between"></div>
                                    <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 block w-full max-w-100 md:max-w-200 h-2 rounded overflow-hidden bg-grey-light bg-gradient-to-r from-blue to-blue bg-no-repeat JS-progress-testimonials"
                                         role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="JS-testimonials-slider-arrows"></div>
                            </div>
        
        
                        </div>
                    </div>
                </div>
                <?php if ($btn) : ?>
                    <div class="text-center mt-40 md:mt-100">
                        <a href="<?php echo $btn['url']; ?>" target="<?php echo $btn['target']; ?>"
                           class="c-btn c-btn--xl c-btn--more c-btn--secondary"><?php echo $btn['title']; ?><?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?></a>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

        <?php endif; ?>

</div>