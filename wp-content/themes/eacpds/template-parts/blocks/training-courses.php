<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';


?>

<div class="acf-training-courses py-40 md:py-60 lg:py:100 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if (have_rows('tabs')): ?>
            <div class="c-tab c-tab--featured-products JS-tab-featured-products">
                <div class="c-tab__list-wrap relative flex mobile-lg:flex-wrap items-start xl:items-center mb-20 md:mb-40">
                    <div class="c-tab__filter-by px-10 py-40 bg-black text-white uppercase w-full sm:w-1/1 md:w-1/3 lg:max-w-140 text-sm text-center lg:mr-10">
                        <?php _e('Filter by:', 'eacpds'); ?>
                    </div>
                    <ul class="c-tab__list JS-tabs flex flex-wrap justify-start items-stretch w-full text-center">
                        <?php while (have_rows('tabs')): the_row();
                            $tabTitle = get_sub_field('tab_title');
                            ?>
                            <li class="relative my-5 w-1/2 sm:w-1/3 md:w-1/4 lg:max-w-140 min-h-80">
                        <span class="<?php echo (get_row_index() == '1') ? 'bg-grey-light' : 'bg-transparent'; ?>  text-black hover:bg-grey-light flex justify-center items-center h-full py-10 px-10 md:px-8 text-xs-sm leading-10 font-bold cursor-pointer"
                              data-tab-trigger="<?php echo get_row_index(); ?>">
                            <?php echo $tabTitle; ?>
                        </span>
                            </li>
                        <?php endwhile; ?>
                    </ul>

                </div>

                <div class="w-full">
                    <?php while (have_rows('tabs')): the_row();
                        $courses = get_sub_field('post_types');
                        $hideEmpty = get_sub_field('empty_tab');
                        ?>
                        <div class="JS-tab-content <?php echo (get_row_index() == '1') ? 'visible' : 'invisible'; ?>"
                             data-tab-content="<?php echo get_row_index(); ?>">
                            <?php if ($courses && !$hideEmpty) : ?>
                                <?php foreach ($courses as $course) :
                                    $courseDate = get_field('course_date', $course->ID);
                                    $courseTime = get_field('course_time', $course->ID);
                                    $date = new DateTime($courseDate);
                                    $price = get_field('price', $course->ID);
                                    $registerUrl = get_field('register_url', $course->ID);
                                    ?>
                                    <div class="flex mobile-md:flex-wrap w-full py-30">
                                        <div class="w-full md:w-5/12 lg:w-4/12 md:pr-15 lg:pr-30 mobile-md:mb-20 text-center">
                                            <div class="inline-block relative h-180 md:h-240 lg:h-260 max-h-260 w-full h-full">
                                                <?php if ($courseDate): ?>
                                                    <div class="absolute left-0 mobile-md:top-0 md:bottom-0 w-100  md:w-140 h-100 md:h-full flex flex-col items-start md:items-center justify-center bg-blue-dark py-10 md:py-25 px-10 md:px-15">
                                                        <span class="font-title font-black text-h3 md:text-h2-md tracking-sm leading-11 text-white"><?php echo $date->format('j'); ?></span>
                                                        <span class="font-roboto font-bold text-extra-xs md:text-sm md:text-md tracking-sm leading-16 text-white"><?php echo $date->format('F'); ?></span>
                                                        <?php if ($courseTime): ?>
                                                            <span class="font-roboto font-bold text-extra-xs md:text-sm tracking-sm text-white md:mt-10"><?php echo $courseTime . ' EST'; ?></span>
                                                        <?php endif ?>
                                                    </div>
                                                <?php endif ?>
                                                <?php echo get_the_post_thumbnail($course->ID, 'post-tab', array('class' => 'object-cover object-center h-full w-full wp-post-image')) ?>
                                            </div>
                                        </div>
                                        <div class="w-full md:w-7/12 lg:w-8/12 text-left pt-10">
                                            <div class="flex flex-col laptop:flex-row">
                                                <div class="lg:mr-30 xl:mr-56">
                                                    <h3 class="font-title font-black mb-20 text-base lg:text-h3 text-black leading-14 tracking-sm">
                                                        <?php echo get_the_title($course->ID); ?>
                                                    </h3>
                                                    <div class="c-wysiwyg font-roboto leading-20 text-black-light">
                                                        <?php echo get_the_content(null, false, $course->ID); ?>
                                                    </div>
                                                    <?php if ($price) : ?>
                                                        <span class="font-title font-black text-blue tracking-sm text-base mobile-md:mt-10"><?php echo '$' . $price; ?></span>
                                                    <?php endif; ?>
                                                </div>
                                                <?php if ($registerUrl): ?>
                                                    <div class="flex sm:justify-start laptop:justify-center items-center flex-wrap">
                                                        <a class="c-btn c-btn--xs c-btn--more c-btn--primary xl:py-30 mobile-md:mt-20"
                                                           target="_blank"
                                                           href="<?php echo $registerUrl ?>">
                                                            <?php _e('Register', 'eacpds') ?>
                                                            <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                                        </a>
                                                    </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>

                                <?php endforeach; ?>

                            <?php elseif ($hideEmpty):
                                $sectionTitle = get_sub_field('related_title');
                                $empty_text = get_sub_field('empty_tab_text');
                                $empty_btn = get_sub_field('empty_tab_button');
                                ?>
                                <div class="bg-grey-light py-10 px-20 md:p-30 xl:p-40 mt-30 mb-80 lg:mb-100">
                                    <div class="w-11/12 mx-auto flex flex-wrap items-center md:justify-between">
                                        <?php if ($empty_text): ?>
                                            <div class="w-full lg:max-w-550 md:mr-30 my-10">
                                                <h3 class="font-title font-black text-base lg:text-h4 xl:text-h3 text-black tracking-sm leading-14 mobile-md:px-5">
                                                    <?php echo $empty_text ?></h3>
                                            </div>
                                        <?php endif ?>
                                        <?php if ($empty_btn): ?>
                                            <div class="mobile-md:w-full my-10">
                                                <a href="<?php echo $empty_btn['url'] ?>" class="c-btn c-btn--md c-btn--primary lg:py-30"><?php echo $empty_btn['title'] ?></a>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php if ($sectionTitle) : ?>
                                <h2 class="mb-40 md:mb-60 font-title text-bold text-h3 leading-10 md:leading-175 uppercase font-black"><?php echo $sectionTitle; ?></h2>
                            <?php endif; ?>
                                <?php if (have_rows('blocks')): ?>
                                <div class="flex flex-wrap -mx-20">

                                    <?php while (have_rows('blocks')): the_row();
                                        $image = get_sub_field('image');
                                        $title = get_sub_field('title');
                                        $subtitle = get_sub_field('subtitle');
                                        $content = get_sub_field('collapses_content');
                                        $btn = get_sub_field('button');
                                        ?>
                                        <div class="mb-20 w-full md:w-1/2 lg:w-1/3 flex flex-col justify-center px-20">
                                            <?php if ($image) : ?>
                                                <div class="max-h-300 lg:max-h-400 h-full overflow-hidden flex justify-center items-start bg-blue-alice py-30 md:py-40 px-40">
                                                    <?php echo wp_get_attachment_image($image, 'middle-square', false, ['class' => 'object-cover']); ?>
                                                </div>
                                            <?php endif; ?>
                                            <div class="mx-10 bg-white px-15 pt-20 md:py-35 -mt-20 md:-mt-35 flex-grow flex flex-col justify-center">
                                                <?php if ($title) : ?>
                                                    <h6 class="font-title text-h6 leading-12 mb-30 md:mb-40 flex-grow font-black"><?php echo $title; ?></h6>
                                                <?php endif; ?>
                                                <?php if ($subtitle) : ?>
                                                    <h6 class="mb-20 font-title leading-12 text-xs-sm md:text-sm md:leading-18 text-blue font-black"><?php echo $subtitle; ?></h6>
                                                <?php endif; ?>
                                                <?php if ($content) : ?>
                                                    <div class="text-xs-sm leading-16 md:text-sm md:leading-18 font-normal">
                                                        <?php echo $content; ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if ($btn) : ?>
                                                    <div class="text-center">
                                                        <a href="<?php echo $btn['url']; ?>"
                                                           class="c-btn c-btn--md c-btn--primary"
                                                           target="<?php echo $btn['target']; ?>"><?php echo $btn['title']; ?></a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>

                            <?php endif; ?>

                        </div>
                    <?php endwhile; ?>
                </div>

            </div>
        <?php endif; ?>


    </div>

</div>
