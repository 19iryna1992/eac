<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';


?>

<div class="acf-post-types-tabs py-50 lg:py-100 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">

    <?php if (have_rows('tabs')): ?>
        <div class="container">
            <div class="c-tab c-tab--post JS-tab-post">
                <div class="c-tab__list-wrap relative">
                    <ul class="c-tab__list JS-tabs relative flex md:justify-center items-center flex-wrap text-center mb-20 md:mb-30 -mx-5 md:-mx-20">
                        <?php while (have_rows('tabs')): the_row();
                            $tab_title = get_sub_field('tab_title');
                            ?>
                            <li class="relative px-5 md:px-20 mt-10">
                                <span class="<?php echo (get_row_index() == '1') ? 'bg-black text-white' : 'bg-grey-light text-black'; ?> block hover:bg-black hover:text-white text-sm lg:text-md leading-12 font-bold text-center cursor-pointer py-20 lg:py-35 px-10 md:px-30 min-w-100 lg:min-w-240 uppercase"  data-tab-trigger="<?php echo get_row_index(); ?>">
                                    <?php echo $tab_title; ?>
                                </span>
                            </li>
                        <?php endwhile; ?> 
                    </ul>
                </div>

                <div class="w-full">
                    <?php while (have_rows('tabs')): the_row();
                        $tab_description = get_sub_field('description');
                        $tabBtn = get_sub_field('tab_btn');
                        $relposts = get_sub_field('post_types');
                        ?>
                        <div class="JS-tab-content md:mx-auto md:w-4/5 <?php echo (get_row_index() == '1') ? 'visible' : 'invisible'; ?>"
                             data-tab-content="<?php echo get_row_index(); ?>">
                            <?php if ($tab_description) : ?>
                                <div class=" text-sm md:text-base font-normal leading-12 text-grey-dim">
                                    <?php echo $tab_description; ?>
                                </div>
                            <?php endif; ?>

                            <?php if ($relposts) : ?>
                                <?php foreach ($relposts as $relpost) :
                                    $logo = get_field('small_logo', $relpost->ID);
                                    ?>
                                    <div class="flex mobile-md:flex-wrap w-full my-30">

                                        <div class="w-full md:w-5/12 lg:w-4/12 md:mr-15 lg:mr-30 mobile-md:mb-20 text-center">
                                            <?php if (has_post_thumbnail($relpost->ID)) : ?>
                                                <div class="inline-block relative max-h-180 md:max-h-220 lg:max-h-260 w-full h-full">
                                                    <?php echo get_the_post_thumbnail($relpost->ID, 'post-tab', ['class' => 'object-cover object-center h-full w-full']); ?>
                                                    <?php if ($logo) : ?>
                                                        <div class="absolute left-0 mobile-md:top-0 md:bottom-0 w-80 md:w-100 h-80 md:h-100 flex items-center justify-center bg-white py-15 md:py-25 px-5 md:px-15">
                                                            <?php echo wp_get_attachment_image($logo, 'full'); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <div class="w-full md:w-7/12 lg:w-8/12 text-left pt-10">
                                            <h3 class="mb-20 text-base md:text-h3 font-title font-black text-black leading-14 tracking-sm"><?php echo get_the_title($relpost->ID); ?></h3>
                                            <?php if (has_excerpt($relpost->ID)): ?>
                                                <div class="text-sm leading-2 tracking-sm text-black-light">
                                                    <?php echo get_the_excerpt($relpost->ID); ?>
                                                </div>
                                            <?php endif; ?>
                                            <div class="mt-20 md:mt-30 mobile-md:text-right">
                                                <a href="<?php echo get_the_permalink($relpost->ID); ?>"
                                                   class="c-link c-link--primary normal-case"><?php _e('Learn more', 'eacpds'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>


                            <?php if ($tabBtn) : ?>
                                <div class="mt-60 text-center">
                                    <a href="<?php echo $tabBtn['url']; ?>" target="<?php echo $tabBtn['target']; ?>"
                                       class="c-btn c-btn--xl c-btn--more c-btn--secondary">
                                        <?php echo $tabBtn['title']; ?>
                                        <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                    </a>
                                </div>
                            <?php endif; ?>

                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
