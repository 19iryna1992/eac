<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$icon = get_field('cf_icon_img') ? get_field('cf_icon_img') : get_field('cf_icon_img', 'option');
$title = get_field('cf_title') ? get_field('cf_title') : get_field('cf_title', 'option');
$description = get_field('cf_description') ? get_field('cf_description') : get_field('cf_description', 'option');
$form = get_field('cf_form_shortcode') ? get_field('cf_form_shortcode') : get_field('cf_form_shortcode', 'option');
?>

<div class="acf-contact-form <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="flex flex-wrap">

            <div class="w-full sm:w-260">
                <div class="w-full sm:w-260 bg-blue-dark inline-block p-60">
                    <?php echo wp_get_attachment_image($icon, 'full', false, ['class' => 'max-w-140 mx-auto']) ?>
                </div>
            </div>

            <div class="w-full md:w-auto flex-1 md:pl-40">
                <?php if ($title) : ?>
                    <h2 class="mb-0 text-h3 font-title font-black leading-11 md:leading-175 tracking-sm "><?php echo $title; ?></h2>
                <?php endif; ?>
                <?php if ($description) : ?>
                    <div class="text-h6">
                        <?php echo $description; ?>
                    </div>
                <?php endif; ?>
                <div class="c-contact-form">
                    <?php echo do_shortcode($form); ?>
                </div>
            </div>

        </div>
    </div>
</div>