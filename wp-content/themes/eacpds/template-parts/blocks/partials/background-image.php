<?php

echo wp_get_attachment_image($args['imageID'], 'full', null, ['class' => 'w-full z-0 h-full absolute top-0 left-0 object-cover ' . $args['position']]);
