<?php
$controller = \Inc\Blocks\Button::getInstance();
$blockClass = $block["className"] ?? '';
?>

<?php if ($controller->getBtnAction() === 'link') :
    $btn = $controller->getBtn();
?>
    <a href="<?php echo esc_url($btn['url']); ?>" class="rounded border border-blu p-2 inline-block text-center <?php echo $controller->getBtnClass(); ?> <?php echo $blockClass; ?>" target="<?php echo $btn['target'] ?>" <?php echo $controller->getBtnRel() ?>>
        <?php esc_html_e($btn['title']);  ?>
    </a>

<?php else :
    $btn = $controller->getBtnFile();
?>
    <a href="<?php echo esc_url($btn['url']); ?>" class="rounded border border-blu p-2 inline-block relative pr-5 <?php echo $controller->getBtnClass(); ?> <?php echo $block['className'] ?>" target="_blank" <?php echo $controller->getBtnRel() ?>>
        <?php esc_html_e($btn['title']); ?>
        <?php echo get_file_icon('colorCurrent w-16 h-16 absolute top-1/2 transform -translate-y-1/2 right-2'); ?>
    </a>
<?php endif; ?>