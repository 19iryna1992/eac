<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$section_title = get_field('section_title');
$section_description = get_field('section_description');
$btn = get_field('fp_tab_btn');
?>

<div class="acf-featured-products s-featured-product py-60 md:py-100 <?php echo $blockClass; ?>"
     id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($section_title || $section_description) : ?>
            <div class="mb-70 md:mb-100 lg:mb-160">
                <?php if ($section_title) : ?>
                    <h2 class="font-title font-black text-h3 md:text-h1 leading-12 tracking-sm text-black uppercase mb-20 md:mb-60"><?php echo $section_title; ?></h2>
                <?php endif; ?>
                <?php if ($section_description) : ?>
                    <div class="text-md lg:text-h3 leading-15 font-normal text-grey-dark">
                        <?php echo $section_description; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if (have_rows('fp_tabs')): ?>
            <div class="c-tab c-tab--featured-products JS-tab-featured-products">
                <div class="c-tab__list-wrap relative flex mobile-lg:flex-wrap items-start xl:items-center mb-20 md:mb-40">
                    <div class="c-tab__filter-by px-10 py-40 bg-black text-white uppercase w-full sm:w-1/1 md:w-1/3 lg:max-w-140 text-sm text-center lg:mr-10">
                        <?php _e('Filter by:', 'eacpds'); ?>
                    </div>
                    <ul class="c-tab__list JS-tabs flex flex-wrap justify-start items-stretch w-full text-center">
                        <?php while (have_rows('fp_tabs')): the_row();
                            $tabTitle = get_sub_field('fp_tab_title');
                            ?>
                            <li class="relative my-5 w-1/2 sm:w-1/3 md:w-1/4 lg:max-w-140 min-h-80">
                        <span class="<?php echo (get_row_index() == '1') ? 'bg-grey-light' : 'bg-transparent'; ?>  text-black hover:bg-grey-light flex justify-center items-center h-full py-10 px-10 md:px-8 text-xs-sm leading-10 font-bold cursor-pointer"
                              data-tab-trigger="<?php echo get_row_index(); ?>">
                            <?php echo $tabTitle; ?>
                        </span>
                            </li>
                        <?php endwhile; ?>
                    </ul>

                </div>

                <div class="w-full">
                    <?php while (have_rows('fp_tabs')): the_row();
                        $featuredPosts = get_sub_field('featured_products');
                        ?>
                        <div class="flex flex-wrap -mx-15 lg:-mx-20 JS-tab-content <?php echo (get_row_index() == '1') ? 'visible' : 'invisible'; ?>"
                             data-tab-content="<?php echo get_row_index(); ?>">
                            <?php if ($featuredPosts) : ?>
                                <?php foreach ($featuredPosts as $featuredPost) :
                                    $logo = get_field('small_logo', $featuredPost->ID);
                                    ?>
                                    <div class="w-full md:w-1/2 lg:w-1/3 my-10 md:my-20 lg:my-30 px-15 lg:px-20">
                                        <div class="c-featured-products bg-white">
                                            <div class="relative h-360">
                                                <?php if (has_post_thumbnail($featuredPost->ID)) : ?>
                                                    <?php echo get_the_post_thumbnail($featuredPost->ID, 'post-tab', ['class' => 'h-full w-full object-cover']); ?>
                                                <?php endif; ?>
                                                <?php if (has_excerpt($featuredPost->ID)): ?>
                                                    <div class="c-featured-products__desk absolute p-40 top-0 left-0 w-full h-full shadow-inner flex flex-col justify-center items-center">
                                                        <div class="max-w-4/5 text-base text-white font-default font-bold leading-12 tracking-sm">
                                                            <?php echo get_the_excerpt($featuredPost->ID); ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="flex items-center">
                                                <?php if ($logo) : ?>
                                                    <div class="w-2/5">
                                                        <?php echo wp_get_attachment_image($logo, 'full', false, ['class' => 'max-w-100 sm:max-w-140 md:max-w-100 laptop:max-w-140 mx-auto']); ?>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="c-featured-products__btn w-3/5 bg-blue">
                                                    <a class="px-40 py-25 block text-sm text-white font-default font-bold leading-12 tracking-sm"
                                                       href="<?php echo get_the_permalink($featuredPost->ID); ?>">
                                                        <?php _e('Learn more about ', 'eacpds'); ?><?php echo get_the_title($featuredPost->ID); ?>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>

            </div>
        <?php endif; ?>


    </div>
</div>