<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('main_title');
$description = get_field('main_description');

?>

<div class="acf-offerings-animated-slideshow <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="s-anim-grad relative s-offer-show pt-40 md:pt-100 lg:pt-150 2xl:pt-200 overflow-hidden">
        <div class="container mb-50">

            <?php if ($title) : ?>
                <h1 class="text-md font-title leading-12 tracking-sm text-white mb-20 uppercase
                    md:max-w-500 md:text-h3-xl md:mb-40
                    lg:max-w-750 lg:text-h2-lg lg:mb-50
                    xl:text-h1 xl:leading-10 xl:mb-60">
                    <?php echo $title; ?>
                </h1>
            <?php endif; ?>

            <?php if ($description) : ?>                
                <div class="text-white text-sm leading-16
                    md:text-h6
                    lg:text-h5
                    xl:text-h4 xl:leading-20">
                    <?php echo $description; ?>
                </div>
            <?php endif; ?>
        </div>
        
        <?php if( have_rows('slides') ): ?> 
            <div class="c-offer-show JS-offer-show">
                <div class="c-offer-show__wrap">
                    <div class="JS-offer-show-wrap">
                        <?php while( have_rows('slides') ): the_row(); 
                            
                            $title = get_sub_field('title');
                            $description = get_sub_field('description');
                            $image = get_sub_field('image');
                            $marquee_title = get_sub_field('marquee_text');
                            $slide_btn = get_sub_field('slide_btn');

                            ?>
                            <div class="JS-offer-show-block md:flex md:items-center overflow-hidden relative lg:height-screen">
                                <?php if ($marquee_title) : ?>
                                    <!-- <div class="c-moving-text__wrap flex opacity-30 w-full absolute">
                                        <div class="JS-moving-text c-moving-text flex animate-movingText">
                                            <h1 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h1>
                                            <h1 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h1>
                                        </div>
                                        <div class="JS-moving-text c-moving-text flex animate-movingText">
                                            <h1 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h1>
                                            <h1 class="c-moving-text__title uppercase text-h1-3md font-title lg:text-h1-xl leading-10 tracking-sm"><?php echo $marquee_title; ?></h1>
                                        </div>
                                    </div> -->
                                <?php endif; ?>
                                <div class="c-offer-show__slide 3xl:pt-120">
                                    <div class="container md:flex justify-between ">
                                        <div class="c-offer-show__text mb-30 md:flex flex-col md:w-1/2">
                                            <?php if ($title) : ?>
                                                <h3 class="text-base font-title leading-12 tracking-sm text-white mb-20 uppercase 
                                                    md:text-h3
                                                    lg:text-h3-xl
                                                    xl:text-h2">
                                                    <?php echo $title; ?>
                                                </h3>
                                            <?php endif; ?>

                                            <?php if ($description) : ?>
                                                <div class="text-white text-md leading:12
                                                    lg:text-h5 lg:leading-18
                                                    xl:text-h4 xl:leading-20">
                                                    <?php echo $description; ?>
                                                </div>
                                            <?php endif; ?>

                                            <?php if ($slide_btn) : ?>
                                                <div class="hidden md:block mt-70
                                                    lg:mt-100
                                                    xl:mt-160">
                                                    <a href="<?php echo esc_url( $slide_btn['url'] ); ?>" class="c-btn c-btn--xl c-btn--secondary">
                                                        <?php echo esc_html( $slide_btn['title'] ); ?>
                                                        <span class="inline-block ml-5 align-middle"><?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?></span>                                
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <?php if ($image) : ?>
                                            <div class="mb-30 md:w-1/2">
                                                <div class="c-offer-show__img relative mx-auto lg:ml-auto">
                                                    <span class="c-offer-show__img-circle absolute top-0 left-0 bottom-0 right-0">
                                                        <svg width="539" height="539" viewBox="0 0 539 539" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <circle  class="icon-circle animate-offerCircle" cx="269.5" cy="269.5" r="268.5" stroke="#008ECF" stroke-width="2"/>
                                                        </svg>
                                                    </span>
                                                    <?php echo wp_get_attachment_image($image, 'full', false, ['class' => 'w-full']) ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if ($slide_btn) : ?>
                                            <div class="mb-30 md:hidden">
                                                <a href="<?php echo esc_url( $slide_btn['url'] ); ?>"
                                                    target="<?php echo esc_attr( $slide_btn['target'] ); ?>"
                                                    class="c-btn c-btn--xl c-btn--secondary px-15 py-20">
                                                    <?php echo esc_html( $slide_btn['title'] ); ?>
                                                    <span class="inline-block ml-5 align-middle"><?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?></span>                                
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div> 
        <?php endif; ?>
    </div>
</div>