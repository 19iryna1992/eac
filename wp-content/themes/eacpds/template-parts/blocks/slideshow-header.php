<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

?>

<div class="acf-slideshow-header <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">

    <section class="JS-bg-anim s-slideshow-header s-anim-grad relative pt-40 lg:pt-60">      
        <div class="relative">                

            <?php if( have_rows('slides') ): ?>
                <div class="JS-slideshow-header">
                
                    <?php while( have_rows('slides') ): the_row(); 
                        
                        $title = get_sub_field('title');
                        $highlight_words = get_sub_field('highlight_words');
                        $highlight_words_array = explode(',', $highlight_words);
                        $description = get_sub_field('description');
                        $slide_btn = get_sub_field('slide_btn');
                        $media_type = get_sub_field('media_type');
                        $image = get_sub_field('image');
                        $video_img = get_sub_field('video_image');
                        $video_source = get_sub_field('video_source');
                        $video_mp4 = get_sub_field('video_mp4');
                        $video_youtube = get_sub_field('video_url_youtube');
                        $video_vimeo = get_sub_field('video_url_vimeo');
                        $video_link = get_sub_field('video_link');

                        ?>
                        <div class="c-slideshow-header pt-20 xl:pt-45 px-10 xl:px-0">
                            <div class="container">
                                <div class="flex flex-wrap md:flex-nowrap">
                                    <div class="w-full c-slideshow-header__text order-2 md:order-1 relative md:pr-20">

                                        <div class="md:flex md:flex-col md:justify-center md:h-full lg:pb-160">
                                            <?php if ($highlight_words) : ?>
                                                <h3 class="c-slideshow-header__header text-white text-h3-lg xl:text-h2-lg normal-case leading-11 xl:leading-125 mb-30 xl:mb-60 font-title tracking-sm">
                                                    <?php echo preg_replace("/(" . implode("|", $highlight_words_array) . ")/i", "<div class=\"inline uppercase text-h3-xl xl:text-h2-3lg whitespace-nowrap leading-125 xl:leading-14\"><div class=\"JS-decode inline-block\">$1</div></div>", $title); ?>
                                                </h3>

                                            <?php else : ?>

                                                <?php if ($title) : ?>
                                                    <h3 class="c-slideshow-header__header text-white text-h3-lg xl:text-h2-lg normal-case leading-11 xl:leading-125 mb-30 font-title tracking-sm"><?php echo $title; ?></h3>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                            <?php if ($description) : ?>
                                                <div class="c-slideshow-header__description text-white uppercase xl:normal-case text-sm xl:text-h5-md leading-12 xl:leading-16 font-title tracking-sm "><?php echo $description; ?></div>
                                            <?php endif; ?>

                                        </div>

                                        <?php if ($slide_btn) : ?>
                                            <div class="mt-50 md:mt-0 md:absolute md:bottom-0">
                                                <a href="<?php echo esc_url( $slide_btn['url'] ); ?>" class="tracking-sm font-title text-white text-sm xl:text-md leading-11 relative lg:block c-slideshow-header__btn">
                                                    <span class="c-slideshow-header__btn-text">
                                                        <?php echo esc_html( $slide_btn['title'] ); ?>
                                                    </span>
                                                    <span class="c-slideshow-header__btn-arrow absolute hidden lg:block">
                                                        <svg width="38" height="148" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M19.449 147.707l17.46-15c.455-.391.455-1.023 0-1.414a1.301 1.301 0 00-1.646 0L19.79 144.586V1c0-.553-.52-1-1.164-1-.643 0-1.164.447-1.164 1v143.586L1.989 131.293a1.301 1.301 0 00-1.646 0 .93.93 0 00-.341.707.93.93 0 00.34.707l17.461 15a1.301 1.301 0 001.646 0z" fill="#D9E0FF"/>
                                                        </svg>
                                                    </span>
                                                    <span class="c-slideshow-header__btn-circle absolute hidden lg:block">
                                                        <svg width="160" height="160" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <circle class="animate-allWork" opacity=".5" cx="80" cy="80" r="79" stroke="url(#paint0_linear)" stroke-width="2"/>
                                                            <defs>
                                                                <linearGradient id="paint0_linear" x1="80" y1="0" x2="80" y2="160" gradientUnits="userSpaceOnUse">
                                                                <stop stop-color="#0C2CC8"/>
                                                                <stop offset="1" stop-color="#00A1DF"/>
                                                                </linearGradient>
                                                            </defs>
                                                        </svg>
                                                    </span>
                                                </a>
                                            </div>
                                        <?php endif; ?>

                                    </div>

                                    <div class="w-full c-slideshow-header__img order-1 md:order-2 mb-20 md:mb-0">
                                        <div class="JS-interior-media max-w-280 md:max-w-340 lg:max-w-750 md:ml-auto md:mr-20 xl:mr-45 ml-auto">
                                            <div class="relative c-slideshow-header__media">
                                                <?php if ($media_type === 'video') : ?>

                                                    <?php if ($video_source === 'local') : ?>
                                                        <div class="c-slideshow-header__media-block">
                                                            <a class="open-popup-video block relative ml-auto h-full" href="#id-<?= $video_mp4['ID']; ?>">
                                                                <?php echo wp_get_attachment_image($video_img, 'header-slider-img', false, ['class' => 'w-full']) ?>
                                                                <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                                                    <span class="c-video__play-span absolute z-2"></span>
                                                                    <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                                                            <div class="w-full relative c-video__wrap">
                                                                <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                                                                    <div class="absolute top-0 left-0 w-full h-full">
                                                                        <div class="overflow-hidden relative">
                                                                            <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                                                                    data-setup='{
                                                                                        "controls": false,
                                                                                        "responsive" : false,
                                                                                        "fluid": true,
                                                                                        "playsinline": true
                                                                                        }'
                                                                                    tabindex="-1"
                                                                                    muted="muted"
                                                                                    autoplay="true"
                                                                                    loop="true">

                                                                                    <source src="<?= $video_mp4['url']; ?>"
                                                                                    type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                                                            </video>

                                                                            <div class="c-video__icon is-active js-video-btn"
                                                                                data-id="video-<?= $id; ?>">
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if ($video_source === 'video_url_youtube') : ?>
                                                        <div class="c-slideshow-header__media-block">
                                                            <a class="popup-youtube block relative ml-auto h-full" href="<?= $video_link; ?>">
                                                                <?php echo wp_get_attachment_image($video_img, 'header-slider-img', false, ['class' => 'w-full']) ?>
                                                                <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                                                    <span class="c-video__play-span absolute z-2"></span>
                                                                    <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if ($video_source === 'video_url_vimeo') : ?>
                                                        <div class="c-slideshow-header__media-block">
                                                            <a class="popup-vimeo block relative ml-auto h-full" href="<?= $video_link; ?>">
                                                                <?php echo wp_get_attachment_image($video_img, 'header-slider-img', false, ['class' => 'w-full']) ?>
                                                                <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                                                                    <span class="c-video__play-span absolute z-2"></span>
                                                                    <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <?php if ($image) : ?>
                                                    <div class="c-slideshow-header__media-block">
                                                        <?php echo wp_get_attachment_image($image, 'header-slider-img', false, ['class' => 'w-full ml-auto']) ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    
                </div>
            <?php endif; ?>
            
        </div>
    </section>
</div>