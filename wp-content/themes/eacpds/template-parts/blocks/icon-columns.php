<?php
$controller = \Inc\Blocks\AnimatedIconColumns::getInstance();

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('title');
$description = get_field('description');
?>

<div class="acf-anim-icon-columns py-50 lg:py-100 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="mb-20 lg:mb-40 w-full">
            <?php if ($title) : ?>
                <h2 class="font-title text-h3 md:text-h2 text-black font-black uppercase leading-12 tracking-sm md:text-center">
                    <?php echo $title; ?>
                </h2>
            <?php endif; ?>
            <?php if ($description) : ?>
                <div class="font-default font-bold text-md leading-12 md:leading-13 text-black tracking-sm mt-20">
                    <?php echo $description; ?>
                </div>
            <?php endif; ?>
        </div>

        <?php if (have_rows('columns')): ?>
            <div class="flex flex-wrap justify-center <?php echo $controller->getGridClass(); ?>">
                <?php while (have_rows('columns')): the_row();
                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="<?php echo $controller->getColumnClass(); ?>">
                        <div class="w-full min-h-200 px-30 py-30 bg-blue-dark <?php echo $controller->getImageClass(); ?>">
                            <?php echo wp_get_attachment_image($icon, $controller->getImageSize(), false, ["class" =>($controller->getRowCount() > 4) ? 'mx-auto max-h-140' : 'mx-auto max-h-140 md:max-h-220']); ?>
                        </div>
                        <div class="mt-20">
                            <?php if ($title) : ?>
                                <h5 class="text-h6 md:text-h5 font-title font-black uppercase text-blue md:text-center leading-12 tracking-sm mb-20">
                                    <?php echo $title; ?>
                                </h5>
                            <?php endif; ?>
                            <?php if ($description) : ?>
                                <div class="c-wysiwyg text-sm text-black-light tracking-sm leading-16 md:leading-18">
                                    <?php echo $description; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>




