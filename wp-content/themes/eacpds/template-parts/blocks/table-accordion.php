<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

// ACF vars
$table_count_column = get_field('table_accordion_count_columns');

// Column First
$popular_column_first = get_field('popular_accordion_column_first');
$title_column_first = get_field('title_accordion_table_first');
$description_column_first = get_field('description_accordion_table_first');


// Column Second
$popular_column_second = get_field('popular_accordion_column_second');
$title_column__second = get_field('title_accordion_table_second');
$description_column__second = get_field('description_accordion_table_second');

// Column Third
$popular_column_third = get_field('popular_accordion_column_third');
$title_column__third = get_field('title_accordion_table_third');
$description_column__third = get_field('description_accordion_table_third');

// Column Fourth
$popular_column_fourth = get_field('popular_accordion_column_fourth');
$title_column_fourth = get_field('title_accordion_table_fourth');
$description_column_fourth = get_field('description_accordion_table_fourth');

?>

<div class="acf-accordion-table <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <h2>Accordion Price</h2>
        <div class="hidden laptop:block"> 
            <div class="grid <?= $table_count_column == 'five' ? 'grid-cols-5' : 'grid-cols-4' ?> w-full">
                <div> <!--for empty first col--> </div>
                <div class="text-center mb-40 md:mb-0 border-solid border border-grey-extra-light mt-40">
                    <?php if ($popular_column_first): ?>
                        <div class="bg-blue-dark text-white">
                            <?php _e('Most Popular', 'eacpds') ?>
                        </div>
                    <?php endif ?>
                    <?php if ($title_column_first): ?>
                        <div class=" <?= $popular_column_first ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php echo $title_column_first ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($description_column_first): ?>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php echo $description_column_first ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="text-center mb-40 md:mb-0">
                    <?php if ($popular_column_second): ?>
                        <div class="bg-blue-dark text-white font-title text-sm leading-11 font-black p-8 md:p-10 min-h-40">
                            <?php _e('Most Popular', 'eacpds') ?>
                        </div>
                    <?php endif ?>
                    <?php if ($title_column__second): ?>
                        <div class="selection-blue-dark <?= $popular_column_second ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php echo $title_column__second ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($description_column__second): ?>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php echo $description_column__second ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="text-center mb-40 md:mb-0 border-solid border border-grey-extra-light mt-40">
                    <?php if ($popular_column_third): ?>
                        <div class="bg-blue-dark text-white">
                            <?php _e('Most Popular', 'eacpds') ?>
                        </div>
                    <?php endif ?>
                    <?php if ($title_column__third): ?>
                        <div class=" <?= $popular_column_third ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php echo $title_column__third ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($description_column__third): ?>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php echo $description_column__third ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if ($table_count_column == 'five'): ?>
                    <div class="text-center mb-40 md:mb-0 border-solid border border-grey-extra-light mt-40">
                        <?php if ($popular_column_fourth): ?>
                            <div class="bg-blue-dark text-white">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($title_column_fourth): ?>
                            <div class=" <?= $popular_column_fourth ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                                <?php echo $title_column_fourth ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($description_column_fourth): ?>
                            <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                <?php echo $description_column_fourth ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>    

        <div class="w-full">
            <?php if (have_rows('table_accordion')): ?>
                <?php while (have_rows('table_accordion')) :
                    the_row();
                    $accordion_label = get_sub_field('table_accordion_btn_label');
                    $price_col_second = get_sub_field('price_accordion_table_second');
                    $table_btn_second = get_sub_field('table_btn_second');
                    $price_col_third = get_sub_field('price_accordion_table_third');
                    $table_btn_third = get_sub_field('table_btn_third');
                    $price_col_fourth = get_sub_field('price_accordion_table_fourth');
                    $table_btn_fourth = get_sub_field('table_btn_fourth');
                    $price_col_fifth = get_sub_field('price_accordion_table_fifth');
                    $table_btn_fifth = get_sub_field('table_btn_fifth');
                    ?>
                    <!-- START: desktop main table -->
                    <div class="hidden laptop:block">
                        <div class="c-accordion-table-price__wpapper JS-accordion-table-wpapper my-5">
                            <table class="c-table-price c-table-price--accordion JS-accordion-table-tab w-full">
                                <tbody>
                                <tr class="c-table-price__tr-header JS-accordion-table-header cursor-pointer">
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/4' : 'w-1/3' ?>">
                                        <div>
                                            <?php if ($accordion_label): ?>
                                                <?php echo $accordion_label ?>
                                            <?php endif; ?>
                                        </div>
                                    </th>
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/6' : 'w-1/5' ?>">
                                        <div>
                                            <?php if ($price_col_second): ?>
                                                <?php echo $price_col_second ?>
                                            <?php endif; ?>
                                        </div>
                                    </th>
    
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/6' : 'w-1/5' ?>">
                                        <div>
                                            <?php if ($price_col_third): ?>
                                                <?php echo $price_col_third ?>
                                            <?php endif; ?>
                                        </div>
                                    </th>
    
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/6' : 'w-1/5' ?>">
                                        <div>
                                            <?php if ($price_col_fourth): ?>
                                                <?php echo $price_col_fourth ?>
                                            <?php endif; ?>
                                        </div>
                                    </th>
    
                                    <?php if ($table_count_column == 'five'): ?>
                                        <th class="<?= $table_count_column == 'five' ? 'w-1/6' : 'w-1/12' ?>">
                                            <div>
                                                <?php if ($price_col_fifth): ?>
                                                    <?php echo $price_col_fifth ?>
                                                <?php endif; ?>
                                            </div>
                                        </th>
                                    <?php endif; ?>
    
                                    <th class="relative w-1/12">
                                        <span class="c-accordion-table__btn-toggle block absolute"></span>
                                    </th>
                                </tr>
                                <tr class="h-10 c-table-price__spacer">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
    
                                <?php if (have_rows('table_accordion_row')): ?>
    
                                    <?php while (have_rows('table_accordion_row')): the_row(); ?>
                                        <tr>
                                            <?php get_template_part('template-parts/components/table/table-row') ?>
                                        </tr>
                                    <?php endwhile; ?>
    
                                    <tr class="c-table-price__tr-btn bg-white">
                                        <td class="bg-white"> <!--for empty first col--> </td>
                                        <td class="bg-white">
                                            <?php if ($table_btn_second): ?>
                                                <a class="c-btn c-btn--ex-xs c-btn--secondary"
                                                   href="<?php echo $table_btn_second['url'] ?>">
                                                    <?php echo $table_btn_second['title'] ?>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                        <td class="bg-white">
                                            <?php if ($table_btn_third): ?>
                                                <a class="c-btn c-btn--ex-xs c-btn--secondary"
                                                   href="<?php echo $table_btn_third['url'] ?>">
                                                    <?php echo $table_btn_third['title'] ?>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                        <td class="bg-white">
                                            <?php if ($table_btn_fourth): ?>
                                                <a class="c-btn c-btn--ex-xs c-btn--secondary"
                                                   href="<?php echo $table_btn_fourth['url'] ?>">
                                                    <?php echo $table_btn_fourth['title'] ?>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                        <td class="bg-white">
                                            <?php if ($table_count_column == 'five'): ?>
                                                <?php if ($table_btn_fifth): ?>
                                                    <a class="c-btn c-btn--ex-xs c-btn--secondary"
                                                       href="<?php echo $table_btn_fifth['url'] ?>">
                                                        <?php echo $table_btn_fifth['title'] ?>
                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>

            <!-- START: mobile second table  -->
            <div class="block laptop:hidden">
                <div class="text-center mb-40 md:mb-0 mt-40">
                    <?php if ($popular_column_first): ?>
                        <div class="bg-blue-dark text-white">
                            <?php _e('Most Popular', 'eacpds') ?>
                        </div>
                    <?php endif ?>
                    <?php if ($title_column_first): ?>
                        <div class=" <?= $popular_column_first ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10">
                            <?php echo $title_column_first ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($description_column_first): ?>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php echo $description_column_first ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php get_template_part('template-parts/components/table/table-mob-accordion/second-column') ?>

            </div>

            <!-- END: mobile second table  -->

            <!-- START: mobile third table  -->
            <div class="block laptop:hidden">
                <div class="text-center mb-40 md:mb-0 mt-40">
                    <div class="text-center mb-40 md:mb-0">
                        <?php if ($popular_column_second): ?>
                            <div class="bg-blue-dark text-white font-title text-sm leading-11 font-black p-8 md:p-10 min-h-40">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($title_column__second): ?>
                            <div class="selection-blue-dark <?= $popular_column_second ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10">
                                <?php echo $title_column__second ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($description_column__second): ?>
                            <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                <?php echo $description_column__second ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php get_template_part('template-parts/components/table/table-mob-accordion/third-column') ?>

                    <!--</div>-->

                    <!-- END: mobile third table  -->

                    <!-- START: mobile fourth table  -->
                    <!-- <div class="block laptop:hidden">-->
                    <div class="text-center mb-40 md:mb-0 mt-40">
                        <?php if ($popular_column_third): ?>
                            <div class="bg-blue-dark text-white">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($title_column__third): ?>
                            <div class=" <?= $popular_column_third ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10">
                                <?php echo $title_column__third ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($description_column__third): ?>
                            <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                <?php echo $description_column__third ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php get_template_part('template-parts/components/table/table-mob-accordion/fourth-column') ?>

                    <!--</div>-->

                    <!-- END: mobile fourth table  -->

                    <!-- START: mobile fifth table  -->
                    <?php if ($table_count_column == 'five'): ?>
                        <!-- <div class="block laptop:hidden">-->
                        <div class="text-center mb-40 md:mb-0 mt-40">
                            <?php if ($popular_column_fourth): ?>
                                <div class="bg-blue-dark text-white">
                                    <?php _e('Most Popular', 'eacpds') ?>
                                </div>
                            <?php endif ?>
                            <?php if ($title_column_fourth): ?>
                                <div class=" <?= $popular_column_fourth ? 'bg-blue text-white' : 'bg-white' ?>  bg-white font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10">
                                    <?php echo $title_column_fourth ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($description_column_fourth): ?>
                                <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                    <?php echo $description_column_fourth ?>
                                </div>
                            <?php endif; ?>
                        </div>

                        <?php get_template_part('template-parts/components/table/table-mob-accordion/fifth-column') ?>

                        <!--</div>-->
                    <?php endif; ?>
                    <!-- END: mobile fifth table  -->

                </div>
            </div>    
        </div>
    </div>
</div>