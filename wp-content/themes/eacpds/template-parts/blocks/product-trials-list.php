<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$section_title = get_field('product_trials_title');
$big_description = get_field('product_trials_big_description');
$small_description = get_field('product_trials_small_description');
$product_trials = get_field('relationship_product_trials');


?>
<div class="acf-product-trials <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($section_title || $big_description || $small_description ): ?>
            <?php if ($section_title): ?>
                <h2 class="text-h3 md:text-h2 uppercase mb-20 md:mb-40">
                    <?php echo $section_title ?>
                </h2>
            <?php endif; ?>
            <?php if ($big_description): ?>
                <div class="text-md font-bold leading-16 lg:leading-20 md:text-base text-black-light">
                    <?php echo $big_description ?>
                </div>
            <?php endif; ?>
            <?php if ($small_description): ?>
                <div class="mb-40 text-md md:text-base leading-16 md:leading-20 text-black-light font-normal">
                    <?php echo $small_description ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($product_trials): ?>
            <div class="">
                <?php foreach ($product_trials as $product):
                    $permalink = get_permalink($product->ID);
                    $title = get_the_title($product->ID);
                    $trial_link = get_field('product_trial', $product->ID);
                    $logo = get_field('small_logo', $product->ID);

                    ?>
                    <div class="flex mobile-md:flex-wrap w-full my-30 md:my-50">
                        <div class="w-full md:w-5/12 lg:w-4/12 md:mr-15 lg:mr-30 mobile-md:mb-20 text-center">
                            <?php if (has_post_thumbnail($product->ID)) : ?>
                                <div class="inline-block relative max-h-180 md:max-h-220 lg:max-h-260 w-full h-full">
                                    <?php echo get_the_post_thumbnail($product->ID, 'post-tab', array('class' => 'object-cover object-center h-full w-full wp-post-image')) ?>
                                    <?php if ($logo) : ?>
                                        <div class="absolute left-0 mobile-md:top-0 md:bottom-0 w-80 md:w-100 h-80 md:h-100 flex items-center justify-center bg-white py-15 md:py-25 px-5 md:px-15">
                                            <?php echo wp_get_attachment_image($logo, 'full'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="w-full md:w-7/12 lg:w-8/12 text-left pt-10">
                            <h3 class="mb-20 text-base md:text-h3 font-title font-black text-black uppercase leading-14 tracking-sm"><?php echo $title ?></h3>
                            <?php if (has_excerpt($product->ID)): ?>
                                <div class="text-sm leading-2 tracking-sm text-black-light">
                                    <?php echo get_the_excerpt($product->ID); ?>
                                </div>
                            <?php endif; ?>
                            <div class="flex flex-row flex-wrap justify-between">
                                <?php if ($trial_link): ?>
                                    <a class="c-btn c-btn--xs c-btn--primary mt-20 md:mt-30 block max-w-full md:max-w-260 md:inline-block" target="_blank"
                                       href="<?php echo $trial_link ?>">
                                        <?php _e('Start product trial', 'eacpds') ?>
                                    </a>
                                <?php endif ?>
                                <a class="c-btn c-btn--xs c-btn--secondary mt-20 md:mt-30 block max-w-full md:max-w-260 md:inline-block"
                                   href="<?php echo $permalink ?>">
                                    <?php _e('Learn more', 'eacpds') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

</div>
