<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('faqs_title') ? get_field('faqs_title') : get_field('faqs_title', 'option');
$subtitle = get_field('faqs_subtitle') ? get_field('faqs_subtitle') : get_field('faqs_subtitle', 'option');
$description = get_field('faqs_description') ? get_field('faqs_description') : get_field('faqs_description', 'option');
$faqs = get_field('faqs');
$button = get_field('faqs_link') ? get_field('faqs_link') : get_field('faqs_link', 'option');
?>

<div class="acf-faqs pt-30 pb-30 lg:pt-76 lg:pb-76 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="text-black text-h3 lg:text-h1 font-title uppercase leading-11 tracking-normal mb-30 lg:mb-40"><?php echo $title ?></h2>
        <?php endif; ?>
        <?php if ($subtitle) : ?>
            <h3 class="text-black-dark text-base lg:text-h3 font-bold mb-18 lg:mb-20 leading-15 uppercase"><?php echo $subtitle ?></h3>
        <?php endif; ?>
        <?php if ($description) : ?>
            <div class="text-black-dark text-sm lg:text-base leading-20 mb-20 lg:mb-76"><?php echo $description ?></div>
        <?php endif; ?>

        <?php if ($faqs) : ?>
            <div class="JS-accordion max-w-500 m-auto lg:max-w-860">
                <?php foreach ($faqs as $faq) : ?>
                    <div class="JS-accordion-tab accordion-tab relative pb-25 lg:pb-40">
                        <div class="JS-accordion-title cursor-pointer accordion-title relative text-md lg:text-h3-lg font-bold leading-12 pt-25 lg:pt-40 pr-30 lg:pr-50 font-title">
                            <?php echo get_the_title($faq->ID); ?> <span
                                    class="block absolute right-0 top-30 lg:top-45"></span>
                        </div>
                        <div class="JS-accordion-body accordion-body">
                            <div class="accordion-body__wrap pr-10 flex flex-wrap lg:flex-nowrap lg:pl-2">
                                <div class="c-wysiwyg flex-1 order-2 lg:order-1">
                                    <?php echo apply_filters('the_content', get_the_content(null, null, $faq->ID)); ?>
                                </div>
                                <?php if (has_post_thumbnail($faq->ID)) : ?>
                                    <div class="accordion-image w-full lg:max-w-260 order-1 lg:order-2 text-center mb-25 lg:mb-0 lg:ml-40">
                                        <?php echo get_the_post_thumbnail($faq->ID, 'accordion-image', ['class' => 'inline-block']); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if ($button) : ?>
            <div class="text-center mt-20 lg:mt-60">
                <a href="<?php echo $button['url']; ?>"
                   class="c-btn c-btn--md c-btn--secondary text-center w-full max-w-500"
                   target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>