<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';
$video_title = get_field('title');
$video_img = get_field('video_image');
$video_source = get_field('video_source');
$video_mp4 = get_field('video_mp4');
$video_youtube = get_field('video_url_youtube');
$video_vimeo = get_field('video_url_vimeo');
$video_link = get_field('video_link');
?>

<div class="acf-featured-video <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <?php if($video_title) : ?>
    <div class="container">
        <h3 class="text-h3 text-black uppercase font-black mb-30 md:mb-56"><?php echo $video_title; ?></h3>
    </div>
    <?php endif; ?>
    <div class="container relative">
        <div class="relative w-full">

            <?php if ($video_source === 'local') : ?>
                <a class="open-popup-video w-full relative block" href="#id-<?= $video_mp4['ID']; ?>">
                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                        <span class="c-video__play-span absolute z-2"></span>
                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                    </div>
                </a>
                <div id="id-<?= $video_mp4['ID']; ?>" class="mfp-hide relative m-auto c-video">
                    <div class="w-full relative c-video__wrap">
                        <div class="w-full max-w-full h-0" style="padding-top: 56.25%;">
                            <div class="absolute top-0 left-0 w-full h-full">
                                <div class="overflow-hidden relative">
                                    <video  id="video-<?= $id; ?>" class="video-js vjs-default-skin vjs-fill"  
                                            data-setup='{
                                                "controls": false,
                                                "responsive" : false,
                                                "fluid": true,
                                                "playsinline": true
                                                }'
                                            tabindex="-1"
                                            muted="muted"
                                            autoplay="true"
                                            loop="true">

                                            <source src="<?= $video_mp4['url']; ?>"
                                            type="video/<?= pathinfo($video_mp4['url'], PATHINFO_EXTENSION) ?>">

                                    </video>

                                    <div class="c-video__icon is-active js-video-btn"
                                        data-id="video-<?= $id; ?>">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($video_source === 'video_url_youtube') : ?>
                <a class="popup-youtube w-full relative block h-330 md:h-540 lg:h-800 overflow-hidden" href="<?= $video_link; ?>">
                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                        <span class="c-video__play-span absolute z-2"></span>
                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                    </div>
                </a>
            <?php endif; ?>

            <?php if ($video_source === 'video_url_vimeo') : ?>
                <a class="popup-vimeo w-full relative block" href="<?= $video_link; ?>">
                    <?php echo wp_get_attachment_image($video_img, 'full', false, ['class' => 'w-full']) ?>
                    <div class="c-video__play-btn absolute flex justify-center items-center rounded-full">
                        <span class="c-video__play-span absolute z-2"></span>
                        <span class="c-video__play-animate absolute z-1 block w-3 h-3 rounded-full animate-videoBtnSm lg:animate-videoBtnLg"></span>
                    </div>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>