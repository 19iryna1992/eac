<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$orientation = get_field('orientation');
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');
$btn = get_field('button');
$textOnly = get_field('text_only');
$useBg = get_field('use_bg') ? ' bg-grey-light' : null;
$textClass = get_field('text_only') ? 'text-sm md:text-base font-title font-black max-w-860 m-auto leading-25 md:leading-20 mb-24' : ' text-xs-sm md:text-sm font-default text-black font-normal leading-20 mb-4 md:mb-24';
?>

<div class="acf-callouts <?php echo $blockClass; ?> <?= $textOnly == 0 ? $useBg . ' md:bg-transparent' : '' ?>"
     id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="md:flex <?php echo $orientation; ?> <?php echo $useBg; ?> <?= $textOnly == 1 ? 'py-20' : 'py-20 md:p-20 lg:p-40' ?>">
            <?php if ($orientation == 'left'): ?>
                <?php if ($image): ?>
                    <?php echo wp_get_attachment_image($image, 'cta', false, ['class' => 'max-h-180 sm:max-h-280 object-cover mb-20 md:mb-0']); ?>
                <?php endif; ?>
            <?php else: ?>
                <?php if ($image): ?>
                    <?php echo wp_get_attachment_image($image, 'cta', false, ['class' => 'max-h-180 sm:max-h-320 object-cover mb-20 md:mb-0']); ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($textOnly == 1): ?>
                <div class="my-30 mx-20 md:mx-30 lg:mx-auto md:my-20 lg:my-76">
                    <div class="tracking-sm <?php echo $textClass; ?>">
                        <?php echo $description; ?>
                    </div>
                </div>
            <?php else: ?>
                <div class="md:w-8/12 <?= $orientation == 'left' ? 'md:pl-30' : 'md:relative md:pr-60 md:pb-80' ?>">
                    <?php if ($title): ?>
                    <h3 class="mb-10 md:mb-20 text-md md:text-h3 font-title font-black text-black normal-case leading-13 md:leading-175 tracking-sm">
                        <?php echo $title; ?>
                    </h3>
                    <?php endif; ?>
                    <div class="leading-2 tracking-sm <?php echo $textClass; ?>">
                        <?php echo $description; ?>
                    </div>
                    <?php if ($btn): ?>
                        <a href="<?php echo $btn['url']; ?>"
                           class="c-btn c-btn--sm c-btn--primary <?= $orientation == 'left' ? '' : 'md:absolute bottom-0' ?>"
                           target="<?php echo $btn['target']; ?>">
                            <?php echo $btn['title']; ?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>