<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';
$blockViewType = get_field('articles_block_type');

$section_title = get_field('title');
$section_description = get_field('description');

$news = get_field('post_types');
?>

<div class="acf-related-news-articles <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">

    <?php if ($blockViewType === 'slider'): ?>
        
        <div class="JS-news-slider-wrapper">
            <div class="bg-black pt-40 md:pt-80 lg:pt-160">
                <div class="container">
                    <div class="lg:pb-330 xl:pb-360">
                        <div class="mb-20 md:mb-40 lg:mb-80 xl:mb-160">
                            <?php if ($section_title) : ?>
                                <h2 class="mb-20 md:mb-40 text-white font-title uppercase text-h3-lg md:text-h2-3lg leading-9 font-black tracking-sm"><?php echo $section_title; ?></h2>
                            <?php endif; ?>
                            <?php if ($section_description) : ?>
                                <div class="w-full md:w-2/3 text-white font-roboto font-bold text-md md:text-base tracking-sm leading-11">
                                    <?php echo $section_description; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="pb-40 lg:pb-0 flex flex-col-reverse md:flex-row justify-end items-end md:items-center">
                            <div class="relative min-w-240 md:min-w-280 md:mr-40 relative">
                                <div class="JS-news-slider-dots flex justify-between"></div>
                                <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 block w-full max-w-180 md:max-w-200 h-2 rounded overflow-hidden bg-grey-light bg-gradient-to-r from-blue to-blue bg-no-repeat JS-progress-news"
                                     role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="JS-news-slider-arrows mb-20 md:mb-0"></div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="pb-150 lg:pb-0 bg-black lg:bg-blue-dark md:relative">
                <div class="container">
                    <div class="lg:h-700 xl:h-700">
                        <div class="lg:relative lg:-top-260 xl:-top-330">
                            <?php if ($news) : ?>
                                <div class="с-news-sler JS-news-slider -m-20 flex">
                                    <?php foreach ($news as $new) :
                                        $postAuthor = get_field('post_author', $new->ID);
                                        $topics = wp_get_post_tags($new->ID, ['fields' => 'names']);
                                        $offsite_link = get_field('offsite_link', $new->ID);
                                        $target_attr = $offsite_link ? '_blank' : '';
                                        $post_url = $offsite_link ? $offsite_link : get_the_permalink($new->ID);
                                        ?>
                                        <div class="h-full xl:mb-0 p-10 md:px-20 z-10 flex flex-col">
                                            <?php if (has_post_thumbnail($new->ID)) : ?>
                                                <div class="relative">
                                                    <a href="<?php echo $post_url; ?>" target="<?php echo $target_attr; ?>"
                                                       class="block overflow-hidden">
                                                        <?php if ($topics) : ?>
                                                            <span class="max-w-150 md:max-w-200 p-10 md:px-5 lg:px-20 lg:py-15 text-xs-sm lg:text-md leading-11 font-title font-black trackung-sm bg-black text-white absolute top-10 md:top-20 left-10 md:left-20 inline-block"><?php echo $topics[0]; ?></span>
                                                        <?php endif; ?>
                                                        <?php echo get_the_post_thumbnail($new->ID, 'post-slider-thumb', array('class' => 'w-full object-cover wp-post-image')) ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <div class="h-full min-h-220 lg:min-h-280 xl:min-h-400 md:-mt-40 md:mx-10 relative z-10 bg-white px-10 xl:px-20 py-10 xl:py-40 flex flex-col">
                                                <div class="mb-10 flex-grow">
                                                    <h3 class="mb-10 lg:mb-20 text-blue text-sm lg:text-base leading-11 font-title capitalize font-black tracking-sm">
                                                        <a href="<?php echo $post_url; ?>"
                                                           target="<?php echo $target_attr; ?>"><?php echo get_the_title($new->ID); ?></a>
                                                    </h3>
                                                    <?php if (has_excerpt($new->ID)): ?>
                                                        <div class="text-xs-sm lg:text-md leading-14 lg:leading-18 font-roboto font-normal text-black-light tracking-sm">
                                                            <?php echo get_the_excerpt($new->ID); ?>
                                                        </div>
                                                    <?php endif; ?>
                                                    <a href="<?php echo $post_url; ?>" target="<?php echo $target_attr; ?>"
                                                       class="c-btn--more hidden md:inline-block text-blue-dark font-roboto text-sm pr-10 leading-18 font-bold text-blue uppercase tracking-sm relative"><?php _e('Read More', 'eacpds'); ?><?php echo get_file_icon_arrow_right('w-15 h-full inline-block absolute -right-15'); ?></a>
                                                </div>
                                                <?php if ($postAuthor) : ?>
                                                    <?php foreach ($postAuthor as $author) :
                                                        $position = get_field('member_position', $author->ID);
                                                        ?>
                                                        <div class="flex items-center">
                                                            <?php if (has_post_thumbnail($author->ID)) : ?>
                                                                <?php echo get_the_post_thumbnail($author->ID, 'author-avatar', ['class' => 'rounded-full mr-20 w-40 lg:w-80 h-40 lg:h-80 wp-post-image object-cover']); ?>
                                                            <?php endif; ?>
                                                            <div class="flex-1">
                                                                <h4 class="text-xs-sm lg:text-base leading-11 lg:leading-18 tracking-sm font-black normal-case"><?php echo get_the_title($author->ID); ?></h4>
                                                                <?php if ($position) : ?>
                                                                    <span class="text-xs-sm lg:text-md font-roboto leading-10 lg:leading-15 tracking-sm font-normal"><?php echo $position; ?></span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($blockViewType === 'columns') : ?>

        <div class="container">
            <?php if ($section_title) : ?>
                <h2 class="mb-20 md:mb-50 font-title text-h3 leading-15 tracking-sm uppercase"><?php echo $section_title; ?></h2>
            <?php endif; ?>
            <?php if ($news) : ?>
                <div class="flex flex-wrap -mx-20">
                    <?php foreach ($news as $new) :
                        $postAuthor = get_field('post_author', $new->ID);
                        $topics = wp_get_post_tags($new->ID, ['fields' => 'names']);
                        $offsite_link = get_field('offsite_link', $new->ID);
                        $target_attr = $offsite_link ? '_blank' : '';
                        $post_url = $offsite_link ? $offsite_link : get_the_permalink($new->ID);
                        $readTime = get_field('read_time', $new->ID);
                        ?>
                        <div class="mb-40 sm:mb-10 md:mb-0 px-10 md:p-20 w-full sm:w-1/2 lg:w-1/3">
                            <?php if (has_post_thumbnail($new->ID)) : ?>
                                <div class="relative">
                                    <a href="<?php echo $post_url; ?>" target="<?php echo $target_attr; ?>"
                                       class="block overflow-hidden">
                                        <?php if ($topics) : ?>
                                            <span class="max-w-150 md:max-w-200 p-10 md:px-5 md:px-20 md:py-15 text-xs-sm md:text-md leading-11 font-title font-black trackung-sm bg-black text-white absolute top-10 md:top-20 left-10 md:left-20 inline-block"><?php echo $topics[0]; ?></span>
                                        <?php endif; ?>
                                        <?php echo get_the_post_thumbnail($new->ID, 'post-columns-thumb', array('class' => 'w-full object-cover wp-post-image')) ?>
                                    </a>
                                </div>
                            <?php endif; ?>

                            <div class="min-h-220 md:min-h-400 md:-mt-40 md:mx-10 relative z-10 bg-white px-10 md:px-20 py-10 md:py-40 flex flex-col">
                                <div class="mb-10 flex-grow">
                                    <h3 class="mb-10 md:mb-20 text-sm md:text-md leading-11 font-title capitalize font-black tracking-sm">
                                        <a href="<?php echo $post_url; ?>"
                                           target="<?php echo $target_attr; ?>"><?php echo get_the_title($new->ID); ?></a>
                                    </h3>
                                    <div class="hidden md:block mb-20">
                                        <span class="text-sm leading-12 font-roboto font-bold tracking-sm inline-block mr-20"><?php echo get_the_date('j F Y', $new->ID); ?></span>
                                        <?php if ($readTime) : ?>
                                            <span class="text-sm leading-12 font-roboto font-bold tracking-sm inline-block text-blue-dark"><?php echo $readTime . ' '; ?><?php _e('read', 'eacpds'); ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (has_excerpt($new->ID)): ?>
                                        <div class="text-xs-sm md:text-sm leading-14 md:leading-18 font-normal text-black-light tracking-sm">
                                            <?php echo get_the_excerpt($new->ID); ?>
                                        </div>
                                    <?php endif; ?>
                                    <a href="<?php echo $post_url; ?>" target="<?php echo $target_attr; ?>"
                                       class="c-btn--more hidden md:inline-block font-roboto text-sm pr-10 leading-18 font-bold text-blue uppercase tracking-sm relative"><?php _e('Read More', 'eacpds'); ?><?php echo get_file_icon_arrow_right('w-15 h-full inline-block absolute -right-15'); ?></a>
                                </div>
                                <?php if ($postAuthor) : ?>
                                    <?php foreach ($postAuthor as $author) :
                                        $position = get_field('member_position', $author->ID);
                                        ?>
                                        <div class="flex items-center">
                                            <?php if (has_post_thumbnail($author->ID)) : ?>
                                                <?php echo get_the_post_thumbnail($author->ID, 'author-avatar', ['class' => 'rounded-full mr-20 w-40 xl:w-80 h-40 xl:h-80 wp-post-image object-cover']); ?>
                                            <?php endif; ?>
                                            <div class="flex-1">
                                                <h4 class="text-sm xl:text-md leading-11 xl:leading-18 tracking-sm font-black normal-case"><?php echo get_the_title($author->ID); ?></h4>
                                                <?php if ($position) : ?>
                                                    <span class="text-xs-sm xl:text-sm font-roboto leading-11 xl:leading-15 tracking-sm font-normal"><?php echo $position; ?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>

    <?php endif; ?>

</div>