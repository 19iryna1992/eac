<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('careers_title');
$description = get_field('careers_description');
$careers = get_field('careers');
$btn = get_field('careers_link');
?>

<div class="acf-careers <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="mb-20 md:mb-40 text-h3 font-title font-black leading-11 md:leading-175 tracking-sm uppercase"><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if ($description) : ?>
            <div class="mb-40 text-md md:text-base leading-16 md:leading-20 text-black-light font-normal">
                <?php echo $description; ?>
            </div>
        <?php endif; ?>
        <?php if ($careers) : ?>
            <div class="flex flex-wrap bg-grey-light mobile-md:-mx-20 py-10 lg:p-20">
                <?php foreach ($careers as $career) : ?>
                    <div class="w-full md:w-1/3 xl:w-1/4 p-10 md:p-10 xl:p-20">
                        <div class="bg-white p-20">
                            <h3 class="font-title font-black text-h6 leading-11 mb-10 tracking-sm normal-case"><?php echo get_the_title($career->ID); ?></h3>
                            <?php if (has_excerpt($career->ID)) : ?>
                                <div class="text-sm leading-18 tracking-sm">
                                    <?php echo get_the_excerpt($career->ID); ?>
                                </div>
                                <a href="<?php echo get_the_permalink($career->ID); ?>"
                                   class="c-btn--more pr-0 text-sm leading-175 tracking-sm uppercase text-blue font-bold relative"><?php _e('Read more', 'eacpds'); ?><?php echo get_file_icon_arrow_right('w-15 h-full absolute -right-30'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if ($btn) : ?>
        <div class="text-center my-40">
            <a href="<?php echo $btn['url'] ?>" class="c-btn-outline c-btn-outline--lg c-btn-outline--primary text-grey hover:text-white" target="<?php echo $btn['target'] ?>"><?php echo $btn['title'] ?></a>
        </div>
        <?php endif; ?>
    </div>
</div>