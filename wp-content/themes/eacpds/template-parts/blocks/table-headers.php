<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('t_headers_title');


?>

<div class="acf-table-headers <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="mb-40 md:mb-60 text-center uppercase font-title font bold text-h3 md:text-h2 leading-11"><?php echo $title; ?></h2>
        <?php endif; ?>

        <?php if (have_rows('t_headers')): ?>
            <div class="sm:flex sm:justify-center sm:flex-wrap -mx-20">
                <?php while (have_rows('t_headers')): the_row();
                    $design = get_sub_field('t_column_design');
                    $subTitle = get_sub_field('sub_title');
                    $title = get_sub_field('main_title');
                    $description = get_sub_field('description');
                    $price = get_sub_field('price');
                    $priceDescription = get_sub_field('price_description');
                    ?>
                    <?php if ($design == 'bordered') : ?>

                        <div class="md:w-1/2 lg:w-1/3 xl:w-1/4 text-center mb-40 md:mb-60 px-20">
                            <div class="mt-40 border-solid border border-grey-extra-light h-full">
                                <?php if ($title) : ?>
                                    <div class="bg-white min-h-96 flex items-end justify-center font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                                        <?php echo $title; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($description) : ?>
                                    <div class="mb-20 md:mb-30 px-10 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                        <?php echo $description; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($price) : ?>
                                    <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                                        <?php echo $price; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($priceDescription) : ?>
                                    <div class="font-roboto font-normal text-sm leading-20">
                                        <?php echo $priceDescription; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    <?php else : ?>
                        <div class="md:w-1/2 lg:w-1/3 xl:w-1/4 text-center mb-40 md:mb-60 px-20">
                            <div>
                                <?php if ($subTitle) : ?>
                                    <div class="bg-blue-dark text-white font-title text-sm leading-11 font-black p-8 md:p-10 min-h-40">
                                        <?php echo $subTitle; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($title) : ?>
                                    <div class="selection-blue-dark bg-blue text-white min-h-96 flex items-end justify-center font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                                        <?php echo $title; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($description) : ?>
                                    <div class="my-20 md:mb-30 px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                        <?php echo $description; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($price) : ?>
                                    <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                                        <?php echo $price; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if ($priceDescription) : ?>
                                    <div class="font-roboto font-normal text-sm leading-20">
                                        <?php echo $priceDescription; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
</div>
