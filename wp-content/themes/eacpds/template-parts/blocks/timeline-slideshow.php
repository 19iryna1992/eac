<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$main_title = get_field('title');
$main_description = get_field('timeline_description');
$timeline_button = get_field('timeline_button');
?>

<div class="acf-timeline-slideshow <?php echo $blockClass; ?> py-30 md:py-50 lg:py-100" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($main_title || $main_description) : ?>
            <div class="md:w-4/5 mb-20 md:mb-40">
                <?php if ($main_title) : ?>
                    <h2 class="mb-20 md:mb-40 text-h3 font-title font-black leading-11 md:leading-175 tracking-sm uppercase"><?php echo $main_title; ?></h2>
                <?php endif; ?>
                <?php if ($main_description) : ?>
                    <div class="text-md md:text-base leading-16 md:leading-20 text-black-light font-normal">
                        <?php echo $main_description; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if (have_rows('blocks')): ?>
            <div class="JS-time-line-slider-wrapper -mx-10">
                <div class="JS-time-line-slider -mx-10">
                    <?php while (have_rows('blocks')): the_row();
                        $imageLabel = get_sub_field('image_label');
                        $image = get_sub_field('image');
                        $years = get_sub_field('years');
                        $overlay_years = get_sub_field('overlay_years');
                        $title = get_sub_field('title');
                        $description = get_sub_field('timeline_content');
                        ?>
                        <div class="md:flex md:items-start px-10 overflow-hidden">
                            <div class="w-full md:max-w-480 mb-20 md:mb-0 md:mr-60 m-x-auto relative overflow-hidden">
                                <?php if ($image) : ?>
                                    <?php echo wp_get_attachment_image($image, 'timeline-img', false, ['class' => 'object-cover w-full md:h-full']); ?>
                                <?php endif; ?>
                                <?php if ($imageLabel) : ?>
                                    <span class="w-full text-center text-h1-xs md:text-h2 lg:text-h1-xs xl:text-h1-4xs font-title tracking-sm leading-9 text-white absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-10"><?php echo $imageLabel; ?></span>
                                    <span class="min-w-280 min-h-280 absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 bg-blue-dark bg-opacity-60 filter blur-3xl"></span>
                                <?php endif; ?>
                            </div>
                            <div>
                                <?php if ($years) : ?>
                                    <div class="text-base leading-11 font-title text-blue font-black tracking-sm mb-10 md:mb-20">
                                        <span class="inline md:hidden">+</span>
                                        <?php echo $years; ?>
                                    </div>
                                <?php endif; ?>
                                <div class="relative">
                                    <?php if ($title) : ?>
                                        <h3 class="text-h6 md:text-h3 leading-15 md:leading-11 md:mb-20 tracking-sm font-title font-black normal-case md:uppercase"><?php echo $title; ?></h3>
                                    <?php endif; ?>
                                    <?php if ($description) : ?>
                                        <div class="lg:w-3/4 text-sm leading-20 font-normal">
                                            <?php echo $description; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($overlay_years) : ?>
                                        <span class="w-full text-center lg:w-auto tracking-sm c-large-symbols text-h1-md md:text-h1-2md leading-9 font-title absolute left-0 top-20 lg:top-76"><?php echo $overlay_years; ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
                <div class="mb-10 mt-5 flex justify-start md:justify-end items-center">
                    <div class="relative min-w-180 md:min-w-280 mr-40 relative">
                        <div class="JS-time-line-slider-dots flex justify-between"></div>
                        <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 block w-full max-w-100 md:max-w-200 h-2 rounded overflow-hidden bg-grey-light bg-gradient-to-r from-blue to-blue bg-no-repeat JS-progress-timeline"
                             role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="JS-time-line-slider-arrows"></div>
                </div>
            </div>

            
            <?php if($timeline_button): ?>
            <div class="text-center">
                <a href="<?php echo $timeline_button['url'] ?>" class="c-btn c-btn--xs c-btn--primary"><?php echo $timeline_button['title']?></a>
            </div>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>
