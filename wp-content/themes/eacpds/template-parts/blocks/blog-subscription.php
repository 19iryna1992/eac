<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$title = get_field('bs_title') ? get_field('bs_title') : get_field('bs_title', 'option');
$description = get_field('bs_description') ? get_field('bs_description') : get_field('bs_description', 'option');
$form = get_field('bs_form_shortcode') ? get_field('bs_form_shortcode') : get_field('bs_form_shortcode', 'option');
?>

<div class="acf-blog-subscription p-20 mx-auto <?php echo $blockClass; ?>"
     id="<?php echo esc_attr($id); ?>">
    <div class="pt-76 pb-60 md:pb-100 bg-gradient-to-t from-black via-blue-dark to-blue">
        <div class="pl-20 pr-20 md:pl-60 lg:pl-100 2xl:pl-140 xl:max-w-4/5 mb-30 md:mb-50">
            <?php if ($title) : ?>
                <h2 class="text-h3 md:text-h1 font-title font-black text-white uppercase md:leading-9 md:leading-14"><?php echo $title; ?></h2>
            <?php endif; ?>
            <?php if ($description) : ?>
                <div class="text-sm md:text-base font-default text-white font-medium leading-14 leading-23">
                    <?php echo $description; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>