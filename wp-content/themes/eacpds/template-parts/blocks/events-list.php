<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$today = date('Ymd');
$title = get_field('events_title');
$event_arg = array(
    'post_type' => 'events',
    'posts_per_page' => 4,
    'meta_key' => 'event_date',
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => 'event_date',
            'compare' => '>=',
            'value' => $today,
        )
    ),
);
$events_query = new WP_Query($event_arg);


?>

<div class="acf-events-list pt-60 lg:pt-100 md:pb-100 lg:pb-160 <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($title) : ?>
            <h2 class="text-black font-title uppercase leading-11 tracking-normal mb-50 lg:mb-70"><?php echo $title ?></h2>
        <?php endif; ?>
        <?php if ($events_query->have_posts()) : ?>
            <div class="JS--post-container" data-pages="<?php echo $events_query->max_num_pages; ?>">
                <?php while ($events_query->have_posts()) : $events_query->the_post(); ?>
                    <?php get_template_part('template-parts/components/event-card') ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <?php if ($events_query->max_num_pages > 1): ?>
                <div class="JS--load--container">
                    <div class="c-load-more text-center d-flex justify-content-center mt-60">
                        <a class="c-btn c-btn-- c-btn--more c-btn--secondary JS--load-more"
                           data-current-post-type="<?php echo 'events' ?>"
                           href="javascript:void(0);">
                            <?php _e('Load More', 'sage'); ?>
                            <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>
