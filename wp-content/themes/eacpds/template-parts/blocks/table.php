<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

// ACF vars

$table_count_column = get_field('table_count_columns');

// Table First
$column_title_first = get_field('column_title_first');

// Table Second
$title_second_col = get_field('title_table_second');
$description_second_col = get_field('description_table_second');
$price_second_col = get_field('price_table_second');
$price_second_first_col = get_field('price_label_second');
$table_btn_second = get_field('table_btn_second');
$popular_column_second = get_field('popular_column_second');
$column_title_second = get_field('column_title_second');

// Table Third
$title_third_col = get_field('title_table_third');
$description_third_col = get_field('description_table_third');
$price_third_col = get_field('price_table_third');
$price_label_third_col = get_field('price_label_third');
$table_btn_third = get_field('table_btn_third');
$popular_column_third = get_field('popular_column_third');
$column_title_third = get_field('column_title_third');

// Table Four
$title_fourth_col = get_field('title_table_fourth');
$description_fourth_col = get_field('description_table_fourth');
$price_fourth_col = get_field('price_table_fourth');
$price_fourth_first_col = get_field('price_label_fourth');
$table_btn_fourth = get_field('table_btn_fourth');
$popular_column_fourth = get_field('popular_column_fourth');
$column_title_fourth = get_field('column_title_fourth');

// Table Five
$title_fifth_col = get_field('title_table_fifth');
$description_fifth_col = get_field('description_table_fifth');
$price_fifth_col = get_field('price_table_fifth');
$price_fifth_first_col = get_field('price_label_fifth');
$table_btn_fifth = get_field('table_btn_fifth');
$popular_column_fifth = get_field('popular_column_fifth');
$column_title_fifth = get_field('column_title_fifth');

$tableTitle = get_field('table_title');
?>

<div class="acf-table overflow-hidden <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if($tableTitle) : ?>
        <h2 class="xl:mb-60 font-title text-h3 md:text-h3-xl lg:text-h2 uppercase text-center leading-11 tracking-sm"><?php echo $tableTitle; ?></h2>
        <?php endif; ?>
            <!-- START: desktop main table  -->
            <div class="hidden laptop:block"> 
                <div class="grid <?= $table_count_column == 'five' ? 'grid-cols-5' : 'grid-cols-4' ?> w-full mb-70">
                    <div>  <!--for empty firs column--> </div> 
                    <div class="text-center mb-40 md:mb-0 <?= !$popular_column_second ? 'mt-40' : ' ' ?>">
                        <?php if ($popular_column_second): ?>
                            <div class="bg-blue-dark text-white">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <div class=" <?= $popular_column_second ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php if ($title_second_col): ?>
                                <?php echo $title_second_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php if ($description_second_col): ?>
                                <?php echo $description_second_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                            <?php if ($price_second_col): ?>
                                <?php echo $price_second_col ?>
                            <?php endif ?>
                        </div>
                        <div class="font-roboto font-normal text-sm leading-20">
                            <?php if ($price_second_first_col): ?>
                                <?php echo $price_second_first_col ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="text-center mb-40 md:mb-0 <?= !$popular_column_third ? 'mt-40' : ' ' ?>">
                        <?php if ($popular_column_third): ?>
                            <div class="bg-blue-dark text-white font-title text-sm leading-11 font-black p-8 md:p-10 min-h-40">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <div class="selection-blue-dark <?= $popular_column_third ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php if ($title_third_col): ?>
                                <?php echo $title_third_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php if ($description_third_col): ?>
                                <?php echo $description_third_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                            <?php if ($price_third_col): ?>
                                <?php echo $price_third_col ?>
                            <?php endif ?>
                        </div>
                        <div class="font-roboto font-normal text-sm leading-20">
                            <?php if ($price_label_third_col): ?>
                                <?php echo $price_label_third_col ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="text-center mb-40 md:mb-0 <?= !$popular_column_fourth ? 'mt-40' : '' ?>">
                        <?php if ($popular_column_fourth): ?>
                            <div class="bg-blue-dark text-white">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <div class=" <?= $popular_column_fourth ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php if ($title_fourth_col): ?>
                                <?php echo $title_fourth_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php if ($description_fourth_col): ?>
                                <?php echo $description_fourth_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                            <?php if ($price_fourth_col): ?>
                                <?php echo $price_fourth_col ?>
                            <?php endif ?>
                        </div>
                        <div class="font-roboto font-normal text-sm leading-20">
                            <?php if ($price_fourth_first_col): ?>
                                <?php echo $price_fourth_first_col ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <?php if ($table_count_column == 'five'): ?>
                        <div class="text-center mb-40 md:mb-0 <?= !$popular_column_fifth  ?  'mt-40' : '' ?>">
                            <?php if ($popular_column_fifth): ?>
                                <div class="bg-blue-dark text-white">
                                    <?php _e('Most Popular', 'eacpds') ?>
                                </div>
                            <?php endif ?>
                            <div class="<?= $popular_column_fifth ? 'bg-blue text-white' : 'bg-white' ?> min-h-96 flex items-end justify-center font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                                <?php if ($title_fifth_col): ?>
                                    <?php echo $title_fifth_col ?>
                                <?php endif ?>
                            </div>
                            <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                <?php if ($description_fifth_col): ?>
                                    <?php echo $description_fifth_col ?>
                                <?php endif ?>
                            </div>
                            <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                                <?php if ($price_fifth_col): ?>
                                    <?php echo $price_fifth_col ?>
                                <?php endif ?>
                            </div>
                            <div class="font-roboto font-normal text-sm leading-20">
                                <?php if ($price_fifth_first_col): ?>
                                    <?php echo $price_fifth_first_col ?>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div>
                    <?php if (have_rows('table_row')): ?>
                        <table class="c-table-price w-full">
                            <tbody>
                                <tr>
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/5' : 'w-1/4' ?>"><?= $column_title_first ? $column_title_first : ' ' ?></th>
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/5' : 'w-1/4' ?>"><?= $column_title_second ? $column_title_second : ' ' ?></th>
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/5' : 'w-1/4' ?>"><?= $column_title_third ? $column_title_third : ' ' ?></th>
                                    <th class="<?= $table_count_column == 'five' ? 'w-1/5' : 'w-1/4' ?>"><?= $column_title_fourth ? $column_title_fourth : ' ' ?></th>
                                    <?php if ($table_count_column == 'five'): ?>
                                        <th class="<?= $table_count_column == 'five' ? 'w-1/5' : 'w-1/4' ?>"><?= $column_title_fifth ? $column_title_fifth : ' ' ?></th>
                                    <?php endif; ?>
                                </tr>
                                <?php while (have_rows('table_row')): the_row(); ?>
                                    <tr>
                                        <?php get_template_part('template-parts/components/table/table-row') ?>
                                    </tr>
                                <?php endwhile; ?>
                                <tr class="c-table-price__tr-btn bg-white">
                                    <td> <!--for empty firs column--> </td> 
                                    <td>
                                        <?php if ($table_btn_second): ?>
                                            <a class="c-btn c-btn--more c-btn--secondary <?= $table_count_column == 'five' ? 'c-btn--ex-xs' : 'c-btn--sm' ?>"
                                            href="<?php echo $table_btn_second['url'] ?>">
                                                <?php echo $table_btn_second['title'] ?>
                                                <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($table_btn_third): ?>
                                            <a class="c-btn c-btn--more c-btn--secondary <?= $table_count_column == 'five' ? 'c-btn--ex-xs' : 'c-btn--sm' ?>"
                                            href="<?php echo $table_btn_third['url'] ?>">
                                                <?php echo $table_btn_third['title'] ?>
                                                <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($table_btn_fourth): ?>
                                            <a class="c-btn c-btn--more c-btn--secondary <?= $table_count_column == 'five' ? 'c-btn--ex-xs' : 'c-btn--sm' ?>"
                                            href="<?php echo $table_btn_fourth['url'] ?>">
                                                <?php echo $table_btn_fourth['title'] ?>
                                                <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($table_count_column == 'five'): ?>
                                            <?php if ($table_btn_fifth): ?>
                                                <a class="c-btn c-btn--more c-btn--secondary <?= $table_count_column == 'five' ? 'c-btn--ex-xs' : 'c-btn--sm' ?>"
                                                href="<?php echo $table_btn_fifth['url'] ?>">
                                                    <?php echo $table_btn_fifth['title'] ?>
                                                    <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END: desktop main table  -->

            <!-- START: mobile second table  -->
            <div class="block laptop:hidden"> 
                <div class="grid grid-cols-1 md:grid-cols-2 w-full"> 
                    <div class="text-center mb-40 md:mb-0 lg:mt-40">
                        <?php if ($popular_column_second): ?>
                            <div class="bg-blue-dark text-white">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <div class=" <?= $popular_column_second ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php if ($title_second_col): ?>
                                <?php echo $title_second_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php if ($description_second_col): ?>
                                <?php echo $description_second_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                            <?php if ($price_second_col): ?>
                                <?php echo $price_second_col ?>
                            <?php endif ?>
                        </div>
                        <div class="font-roboto font-normal text-sm leading-20">
                            <?php if ($price_second_first_col): ?>
                                <?php echo $price_second_first_col ?>
                            <?php endif ?>
                        </div>
                    </div>  
                </div>

                <div>
                    <?php if (have_rows('table_row')): ?>
                        <table class="c-table-price w-full">
                            <tbody>
                                <tr> 
                                    <th class="md:w-1/2"><?= $column_title_second ? $column_title_second : ' ' ?></th>
                                </tr>
                                <?php while (have_rows('table_row')): the_row(); ?>
                                    <tr>
                                        <?php get_template_part('template-parts/components/table/table-row-mobile/second') ?>
                                    </tr>
                                <?php endwhile; ?>
                                <tr class="c-table-price__tr-btn bg-white"> 
                                    <td class="w-full px-20 pt-20 pb-40" colspan="2">
                                        <?php if ($table_btn_second): ?>
                                            <a class="c-btn c-btn--sm c-btn--more c-btn--secondary"
                                            href="<?php echo $table_btn_second['url'] ?>">
                                                <?php echo $table_btn_second['title'] ?>
                                                <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END: mobile second table  -->

            <!-- START: mobile third table  --> 
            <div class="block laptop:hidden"> 
                <div class="grid grid-cols-1 md:grid-cols-2 w-full"> 
                    <div class="text-center mb-40 md:mb-0">
                        <?php if ($popular_column_third): ?>
                            <div class="bg-blue-dark text-white font-title text-sm leading-11 font-black p-8 md:p-10 min-h-40">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <div class="selection-blue-dark <?= $popular_column_third ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php if ($title_third_col): ?>
                                <?php echo $title_third_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php if ($description_third_col): ?>
                                <?php echo $description_third_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                            <?php if ($price_third_col): ?>
                                <?php echo $price_third_col ?>
                            <?php endif ?>
                        </div>
                        <div class="font-roboto font-normal text-sm leading-20">
                            <?php if ($price_label_third_col): ?>
                                <?php echo $price_label_third_col ?>
                            <?php endif ?>
                        </div>
                    </div> 
                </div>

                <div>
                    <?php if (have_rows('table_row')): ?>
                        <table class="c-table-price w-full">
                            <tbody>
                                <tr>                                      
                                    <th class="md:w-1/2"><?= $column_title_third ? $column_title_third : ' ' ?></th>
                                </tr>
                                <?php while (have_rows('table_row')): the_row(); ?>
                                    <tr>
                                        <?php get_template_part('template-parts/components/table/table-row-mobile/third') ?>
                                    </tr>
                                <?php endwhile; ?>
                                <tr class="c-table-price__tr-btn bg-white"> 
                                    <td class="w-full px-20 pt-20 pb-40" colspan="2"> 
                                        <?php if ($table_btn_third): ?>
                                            <a class="c-btn c-btn--sm c-btn--more c-btn--secondary"
                                            href="<?php echo $table_btn_third['url'] ?>">
                                                <?php echo $table_btn_third['title'] ?>
                                                <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </td> 
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END: mobile third table  -->

            <!-- START: mobile fourth table  --> 
            <div class="block laptop:hidden"> 
                <div class="grid grid-cols-1 md:grid-cols-2 w-full"> 
                    <div class="text-center mb-40 md:mb-0 lg:mt-40">
                        <?php if ($popular_column_fourth): ?>
                            <div class="bg-blue-dark text-white">
                                <?php _e('Most Popular', 'eacpds') ?>
                            </div>
                        <?php endif ?>
                        <div class=" <?= $popular_column_fourth ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                            <?php if ($title_fourth_col): ?>
                                <?php echo $title_fourth_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                            <?php if ($description_fourth_col): ?>
                                <?php echo $description_fourth_col ?>
                            <?php endif ?>
                        </div>
                        <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                            <?php if ($price_fourth_col): ?>
                                <?php echo $price_fourth_col ?>
                            <?php endif ?>
                        </div>
                        <div class="font-roboto font-normal text-sm leading-20">
                            <?php if ($price_fourth_first_col): ?>
                                <?php echo $price_fourth_first_col ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

                <div>
                    <?php if (have_rows('table_row')): ?>
                        <table class="c-table-price w-full">
                            <tbody>
                                <tr>                                      
                                    <th class="md:w-1/2"><?= $column_title_fourth ? $column_title_fourth : ' ' ?></th>
                                </tr>
                                <?php while (have_rows('table_row')): the_row(); ?>
                                    <tr>
                                        <?php get_template_part('template-parts/components/table/table-row-mobile/fourth') ?>
                                    </tr>
                                <?php endwhile; ?>
                                <tr class="c-table-price__tr-btn bg-white"> 
                                    <td class="w-full px-20 pt-20 pb-40" colspan="2"> 
                                        <?php if ($table_btn_fourth): ?>
                                            <a class="c-btn c-btn--sm c-btn--more c-btn--secondary"
                                            href="<?php echo $table_btn_fourth['url'] ?>">
                                                <?php echo $table_btn_fourth['title'] ?>
                                                <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </td> 
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END: mobile fourth table  -->

            <!-- START: mobile fifth table  --> 
            <div class="block laptop:hidden"> 
                <div class="grid grid-cols-1 md:grid-cols-2 w-full"> 
                    <?php if ($table_count_column == 'five'): ?>
                        <div class="text-center mb-40 md:mb-0 lg:mt-40">
                            <?php if ($popular_column_fifth): ?>
                                <div class="bg-blue-dark text-white">
                                    <?php _e('Most Popular', 'eacpds') ?>
                                </div>
                            <?php endif ?>
                            <div class="<?= $popular_column_fifth ? 'bg-blue text-white' : 'bg-white' ?> font-title text-h3 leading-11 mt-2 mb-6 md:mb-30 px-10 py-25 md:p-10 font-bold">
                                <?php if ($title_fifth_col): ?>
                                    <?php echo $title_fifth_col ?>
                                <?php endif ?>
                            </div>
                            <div class="mb-20 md:mb-30 md:px-20 font-roboto font-normal text-sm md:leading-12 leading-16">
                                <?php if ($description_fifth_col): ?>
                                    <?php echo $description_fifth_col ?>
                                <?php endif ?>
                            </div>
                            <div class="mb-10 font-title font-black text-blue text-h3-lg leading-11">
                                <?php if ($price_fifth_col): ?>
                                    <?php echo $price_fifth_col ?>
                                <?php endif ?>
                            </div>
                            <div class="font-roboto font-normal text-sm leading-20">
                                <?php if ($price_fifth_first_col): ?>
                                    <?php echo $price_fifth_first_col ?>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div>
                    <?php if (have_rows('table_row')): ?>
                        <table class="c-table-price w-full">
                            <tbody>
                                <tr>                                       
                                    <?php if ($table_count_column == 'five'): ?>
                                        <th class="md:w-1/2"><?= $column_title_fifth ? $column_title_fifth : ' ' ?></th>
                                    <?php endif; ?>
                                </tr>
                                <?php while (have_rows('table_row')): the_row(); ?>
                                    <tr>
                                        <?php get_template_part('template-parts/components/table/table-row-mobile/fifth') ?>
                                    </tr>
                                <?php endwhile; ?>
                                <tr class="c-table-price__tr-btn bg-white"> 
                                    <td class="w-full px-20 pt-20 pb-40" colspan="2"> 
                                        <?php if ($table_count_column == 'five'): ?>
                                            <?php if ($table_btn_fifth): ?>
                                                <a class="c-btn c-btn--sm c-btn--more c-btn--secondary"
                                                href="<?php echo $table_btn_fifth['url'] ?>">
                                                    <?php echo $table_btn_fifth['title'] ?>
                                                    <?php echo get_file_icon_arrow_right('colorCurrent w-24 h-18'); ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td> 
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
            <!-- END: mobile fifth table  -->
    </div>
</div>