<?php

$blockClass = $block["className"] ?? '';


$spacer = get_field('spacer');
?>

<div class="acf-spacer <?php echo $blockClass; ?>">
    <div class="container">
        <div class="<?php echo $spacer; ?>"></div>
    </div>
</div>