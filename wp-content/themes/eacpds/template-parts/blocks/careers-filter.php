<?php
$title = get_field('careers_filter_title' );
$description = get_field('careers_filter_description');
$posts_per_page = 8;
$careers_args = array(
    'post_type' => 'careers',
    'post_status' => 'publish',
    'posts_per_page' => $posts_per_page,
    'no_found_rows' => true
);
$careers_posts = new WP_Query($careers_args);

$count_careers = wp_count_posts('careers');
$publish_careers_count = $count_careers->publish;

$locations = get_terms([
    'taxonomy' => 'locations',
    'hide_empty' => true,
    'fields' => 'id=>name'
]);

$positions = get_terms([
    'taxonomy' => 'positions',
    'hide_empty' => true,
    'fields' => 'id=>name'
]);
?>

<section class="s-careers-filter">
    <div class="container">
        <h2 class="mb-20 md:mb-40 text-h3 font-title font-black leading-11 md:leading-175 tracking-sm uppercase"><?php echo $title; ?></h2>
        <div class="mb-40 text-md md:text-base leading-16 md:leading-20 text-black-light font-normal">
            <?php echo $description; ?>
        </div>
    </div>

    <div class="container">
        <form method="POST" class="JS--careers-filter-form">
            <div class="lg:flex lg:justify-between mb-40">
                <div class="c-select JS-select-wrapper relative">
                    <select name="positions" id="positions" class="JS-select hidden">
                        <option value=""><?php _e('All Positions', 'eacpds'); ?></option>
                        <?php foreach($positions as $pID => $pName) : ?>
                            <option value="<?php echo $pID; ?>"><?php echo $pName; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="c-select JS-select-wrapper relative">
                    <select name="clocations" id="locations" class="JS-select hidden">
                        <option value=""><?php _e('All Locations', 'eacpds'); ?></option>
                        <?php foreach($locations as $lID => $lName) : ?>
                            <option value="<?php echo $lID; ?>"><?php echo $lName; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="text-center">
                    <button type="submit" class="appearance-none c-btn c-btn--xs c-btn--primary block max-w-full md:max-w-260 md:inline-block"><?php _e('Show', 'eacpds'); ?></button>
                </div>

            </div>
        </form>

    </div>

    <div class="container">
        <?php if ($careers_posts->have_posts()) : ?>
            <div class="flex flex-wrap bg-grey-light mobile-md:-mx-20 py-10 lg:p-20 JS--career-cards">
                <?php while ($careers_posts->have_posts()) : $careers_posts->the_post(); ?>
                    <?php get_template_part('template-parts/components/career-card'); ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>

        <div class="text-center" <?php echo ($publish_careers_count <= $posts_per_page) ? ' style="display:none;"' : ''; ?>>
            <a href="javascript:void();"
               class="c-btn-outline my-40 c-btn-outline--lg c-btn-outline--primary text-grey hover:text-white JS--load-careers"><?php _e('Show more', 'eacpds'); ?></a>
        </div>


    </div>

</section>
