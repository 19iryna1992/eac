<?php

$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$chatIcon = get_field('lc_icon') ? get_field('lc_icon') : get_field('lc_icon', 'option');
$chatTitle = get_field('lc_title') ? get_field('lc_title') : get_field('lc_title', 'option');
$chatDescription = get_field('lc_description') ? get_field('lc_description') : get_field('lc_description', 'option');
$chatBtn = get_field('lc_btn') ? get_field('lc_btn') : get_field('lc_btn', 'option');

$ctaIcon = get_field('cta_icon') ? get_field('cta_icon') : get_field('cta_icon', 'option');
$ctaTitle = get_field('cta_title') ? get_field('cta_title') : get_field('cta_title', 'option');
$ctaDescription = get_field('cta_description') ? get_field('cta_description') : get_field('cta_description', 'option');
?>

<div class="acf-live-chat-cta <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="laptop:flex">
            <div class="md:flex md:mb-60 laptop:w-1/2">
                <?php if ($chatIcon) : ?>
                    <div class="bg-blue-dark flex justify-center items-center mb-20 pt-40 pb-40 sm:max-w-260 md:w-1/2 md:mb-0 sm:h-260">
                        <?php echo wp_get_attachment_image($chatIcon); ?>
                    </div>
                <?php endif; ?>
                <div class="md:w-1/2 md:ml-20 md:relative">
                    <?php if ($chatTitle) : ?>
                        <h3 class="font-title text-h3 font-bold text-black leading-11 laptop:leading-14 laptop:mb-10">
                            <?php echo $chatTitle ?>
                        </h3>
                    <?php endif; ?>
                    <?php if ($chatDescription) : ?>
                        <div class="font-default text-sm md:text-md font-normal leading-16 md:leading-12 tracking-sm mb-20">
                            <?php echo $chatDescription ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($chatBtn) : ?>
                        <a href="#"
                           class="c-btn c-btn--xs c-btn--more c-btn--primary mb-60 md:mb-0 md:absolute md:bottom-0 laptop:bottom-25">
                            <?php echo $chatBtn; ?>
                            <svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z"
                                      fill="currentColor"></path>
                            </svg>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="md:flex md:mb-60 laptop:w-1/2">
                <?php if ($ctaIcon) : ?>
                    <div class="bg-blue-dark flex justify-center items-center mb-20 pt-40 pb-40 sm:max-w-260 md:w-1/2 md:mb-0 sm:h-260">
                        <?php echo wp_get_attachment_image($ctaIcon); ?>
                    </div>
                <?php endif; ?>
                <div class="md:w-1/2 md:ml-20 md:relative">
                    <?php if ($ctaTitle) : ?>
                        <h3 class="font-title text-h3 font-bold text-black leading-11 lg:leading-175">
                            <?php echo $ctaTitle ?>
                        </h3>
                    <?php endif; ?>
                    <?php if ($ctaDescription) : ?>
                        <div class="font-default text-sm md:text-md font-normal leading-16 md:leading-12 tracking-sm mb-20">
                            <?php echo $ctaDescription ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>