<?php
$id = $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$blockClass = $block["className"] ?? '';

$textContent = get_field('simple_text');
?>

<div class="acf-simple-text <?php echo $blockClass; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="c-wysiwyg">
            <?php echo $textContent; ?>
        </div>
    </div>
</div>