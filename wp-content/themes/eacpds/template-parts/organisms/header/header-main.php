<?php

if (!defined('ABSPATH')) exit;

$menuBtn = get_field('menu_btn', 'option');
?>
<div class="o-header__main"> 
    <div class="flex items-center laptop:items-stretch justify-between relative <?php if( !is_page_template(array('templates/template-landing.php')) ) { echo 'pt-45 pr-15 md:pr-40'; } ?>  pb-15 pl-20 md:pl-40 laptop:pt-0 laptop:pr-0 laptop:pb-0 laptop:pl-20 3xl:pl-100">
        <div class="o-header__logo laptop:self-center">
            <?php the_custom_logo(); ?>
        </div>

        <?php if(!is_page_template(array('templates/template-landing.php'))) : ?>
            <div class="laptop:flex-1">
                <div class="o-header__btn laptop:hidden JS-mob-toggle">
                    <div class="c-mob-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="c-mob-btn__text">
                        <span class="c-mob-btn__text--open">MENU</span>
                        <span class="c-mob-btn__text--close">CLOSE</span>
                    </div>
                </div>
                <?php get_template_part('template-parts/components/navigation/main-menu'); ?>
            </div>
        <?php endif; ?>

        <div class="hidden xl:flex items-center">
            <?php get_template_part('searchform'); ?>
        </div>

        <div class="<?php if( !is_page_template(array('templates/template-landing.php')) ) { echo 'hidden'; } ?>  laptop:block o-header__contact-btn">
            <?php if ($menuBtn) : ?>
                <a href="<?php echo $menuBtn['url']; ?>"
                    class="c-btn c-btn--xs c-btn--primary block max-w-full md:max-w-260 md:inline-block uppercase font-roboto"
                    target="<?php echo $menuBtn['target']; ?>"><?php echo $menuBtn['title']; ?>
                    <span> →</span>
                </a>
            <?php endif; ?>
        </div>
    </div>
 
</div>