<?php

if (!defined('ABSPATH')) exit;

?>

<div class="o-footer__bottom mt-45 md:mt-100 lg:mt-150">
    <div class="container">
        <div class="lg:mx-auto lg:w-11/12">
            <?php get_template_part('template-parts/components/navigation/bottom-footer-menu'); ?>
        </div>
    </div>
</div>