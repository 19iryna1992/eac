<?php

if (!defined('ABSPATH')) exit;

?>

<div class="o-footer__top mb-30 md:mb-70 lg:mb-100">
    <div class="container">
        <div class="flex md:justify-center -mx-15">
            <?php if (is_active_sidebar('footer-1')) : ?>
                <div class="w-full md:w-1/3 lg:w-1/5 px-15">
                    <?php dynamic_sidebar('footer-1'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-2')) : ?>
                <div class="w-full md:w-1/3 lg:w-1/5 px-15">
                    <?php dynamic_sidebar('footer-2'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-3')) : ?>
                <div class="w-full md:w-1/3 lg:w-1/5 px-15">
                    <?php dynamic_sidebar('footer-3'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-4')) : ?>
                <div class="w-full md:w-1/3 lg:w-1/5 px-15">
                    <?php dynamic_sidebar('footer-4'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-5')) : ?>
                <div class="w-full md:w-1/3 lg:w-1/5 px-15">
                    <?php dynamic_sidebar('footer-5'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>