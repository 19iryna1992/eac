<?php

if (!defined('ABSPATH')) exit;

?>
<div class="o-footer__middle">
    <div class="container">
        <div class="lg:mx-auto lg:w-11/12"> 
            <?php if (is_active_sidebar('footer-6')) : ?>
                <div class="mb-60">
                    <?php dynamic_sidebar('footer-6'); ?>
                </div>
            <?php endif; ?>

            <?php if (is_active_sidebar('footer-7')) : ?>
                <div class="o-footer__contacts flex flex-wrap">
                    <?php dynamic_sidebar('footer-7'); ?>
                </div>
            <?php endif; ?> 
        </div>
    </div>
</div>