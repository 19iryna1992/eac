<?php

namespace Inc;

/**
 * gut functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gut
 */

if (!defined('ABSPATH')) exit;

define('PREVIEWBLOCKIMGDIR', get_stylesheet_directory_uri() . '/img/blocks-previews/');
define('STATICBLOCKIMAGEDIR', get_stylesheet_directory_uri() . '/img/blocks/');

if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ . '/vendor/autoload.php';
}


use Inc\Blocks\AnimatedIconColumns;
use Inc\Blocks\BlogSubscription;
use Inc\Blocks\Buttons;
use Inc\Blocks\Callouts;
use Inc\Blocks\CareersFilter;
use Inc\Blocks\CareersPosts;
use Inc\Blocks\ContactForm;
use Inc\Blocks\EventsList;
use Inc\Blocks\ExpandCollapse;
use Inc\Blocks\FeaturedContentTabCards;
use Inc\Blocks\FeaturedProducts;
use Inc\Blocks\FeaturedText;
use Inc\Blocks\IconColumnsGrid;
use Inc\Blocks\ImageGrid;
use Inc\Blocks\InnerBlock;
use Inc\Blocks\Interior;
use Inc\Blocks\LiveChatCta;
use Inc\Blocks\Logos;
use Inc\Blocks\MoreSectionNavigation;
use Inc\Blocks\NewsLetterSignUp;
use Inc\Blocks\OfferingsAnimatedSlideshow;
use Inc\Blocks\PostAuthor;
use Inc\Blocks\PostTypesTabs;
use Inc\Blocks\ProductTrialsList;
use Inc\Blocks\RelatedNewsArticles;
use Inc\Blocks\ShareIcons;
use Inc\Blocks\SimpleText;
use Inc\Blocks\SlideshowHeader;
use Inc\Blocks\Spacer;
use Inc\Blocks\Statistics;
use Inc\Blocks\Table;
use Inc\Blocks\TableAccordion;
use Inc\Blocks\TableHeaders;
use Inc\Blocks\TeamSpotlight;
use Inc\Blocks\Testimonials;
use Inc\Blocks\TextImage;
use Inc\Blocks\TextImageLinks;
use Inc\Blocks\TimelineSlideshow;
use Inc\Blocks\TrainingCourses;
use Inc\Blocks\FeaturedVideo;
use Inc\PostType\Careers;
use Inc\PostType\Events;
use Inc\PostType\Faqs;
use Inc\PostType\Products;
use Inc\PostType\Services;
use Inc\PostType\Solutions;
use Inc\PostType\TeamMembers;
use Inc\PostType\TrainingCalendar;
use StoutLogic\AcfBuilder\FieldsBuilder;

require_once __DIR__ . '/inc/settings/settings.php';
require_once __DIR__ . '/inc/acf/acf.php';
require_once __DIR__ . '/inc/admin/admin.php';
require_once __DIR__ . '/inc/utils/utils.php';
require_once __DIR__ . '/inc/posts/posts.php';
require_once __DIR__ . '/inc/plugins/plugins.php';
require_once __DIR__ . '/inc/widget/widgets.php';
require_once __DIR__ . '/inc/ajax-functions/ajax-functions.php';


/**
 * Initialize Post Types
 */
add_action('init', function () {
    Products::getInstance()->register();
    Services::getInstance()->register();
    Solutions::getInstance()->register();
    TeamMembers::getInstance()->register();
    Faqs::getInstance()->register();
    Careers::getInstance()->register();
    TrainingCalendar::getInstance()->register();
    Events::getInstance()->register();
    \Inc\PostType\Testimonials::getInstance()->register();
});

/**
 * Initialize ACF Builder
 */

add_action('acf/init', function () {
    //InnerBlock::getInstance();
    //\Inc\Blocks\Button::getInstance();
    \Inc\Blocks\FAQS::getInstance();
    ExpandCollapse::getInstance();
    Interior::getInstance();
    SlideshowHeader::getInstance();
    Logos::getInstance();
    FeaturedText::getInstance();
    AnimatedIconColumns::getInstance();
    Callouts::getInstance();
    IconColumnsGrid::getInstance();
    TextImageLinks::getInstance();
    TimelineSlideshow::getInstance();
    Statistics::getInstance();
    OfferingsAnimatedSlideshow::getInstance();
    TeamSpotlight::getInstance();
    FeaturedContentTabCards::getInstance();
    Spacer::getInstance();
    TextImage::getInstance();
    Testimonials::getInstance();
    MoreSectionNavigation::getInstance();
    RelatedNewsArticles::getInstance();
    PostTypesTabs::getInstance();
    NewsLetterSignUp::getInstance();
    ContactForm::getInstance();
    LiveChatCta::getInstance();
    BlogSubscription::getInstance();
    ImageGrid::getInstance();
    CareersPosts::getInstance();
    FeaturedProducts::getInstance();
    EventsList::getInstance();
    ProductTrialsList::getInstance();
    TrainingCourses::getInstance();
    FeaturedVideo::getInstance();
    Table::getInstance();
    TableAccordion::getInstance();
    TableHeaders::getInstance();
    ShareIcons::getInstance();
    PostAuthor::getInstance();
    SimpleText::getInstance();
    Buttons::getInstance();
    CareersFilter::getInstance();
});


add_action('init', function () {
    if (!function_exists('acf_add_local_field_group')) {
        return;
    }

    collect(glob(__DIR__ . '/inc/Fields/*.php'))->map(function ($field) {
        return require_once($field);
    })->map(function ($field) {
        if ($field instanceof FieldsBuilder) {
            acf_add_local_field_group($field->build());
        }
    });
});

add_action('after_switch_theme', 'rfp_flush_rewrite_rules');

function rfp_flush_rewrite_rules()
{
    flush_rewrite_rules();
}
