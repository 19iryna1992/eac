<form class="search mx-10 flex items-center" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
    <input class="search-input text-grey-dark text-sm font-roboto font-normal p-5 border border-transparent focus:border-blue border-solid" type="search" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php _e( 'Type keywords and press enter', 'eacpds' ); ?>">
    <button class="search-submit text-blue hover:text-blue-dark cursor-pointer outline-none" type="submit" role="button">
        <?php echo get_search_icon('w-24 h-18'); ?>
    </button>
</form>