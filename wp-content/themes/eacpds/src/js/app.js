global.$ = global.jQuery = require('jquery');

import SmoothScroll from "./components/smoothScroll";
import CustomScroll from "./components/customScroll";
import ResourcesFilter from "./components/resources-filter";
import CareersFilter from "./components/careerFilter";

SmoothScroll();
CustomScroll();
ResourcesFilter();
CareersFilter();

require('./components/accordionTable');
require('./components/video');
require('./components/slideshowHeader');
require('./components/sliders');
require('./components/tabs');
require('./components/tabs');
require('./components/menuFooter');
require('./components/parallax');
require('./components/load-more-ajax');
require('./components/mainMenu');
require('./components/numberCounter');
require('./components/offerSlideshow');
require('./components/select');