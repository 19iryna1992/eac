if(document.querySelector(".o-footer__top") != null) {
    let windowWidth = window.innerWidth; 
    let menuFooterTop = document.querySelector(".o-footer__top");
    let menuFooterTopAccordionTitles = menuFooterTop.querySelectorAll(".JS--widget-title");
    let menuFooterTopAccordionContents = menuFooterTop.querySelectorAll(".c-widget > div");

     function listener(e)  {
        let accordionTitleCurrent = e.target;
        let accordionWrapper = accordionTitleCurrent.parentNode;
        let accordionContentCurrent = accordionTitleCurrent.nextSibling;

        let accordionMenuCurrent = accordionWrapper.querySelector(".menu");
        let accordionMenuCurrentHeight = accordionMenuCurrent.offsetHeight; 
        accordionContentCurrent.style.height = accordionMenuCurrentHeight + "px";

        menuFooterTopAccordionTitles.forEach(accordionTitle => {
            if(accordionTitle.classList.contains("active")) {
                accordionTitle.classList.remove("active"); 
            }
        });
         
        accordionTitleCurrent.classList.add("active");

        menuFooterTopAccordionContents.forEach(accordionContent => {
            if (accordionContent != accordionContentCurrent) {
                accordionContent.style.height = "0px";  
            }
        }); 
      };

    let checkShowmenuFooter = function(windowWidth) {   
        if (windowWidth < 768) {   
            menuFooterTopAccordionTitles.forEach(accordionTitle => {
                accordionTitle.classList.add("arrow");
                accordionTitle.classList.remove("active");

                accordionTitle.addEventListener("click", listener);
            }); 
        } else {
            menuFooterTopAccordionTitles.forEach(accordionTitle => {
                accordionTitle.classList.remove("arrow", "active");
                accordionTitle.removeEventListener("click", listener, false);
            }); 
        }
                    
        menuFooterTopAccordionContents.forEach(accordionContent => { 
            if (windowWidth < 768) { 
                accordionContent.style.height = "0px";
            } else {
                accordionContent.style.height = "auto";   
            }
        });
    };

    window.addEventListener("resize", () => {
        windowWidth = window.innerWidth; 
        checkShowmenuFooter(windowWidth);
    });
    checkShowmenuFooter(windowWidth);
} 