import simpleParallax from 'simple-parallax-js';

var imageSl = document.getElementsByClassName('JS-parallax-sl');
new simpleParallax(imageSl, {
	delay: .6,
	transition: 'cubic-bezier(0,0,0,1)',
    overflow: true
});

var imageMd = document.getElementsByClassName('JS-parallax-md');
new simpleParallax(imageMd, { 
	delay: .4,
	transition: 'cubic-bezier(.17,.67,.83,.67)',
    overflow: true
});

var imageQk = document.getElementsByClassName('JS-parallax-qk');
new simpleParallax(imageQk, {
	delay: .3,
	transition: 'cubic-bezier(.17,.67,.83,.67)',
    overflow: true
});