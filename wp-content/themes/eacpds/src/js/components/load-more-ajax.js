$(document).ready(function() {

    $(".JS--load-more").click(function () {
        LoadMore();
    });



    function LoadMore() {
        var maxPages = $('.JS--post-container').data('pages');
        var currentPostType = $('.JS--load-more').data('current-post-type');

        $.ajax({
            url: appLocations.admin_ajax,
            data: {
                action: "load_more_posts",
                page: appLocations.page,
                post_type: currentPostType,
            },
            type: 'POST',
            beforeSend: function () {
            },

            success: function (data) {
                $('.JS--post-container').append(data).fadeIn(250);
                appLocations.page++;
                if (appLocations.page == maxPages) {
                    $('.JS--load--container').remove();
                }
            },

            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    }

});