import videojs from 'video.js';
import "magnific-popup";

$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo').magnificPopup({
		type: 'iframe',
		iframe: {
			markup: '<div class="mfp-iframe-scaler">' +
				'<div class="mfp-close"></div>' +
				'<iframe class="mfp-iframe" frameborder="0" allow="autoplay"></iframe>' +
				'</div>',
			patterns: {
				vimeo: {
					index: 'vimeo.com/', 
					id: function(url) {        
							var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
							if ( !m || !m[5] ) return null;
							return m[5];
					},
					src: 'https://player.vimeo.com/video/%id%?autoplay=1'
			},
				youtube: {
					index: 'youtube.com/',
					id: 'v=',
					src: 'https://www.youtube.com/embed/%id%?autoplay=1'
				}
			}
		},
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});

	$('.open-popup-video').magnificPopup({
		type:'inline',
  	midClick: true,
		callbacks: {
			open: function() {
				function videoSection(item, index){
					var btn = videoBtn[index];
					var id = btn.getAttribute('data-id');
			
					btn.addEventListener("click", function () {
							videojs(id).play();
					});
			
					videojs(id).on('play', function () {
							btn.classList.remove('is-active');
							videojs(id).controls(true);
					});
			
					videojs(id).on('pause', function () {
							btn.classList.add('is-active');
							videojs(id).controls(false);
					});
				}
				
				var videoBtn = document.querySelectorAll('.js-video-btn');
				
				for(var i = 0 ;i < videoBtn.length ; i++){
						videoSection(videoBtn[i],i);
				}
			},
		},
	});
});



