$(document).ready(function() {	

    if($('.JS-decode').length) {

        $('.JS-decode').each(function(){
            var fullWord = $(this).text();
            var str_word = '';

            for (var i = 0; i < fullWord.length; i++) {
            str_word = str_word + '<span class="text-animation">' + fullWord.charAt(i) + '</span>'      
            }
            
            $(this).html(str_word);

            var curDec = $('.JS-slideshow-header').find('.slick-current').find('.JS-decode');
            if (curDec.length) {
            $('.slick-current').find('.JS-decode').addClass('JS-decode-active');
            decodeText();
            }
        });

        $('.JS-slideshow-header').on('beforeChange', function(slick, currentSlide){
            $('.JS-decode-active').removeClass('JS-decode-active');    
        });

        $('.JS-slideshow-header').on('afterChange', function(slick, currentSlide){

            if ($('.slick-current').find('.JS-decode').length) {
            $('.slick-current').find('.JS-decode').addClass('JS-decode-active');
            decodeText();
            }
            
        });

        function decodeText(){
            var text = document.getElementsByClassName('JS-decode-active')[0];
            var state = [];
            for(var i = 0, j = text.children.length; i < j; i++ ){
                text.children[i].classList.remove('state-1','state-2','state-3');
                state[i] = i;
            }

            var shuffled = shuffle(state);
        
            for(var i = 0, j = shuffled.length; i < j; i++ ){
                var child = text.children[shuffled[i]];
                classes = child.classList;

                var state1Time = Math.round( Math.random() * (2000 - 300) ) + 50;
                if(classes.contains('text-animation')){ 
                    setTimeout(firstStages.bind(null, child), state1Time);
                }
            }
        }

        function firstStages(child){
            if( child.classList.contains('state-2') ){   
                child.classList.add('state-3');
            } else if( child.classList.contains('state-1') ){
                child.classList.add('state-2')
            } else if( !child.classList.contains('state-1') ){
                child.classList.add('state-1');
                setTimeout(secondStages.bind(null, child), 100);
            }    
        }
        function secondStages(child){
            if( child.classList.contains('state-1') ){
                child.classList.add('state-2')
                setTimeout(thirdStages.bind(null, child), 100);
            } 
            else if( !child.classList.contains('state-1') )
            {
                child.classList.add('state-1')
            }
        }
        function thirdStages(child){
            if( child.classList.contains('state-2') ){
                child.classList.add('state-3')
            }
        }

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        }

    }



});