import 'slick-carousel';

$(document).ready(function() {	

  if( $('.JS-offer-show').length ) {
    //slider start

    var viewportWidth = $(window).width(); 
    var viewportHeight = $(window).height();

    const slider = $('.JS-offer-show-wrap');
    const offerSlideShowArr = document.querySelectorAll('.JS-offer-show');
    let visibleInViewPort = false;
      

    if (viewportWidth > 1024) {  
      slider.each(function(index, element) {
        var $element = $(element);
        // set the length of children in each loop 
        $element.data('slider-length', $element.children().length);
      })
      .slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1800, 
        dots: false,
        arrows: false,
      });   

      $('.JS-offer-show-wrap').each(function() {
        let element = $(this);
        element = element.css('height', viewportHeight + 'px');
      })

    } else { 
      slider.slick('unslick');
    }
              
    function onSliderAfterChange(event, slick, currentSlide) {
      $(event.target).data('current-slide', currentSlide);
    }
    
    let onSliderWheelListener = function (e) {  
        var deltaY = e.originalEvent.deltaY,
            $currentSlider = $(e.currentTarget),
            currentSlickIndex = $currentSlider.data('current-slide') || 0;
        
        if (
          // check when you scroll up
          (deltaY < 0 && currentSlickIndex == 0) ||
          // check when you scroll down
          (deltaY > 0 && currentSlickIndex == $currentSlider.data('slider-length') - 1)
        ) {
        // $currentSlider.removeClass('JS-visible-in-viewport'); 
        enableScroll();
          return;
        }
    
        e.preventDefault();
    
        if(visibleInViewPort == true) {

          if (e.originalEvent.deltaY < 0) {
            $currentSlider.slick('slickPrev');
          } else {
            $currentSlider.slick('slickNext');
          }
        }
 
    }

    // left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e.preventDefault();
}

function preventDefaultForScrollKeys(e) {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}

// modern Chrome requires { passive: false } when adding event
  var supportsPassive = false;
  try {
    window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
      get: function () { supportsPassive = true; } 
    }));
  } catch(e) {}

  var wheelOpt = supportsPassive ? { passive: false } : false;
  var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

  // call this to Disable
  function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
  }

  // call this to Enable
  function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt); 
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
  }
    // jQuery(window).on('resize', function() { 
    //   if (viewportWidth <= 1024) {
    //     slider.slick('unslick');

    //     $('.JS-offer-show').each(function() {
    //       let element = $(this);
    //       element = element.css('height', 'auto');
    //     })
    //   }
    // });  
 
    const options = {
      root: null, 
      rootMargin: '0px', 
      threshold: 0.8
    }
 
    const observer = new IntersectionObserver((entries, observer) => { 
        entries.forEach(entry => {   
            if (entry.isIntersecting) { 
              visibleInViewPort = true; 

              $('html, body').animate({scrollTop: slider.offset().top}, 100); 
              disableScroll(); 
              
              //$(entry.target).find( ".JS-offer-show-wrap" ).addClass('JS-visible-in-viewport'); 
              slider.each(function(index, element) {
                var $element = $(element); 

                $element
                .on('afterChange', onSliderAfterChange)
                .on('wheel', onSliderWheelListener); 
              })  
            } else { 
              visibleInViewPort = false;
              enableScroll();

              slider.each(function(index, element) { 
                var $element = $(element);
                $element.off('wheel', onSliderWheelListener); 
              })  
            }
        })
      }, options);
  
      offerSlideShowArr.forEach(i => {
        observer.observe(i);
      }) 
  }  
});