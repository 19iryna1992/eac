const CareersFilter = () => {
    let posts_offset = 0;
    const CareersFilterFrom = $('.JS--careers-filter-form');
    const PositionsSelect = $('#positions');
    const LocationsSelect = $('#locations');
    const CareersCards = $('.JS--career-cards');
    const loadMoreBtn = $('.JS--load-careers');


    CareersFilterFrom.on('submit', function (e) {
        e.preventDefault();
        let currentPosition = PositionsSelect.val();
        let currentLocation = LocationsSelect.val();
        careerFilter(currentPosition,currentLocation,0);
    });

    loadMoreBtn.on('click',function(e){
        e.preventDefault();
        let currentPosition = PositionsSelect.val();
        let currentLocation = LocationsSelect.val();
        setTimeout(function () {
            posts_offset = $('.JS--career-card').length;
            loadMoreCareers(currentPosition,currentLocation,posts_offset);
        }, 100);
    });


    function careerFilter(currentPosition,currentLocation,offset) {
        $.ajax({
            url: appLocations.admin_ajax,
            data: {
                action: "career_posts",
                offset: offset,
                positions: currentPosition,
                locations:currentLocation
            },
            type: 'POST',
            beforeSend: function () {

            },
            success: function (data) {
                if (!data.success) {
                    loadMoreBtn.fadeOut(0);
                    CareersCards.html('<div class="py-50 text-h3-lg">Nothing found</div>');
                    return false;
                }
                let result = data.data;
                // Append results to DOM
                if (result.success !== false) {
                    CareersCards.html('');
                    CareersCards.append(result.content);
                }
                if (result.success == false || result.load_button == false || data.success == false) {
                    loadMoreBtn.fadeOut(0);
                } else if (result.load_button === true) {
                    loadMoreBtn.fadeIn();
                }

            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function loadMoreCareers(currentPosition,currentLocation,offset) {
        $.ajax({
            url: appLocations.admin_ajax,
            data: {
                action: "career_posts",
                offset: offset,
                locations: currentLocation,
                positions:currentPosition
            },
            type: 'POST',
            beforeSend: function () {

                loadMoreBtn.append('<span class="c-primary-button-icon-loading"></span>');
            },
            success: function (data) {
                $('.c-primary-button-icon-loading').remove();
                if (!data.success) {
                    loadMoreBtn.fadeOut();
                    return false;
                }
                var result = data.data;
                // Append results to DOM
                if (result.success !== false) {
                    CareersCards.append(result.content);
                }
                if (result.success == false || result.load_button == false || data.success == false) {
                    loadMoreBtn.fadeOut(0);
                } else if (result.load_button === true) {
                    loadMoreBtn.fadeIn();
                }

            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    }

}

export default CareersFilter;