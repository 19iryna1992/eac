document.addEventListener("DOMContentLoaded", function(event) {
    if(document.getElementsByClassName("JS-select") != null) {
      let selectBox, i, j, selectBoxLength, selectElementLength, selectElement, selectVisible, selectList, selectListItem;
  
      selectBox = document.getElementsByClassName("JS-select-wrapper");
      selectBoxLength = selectBox.length;
  
      for (i = 0; i < selectBoxLength; i++) {
        selectElement = selectBox[i].getElementsByTagName("select")[0];
        selectElementLength = selectElement.length;
  
        selectVisible = document.createElement("div");
        selectVisible.setAttribute("class", "c-select__visible");
        selectVisible.setAttribute("value", "");
        selectVisible.innerHTML = selectElement.options[selectElement.selectedIndex].innerHTML;
        selectBox[i].appendChild(selectVisible);
  
        selectList = document.createElement("ul");
        selectList.setAttribute("class", "c-select__list hidden");
  
        for (j = 1; j < selectElementLength; j++) {
          selectListItem = document.createElement("li");
          selectListItem.innerHTML = selectElement.options[j].innerHTML;
          selectListItem.setAttribute("value", selectElement.options[j].innerHTML.toLowerCase().replace(/\s/g, ''));
          
          
          selectListItem.addEventListener("click", function(e) {
            let selectItemSelected, i, k, select, selectItemParentSibling, selectLenght, selectItemSelectedLenght;
            select = this.parentNode.parentNode.getElementsByTagName("select")[0];
            selectLenght = select.length;
            selectItemParentSibling = this.parentNode.previousSibling;
            
            for (i = 0; i < selectLenght; i++) {
              if (select.options[i].innerHTML == this.innerHTML) {
                select.selectedIndex = i;
                selectItemParentSibling.innerHTML = this.innerHTML;
                selectItemSelected = this.parentNode.getElementsByClassName("selected");
                selectItemSelectedLenght = selectItemSelected.length;
                
                for (k = 0; k < selectItemSelectedLenght; k++) {
                  selectItemSelected[k].removeAttribute("class");
                }
                
                this.setAttribute("class", "selected");
                
                document.querySelector(".c-select__visible").setAttribute("value", selectItemParentSibling.innerHTML.toLowerCase().replace(/\s/g, ''));
                document.querySelector(".JS-select").setAttribute("value", selectItemParentSibling.innerHTML.toLowerCase().replace(/\s/g, ''));
              }
            }
  
              selectItemParentSibling.click();
          });
  
          selectList.appendChild(selectListItem);
        }
  
        selectBox[i].appendChild(selectList);
        selectVisible.addEventListener("click", function(e) {
  
          e.stopPropagation();
          closeAllSelect(this);
          this.nextSibling.classList.toggle("hidden");
          this.classList.toggle("JS-select-arrow-active");
        });
      }
  
      
      function closeAllSelect(elmnt) {
        let selectItems, selectVisible, i, selectItemsLength, selectVisibleLength, arrNo = [];
        selectItems = document.getElementsByClassName("c-select__list");
        selectVisible = document.getElementsByClassName("c-select__visible");
        selectItemsLength = selectItems.length;
        selectVisibleLength = selectVisible.length;
  
        for (i = 0; i < selectVisibleLength; i++) {
          if (elmnt == selectVisible[i]) {
            arrNo.push(i)
          } else {
            selectVisible[i].classList.remove("JS-select-arrow-active");
          }
        }
  
        for (i = 0; i < selectItemsLength; i++) {
          if (arrNo.indexOf(i)) {
            selectItems[i].classList.add("hidden");
          }
        }
      }
  
      document.addEventListener("click", closeAllSelect);
    }
  });