if (document.querySelector(".JS-counter") != null) { 
    let counterContainers = document.querySelectorAll(".JS-counter");  

    function isInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    } 
    counterContainers.forEach(counterContainer => {
        let counterEnd = counterContainer.getAttribute('data-counter-end');  
        if(Number(counterEnd) < 10 && counterContainer.classList.contains("JS-counter-step")) { 
            counterContainer.innerHTML = "0" + counterEnd; 
        }
    });

    window.addEventListener('scroll', function () {
        counterContainers.forEach(counterContainer => {
            if(isInViewport(counterContainer) == true) {   
                let counterEnd = counterContainer.getAttribute('data-counter-end');   
                const rect = counterContainer.getBoundingClientRect();
                
                let percent = (1 - rect.top/window.innerHeight) * 2;
                percent  = percent > 1 ? percent = 1 : percent;

                if (Math.round(counterEnd * percent) < 10 && counterContainer.classList.contains("JS-counter-step")) {
                    counterContainer.innerHTML = "0" + Math.round(counterEnd * percent);  
                } else {
                    counterContainer.innerHTML = Math.round(counterEnd * percent); 
                }
            } 
        }) 
    }, {
        passive: true
    }); 
}