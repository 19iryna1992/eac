import 'slick-carousel';

$(window).on('load', function() {	 
  let windowWidth = window.innerWidth; 

  //---START: more info  
  if( $('.JS-cards-slider').length ) {
 
      function getSliderSettings() {
        return { 
          infinite: false,
          arrows: false,
          slidesToShow: 1.25, 
          speed: 800,  
        }
      } 
    
      if(!$('.JS-cards-slider').hasClass('slick-initialized')) {
        if (windowWidth < 1024) {
          $('.JS-cards-slider').slick(getSliderSettings());
        } 
      } else if (windowWidth > 1024 && $('.JS-cards-slider').hasClass('slick-initialized')) {
          $('.JS-cards-slider').hasClass('slick-initialized').slick('unslick');
      } 


      $(window).resize(function() {
        windowWidth = window.innerWidth;
        if(!$('.JS-cards-slider').hasClass('slick-initialized')) {
          if (windowWidth < 1024) { 
            $('.JS-cards-slider').slick(getSliderSettings()); 
          }
        } else if (windowWidth > 1024 && $('.JS-cards-slider').hasClass('slick-initialized')) {
            $('.JS-cards-slider').slick('unslick');
        } 
      });  
  }
  //---END: more info 

  //---START: slideshow-header
  if($('.JS-slideshow-header').length) {
    $('.JS-slideshow-header').on('init', function(event, slick){
      $('.JS-slideshow-header').find('.slick-current').addClass('active');
    });

    jQuery(window).on('resize', function() {
      $('.JS-slideshow-header').find('.slick-current').addClass('active');
    });

    $('.JS-slideshow-header').on('beforeChange', function(slick, currentSlide) {
      $('.JS-slideshow-header').find('.slick-current.active').removeClass('active');
    });

    $('.JS-slideshow-header').on('afterChange', function(slick, currentSlide){
      $('.JS-slideshow-header').find('.slick-current').addClass('active');
    });

    $('.JS-slideshow-header').slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear',
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: false
          }
        }
      ]
    });
  }
  //---END: slideshow-header

  //---START: timeline slider 
  if($('.JS-time-line-slider').length) {
    $('.JS-time-line-slider-wrapper').each(function (index, sliderWrap) { 
        let sliderBox = $(sliderWrap);
        let slider = sliderBox.find('.JS-time-line-slider');
                
        let appendArrows = sliderBox.find('.JS-time-line-slider-arrows');
        let status = sliderBox.find('.JS-time-line-slider-dots');
        let progressBar = sliderBox.find('.JS-progress-timeline');
        
        slider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            let slideNumber = (currentSlide ? currentSlide : 0) + 1;
            let slideNumberValue = slideNumber;
            let slideCountValue = slick.slideCount;

            if(slideNumber <= 9) {
              slideNumberValue = '0' + slideNumber;
            }

            if(slick.slideCount <= 9) {
              slideCountValue = '0' + slick.slideCount;
            } 

            status.html($('<span class="mr-10 text-md linear-20 font-title">' + slideNumberValue  + '</span>' + ' ' + '<span class="ml-10 text-md linear-20 font-title">' + slideCountValue + '</span>'));
        });

        function setProgress(index) {
          const calc = ((index + 1) / (slider.slick('getSlick').slideCount)) * 100;

          progressBar
            .css('background-size', `${calc}% 100%`)
            .attr('aria-valuenow', calc);
        } 

        slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
          setProgress(nextSlide);
        }); 

        slider.slick({
          centerPadding: '50px',
          accessibility: true,
          appendArrows: appendArrows,
          prevArrow: '<a href="#" class="c-btn c-btn--round c-btn--round-blue c-btn--round-left mr-20"><svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z" fill="currentColor"></path></svg></a>',
          nextArrow: '<a href="#" class="c-btn c-btn--round c-btn--round-blue c-btn--round-right"><svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z" fill="currentColor"></path></svg></a>',
          responsive: [{

            breakpoint: 768,
              settings: {
                arrows: false,
              }
          }]
        });

        setProgress(0);
    });
  }
  //---END: timeline slider
  
  //---START: testimonials slider
  if($('.JS-testimonials-slider').length) {
    $('.JS-testimonials-slider-wrapper').each(function (index, sliderWrap) {
      let sliderBox = $(sliderWrap);
      let slider = sliderBox.find('.JS-testimonials-slider');
              
      let appendArrows = sliderBox.find('.JS-testimonials-slider-arrows');
      let status = sliderBox.find('.JS-testimonials-slider-dots');
      let progressBar = sliderBox.find('.JS-progress-testimonials');
    
      slider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
          let slideNumber = (currentSlide ? currentSlide : 0) + 1;
          let slideNumberValue = slideNumber;
          let slideCountValue = slick.slideCount;
    
          if(slideNumber <= 9) { 
            slideNumberValue = '0' + slideNumber;
          } 
    
          if(slick.slideCount <= 9) {
            slideCountValue = '0' + slick.slideCount;
          } 
    
          status.html($('<span class="mr-10 text-md linear-20 font-title text-white">' + slideNumberValue  + '</span>' + ' ' + '<span class="ml-10 text-md linear-20 font-title text-white">' + slideCountValue + '</span>'));
      });
    
      function setProgress(index) {
        const calc = ((index + 1) / (slider.slick('getSlick').slideCount)) * 100;
    
        progressBar
          .css('background-size', `${calc}% 100%`)
          .attr('aria-valuenow', calc);
      }
    
      slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        setProgress(nextSlide);
      });
    
    
      slider.slick({
        centerPadding: '50px',
        accessibility: true,
        appendArrows: appendArrows,
        prevArrow: '<a href="#" class="c-btn c-btn--round c-btn--round-blue c-btn--round-left mr-20"><svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z" fill="currentColor"></path></svg></a>',
        nextArrow: '<a href="#" class="c-btn c-btn--round c-btn--round-blue c-btn--round-right"><svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z" fill="currentColor"></path></svg></a>',
        adaptiveHeight: true,
    
        responsive: [{
    
          breakpoint: 768,
            settings: {
              arrows: false,
            }
        }]
      });
    
      setProgress(0);
    });
  }
  //---END: testimonials slider
  
  //---START: news slider
  if($('.JS-news-slider').length) {
    $('.JS-news-slider-wrapper').each(function (index, sliderWrap) {
        let sliderBox = $(sliderWrap);
        let slider = sliderBox.find('.JS-news-slider');

        let appendArrows = sliderBox.find('.JS-news-slider-arrows');
        let status = sliderBox.find('.JS-news-slider-dots');
        let progressBar = sliderBox.find('.JS-progress-news');
      
        slider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        
            let checkAmountSlides = function(windowWidth, currentSlide) { 
              let slideNewsNumber;

              slideNewsNumber = (currentSlide ? currentSlide : 0) + 1; 
      
              if(slideNewsNumber <= 9) {
                slideNewsNumber = '0' + slideNewsNumber;
              } 
      
              return(slideNewsNumber);
            }; 
      
            let checkTwoDigit = function(windowWidth) { 
              let slideNewsCountValue;  

              if(slick.slideCount > 9) {
                slideNewsCountValue = slick.slideCount;
              } else {
                slideNewsCountValue = '0' + slick.slideCount;
              };
    
              return(slideNewsCountValue);
            }; 

            function insertCountSlides(windowWidth) {
              status.html($('<span class="text-white mr-10 text-sm md:text-md linear-20 font-title text-white">' + checkAmountSlides(windowWidth, currentSlide) + '</span>' + ' ' + '<span class="text-white ml-10 text-sm md:text-md linear-20 font-title text-white">' + checkTwoDigit(windowWidth) + '</span>'));
            }

            $(window).resize(function() {
              windowWidth = window.innerWidth;
              insertCountSlides(windowWidth);            
            });

            insertCountSlides(windowWidth);
        }); 
      
      
        function setProgress(indexNews) {
          let calc;

          calc = ((indexNews + 1) / (slider.slick('getSlick').slideCount)) * 100;
      
          progressBar
            .css('background-size', `${calc}% 100%`)
            .attr('aria-valuenow', calc);
        }
      
        slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
          setProgress(nextSlide);
        });
      
        slider.slick({
          slidesToShow: 2.1,
          infinite: true,
          centerMode: true,
      
          appendArrows: appendArrows,
          prevArrow: '<a href="#" class="c-btn c-btn--round c-btn--round-transparent c-btn--round-left mr-20"><svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z" fill="currentColor"></path></svg></a>',
          nextArrow: '<a href="#" class="c-btn c-btn--round c-btn--round-transparent c-btn--round-right"><svg class="colorCurrent w-24 h-18" viewBox="0 0 24 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.8535 8.66871L16.3535 0.16608C16.1582 -0.0553601 15.8418 -0.0553601 15.6465 0.16608C15.4512 0.387521 15.4512 0.746225 15.6465 0.967612L22.293 8.50264L0.500015 8.50264C0.22364 8.50264 8.19011e-10 8.75618 8.48318e-10 9.0695C8.77624e-10 9.38282 0.22364 9.63636 0.500015 9.63636L22.293 9.63636L15.6465 17.1713C15.4512 17.3928 15.4512 17.7515 15.6465 17.9729C15.7441 18.0836 15.8721 18.1389 16 18.1389C16.1279 18.1389 16.2559 18.0836 16.3535 17.9729L23.8535 9.47024C24.0488 9.24885 24.0488 8.89015 23.8535 8.66871Z" fill="currentColor"></path></svg></a>',
          responsive: [{
      
            breakpoint: 480,
              settings: {
                slidesToShow: 1.2,
              }
          }, {

            breakpoint: 768,
              settings: {
                slidesToShow: 1.1,
              }
          }]
        });
      
        setProgress(0);
    });
  }
  //---END: news slider
});