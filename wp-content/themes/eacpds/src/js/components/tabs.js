function checkChangeTab(tabName, bgColorActive, textColorActive, bgColorDefault, textColorDefault, brColorActive, brColorDefault) { 
    let tabTriggerBtns = document.querySelectorAll(`${tabName} .JS-tabs span`); 

    tabTriggerBtns.forEach(function(tabTriggerBtn, index) {     
        tabTriggerBtn.addEventListener('click', function(e) {  
            let currentTab = e.target.parentNode.parentNode.parentNode.parentNode;    
            let currentTabData = currentTab.querySelector('[data-tab-content="' + this.dataset.tabTrigger + '"]');
                
            currentTab.querySelector('.JS-tab-content.visible').classList.add('invisible');
            currentTab.querySelector('.JS-tab-content.visible').classList.remove('visible');
    
            currentTab.querySelector(`.JS-tabs span.${bgColorActive}`).classList.add(bgColorDefault, textColorDefault);
            currentTab.querySelector(`.JS-tabs span.${bgColorActive}`).classList.remove(bgColorActive, textColorActive);
                
            currentTabData.classList.remove('invisible');
            currentTabData.classList.add('visible');
    
            this.classList.remove(bgColorDefault, textColorDefault);
            this.classList.add(bgColorActive, textColorActive);

            if(brColorDefault != undefined) {
                currentTab.querySelector(`${tabName} span.${brColorActive}`).classList.add(brColorDefault);
                currentTab.querySelector(`${tabName} span.${brColorActive}`).classList.remove(brColorActive); 

                this.classList.remove(brColorDefault);
                this.classList.add(brColorActive);
            }
        });
    });
}

if (document.querySelector(".JS-tab-post") != null) { 
    checkChangeTab('.JS-tab-post', 'bg-black', 'text-white', 'bg-grey-light', 'text-black'); 
} 

if (document.querySelector(".JS-tab-сards") != null) {  
    checkChangeTab('.JS-tab-сards', 'bg-blue', 'text-white', 'bg-transparent', 'text-black', 'border-blue', 'border-black-dim');
}

if (document.querySelector(".JS-tab-featured-products") != null) {  
    checkChangeTab('.JS-tab-featured-products', 'bg-grey-light', 'text-black', 'bg-transparent', 'text-black');
}