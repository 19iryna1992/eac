import SimpleBar from "simplebar";

const CustomScroll = () => {
  if($('.JS-accordion-body').length) {
    Array.from(document.querySelectorAll('.JS-accordion-body'))
      .forEach(
        el => new SimpleBar(el, {
          autoHide: false,
        })
      );
      
    //accordion open-close animation    
    $('.JS-accordion-title').click(function () {
      $(this).closest('.JS-accordion-tab').toggleClass('active'); 
    });
  }
}

export default CustomScroll;