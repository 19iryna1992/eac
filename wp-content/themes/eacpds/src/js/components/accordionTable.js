$( document ).ready(function() {	 
    if($('.JS-accordion-table-tab').length) {  
        let accordionTableHeader = $('.JS-accordion-table-header');


        let setaccordionTableHeaderHeight = function(accordionHeader, closestWrap) { 
            if ($(closestWrap).hasClass("is-active")) {
                $(closestWrap).removeClass("is-active")
            }
            
            accordionHeader.each(function (i, element) { 
                let elementHeaderHeight = $(element).height();  
                $(this).closest(closestWrap).css({'max-height': elementHeaderHeight + 10 + 'px', 'overflow': 'hidden'});  
            });
        }

        setaccordionTableHeaderHeight(accordionTableHeader, '.JS-accordion-table-wpapper');

        //---accordion table open-close animation    
        accordionTableHeader.click(function () {
            let accordionTableWpapper = $(this).closest('.JS-accordion-table-wpapper');  

            if (!accordionTableWpapper.hasClass("is-active")) {
                accordionTableWpapper.css({'max-height': '100%', 'overflow': 'visible'}); 
                
            } else {  
                let elementHeaderHeight = $(this).closest('.JS-accordion-table-header').height();  
                accordionTableWpapper.css({'max-height': elementHeaderHeight + 10 + 'px', 'overflow': 'hidden'}); 
            } 
            
            accordionTableWpapper.toggleClass('is-active');  
        }); 

        $(window).resize(function() {
            setaccordionTableHeaderHeight(accordionTableHeader, '.JS-accordion-table-wpapper');
        }); 
    }
});