$(document).on('click', '.JS-mob-toggle', function(){
  $('html, body').toggleClass('overflow');
  $('.o-header__main').toggleClass('open');
  $('.JS--mega-menu-item.active, .c-text-menu__item.active').removeClass('active');
  $('.c-text-menu, .c-text-menu__submenu, .c-about-menu, .c-resources-menu').slideUp();
});

$(document).on('click', '.JS--submenu-open', function(e){
  if ($(document).width() < 1200) {
    e.stopPropagation();
    e.preventDefault();
    $(this).closest('.has-mega-menu').toggleClass('active');
    $(this).parent().siblings('div').slideToggle();
  }
});

$(document).on('click', '.JS-sub-menu-toggle', function(e){
  if ($(document).width() < 1200) {
    e.stopPropagation();
    e.preventDefault();
    $(this).closest('.c-text-menu__item').toggleClass('active');
    $(this).parent().siblings('.c-text-menu__submenu').slideToggle();
    $(this).find('.c-text-menu__submenu').slideUp();
  }
});

$('.JS--mega-menu-item').mouseenter(function(){
  if ($(document).width() >= 1200) {
    if( !$(this).hasClass('open-item') ) {
      $('.JS--mega-menu-item.open-item').removeClass('open-item');
      $(this).addClass('open-item');
      $('html, body').addClass('overflow');
      $(this).siblings('.JS--mega-menu-item').find('.c-text-menu__bg.active').removeClass('active');
    }
  }
});

$('.c-text-menu__item').mouseenter(function(){
  if ($(document).width() >= 1200) {
    if( $(this).hasClass('has-child') ) {
      $('.c-text-menu__item.has-child').removeClass('active');
      $(this).addClass('active');
      $(this).siblings('.c-text-menu__bg').addClass('active');
    } else {
      $('.c-text-menu__bg').removeClass('active');
      $('.c-text-menu__item.has-child').removeClass('active');
    }
  }
});

$(document).on('click', '.JS-close-sub-menu', function(){
  $('.c-text-menu__bg').removeClass('active');
  $('.JS--mega-menu-item').removeClass('open-item');
  $('html, body').removeClass('overflow');
  $('.c-text-menu__item').removeClass('active');
});

$(document).on('click', '.JS-close-sub-menu', function(){
  $('.c-text-menu__bg').removeClass('active');
  $('.JS--mega-menu-item').removeClass('open-item');
  $('html, body').removeClass('overflow');
  $('.c-text-menu__item').removeClass('active');
});

$(document).on('click', '.JS-close-resources-menu, .JS-close-about-menu', function(){  
  $('.JS--mega-menu-item.open-item').removeClass('open-item');
  $('.c-text-menu__item').removeClass('active');
  $('html, body').removeClass('overflow');
});