const ResourcesFilter = () => {

    const archiveOrderby = document.getElementById('orderby');
    const archiveOrder = document.getElementById('order');

    if (archiveOrderby && archiveOrder) {

        const setOrder = () => {
            const orderBy = archiveOrderby.options[archiveOrderby.selectedIndex].value;

            if('title' === orderBy || 'oldest' === orderBy){
                archiveOrder.value = 'ASC';
            }else{
                archiveOrder.value = 'DESC';
            }
        }

        archiveOrderby.addEventListener('change', setOrder);

        setOrder();
    }
}

export default ResourcesFilter;

$(function() {
    let blogFilter = $('.JS-blog-filter');
    let blogFilterHeading = $('.JS-blog-filter-heading');
    let blogFilterOverlay = $('.JS-blog-filter-overlay');
    let body = $('body');
    let windowWidth = window.innerWidth;

    blogFilterHeading.on('click', function() {
        blogFilter.toggleClass('active');
        blogFilterOverlay.toggleClass('active');
        body.toggleClass('overflow-hidden');
    });

    let checkIsOpenFilter = function(windowWidth) { 
        if (windowWidth >= 768) {
            blogFilter.removeClass('active');
            blogFilterOverlay.removeClass('active');
            body.removeClass('overflow-hidden');
        };
    };
    
    checkIsOpenFilter(windowWidth);

    $(window).resize(function() {
        windowWidth = window.innerWidth; 
        checkIsOpenFilter(windowWidth);
    });
});