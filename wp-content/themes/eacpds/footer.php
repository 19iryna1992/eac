<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eacpds
 */

if (!defined('ABSPATH')) exit;

?>

</div><!-- #content -->

<?php if (!is_page_template(array('templates/template-landing.php'))) : ?>
    <footer class="o-footer bg-black text-white pt-40 md:pt-100 lg:pt-200 pb-90 pb-50 lg:pb-30">
        <?php get_template_part('template-parts/organisms/footer/top-footer'); ?>
        <?php get_template_part('template-parts/organisms/footer/middle-footer'); ?>
        <?php get_template_part('template-parts/organisms/footer/bottom-footer'); ?>
    </footer>
<?php endif; ?>

</div><!-- #page -->
<?php if (is_singular('post')): ?>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
<?php endif ?>
<?php wp_footer(); ?>

</body>
</html>
