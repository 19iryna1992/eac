<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header(); ?>

<main id="main" role="main" tabindex="-1">

    <?php while (have_posts()) : the_post(); ?>

        <?php the_content(); ?>

    <?php endwhile; ?>

</main>

<?php get_footer(); ?>