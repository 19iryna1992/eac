<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package eacpds
 */

get_header();

?>

<main id="main" role="main" tabindex="-1">

    <section class="s-404">
        <div class="container text-center">
            <h2 class="c-large-symbols c-large-symbols--blue font-title font-norma text-h1 md:text-h1-sm lg:text-h1-xl leading-9">404</h2> 
            <h2 class="text-blue"><?php _e('Not found', 'eacpds') ?></h2>
        </div>
    </section>

</main>

<?php
get_footer(); ?>