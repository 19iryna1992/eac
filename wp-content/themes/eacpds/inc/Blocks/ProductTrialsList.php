<?php


namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class ProductTrialsList extends Block
{
    protected string $name = 'product-trials-list';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Product Trials List'),
            'description' => __('Add Products'),
            'category' => 'formatting',
            'icon' => 'products',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'product-trials.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('product_trials_title', [
                'label' => 'Title'
            ])
            ->addTextarea('product_trials_big_description', [
                'label' => 'Big Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addTextarea('product_trials_small_description', [
                'label' => 'Small Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRelationship('relationship_product_trials', [
                'label' => 'Product Trials',
                'post_type' => ['products'],
                'filters' => [
                    0 => 'search',
                ],
                'min' => '1',
                'max' => '',
                'return_format' => 'object',
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));

        return $block->build();
    }

}