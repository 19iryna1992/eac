<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Logos extends Block
{
    protected string $name = 'logos';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Client Logos Section'),
            'description' => __('Add Client Logos Section'),
            'category' => 'formatting',
            'icon' => 'awards',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'client-logos-section.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addRepeater('logos', [
                'min' => 1,
                'button_label' => 'Add Logo',
                'layout' => 'block',
            ])
            ->addimage('logo', [
                'label' => 'Logo',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->addLink('url',[
                'label'=>'Url (optional)',
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}