<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class PostAuthor extends Block
{
    protected string $name = 'post-author';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Post Author'),
            'description' => __('Add Post Author'),
            'category' => 'formatting',
            'icon' => 'businessperson',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'post-author.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block
            ->addTextarea('post_author_description', [
                'label' => 'Post Author Description (Optional)',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));

        return $block->build();
    }
}