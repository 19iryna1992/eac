<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class Buttons extends Block
{
    protected string $name = 'buttons';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Buttons'),
            'description' => __('Add Buttons'),
            'category' => 'formatting',
            'icon' => 'button',
            'supports' => [
                'anchor' => false,
                'align' => false,
                'mode' => true,
            ]
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addSelect('buttons_position', [
                'label' => 'Buttons position',
                'choices' => [
                    'one_row' => 'One row',
                    'two_rows' => 'Two rows'
                ],
                'default_value' => ['one_row'],
                'return_format' => 'value',
            ])
            ->addRepeater('buttons', [
                'min' => 1,
                'button_label' => 'Add Button',
                'layout' => 'block',
            ])
            ->addLink('button',[
                'label'=>'Button',
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}