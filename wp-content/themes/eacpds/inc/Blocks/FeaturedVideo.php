<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class FeaturedVideo extends Block
{
    protected string $name = 'featured-video';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Featured Video'),
            'description' => __('Add Featured Video'),
            'category' => 'formatting',
            'icon' => 'format-video',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'featured-video.png'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addTextarea('title', [
                'label' => 'Title',
                'new_lines' => 'br',
                'rows' => '4',
            ])
            ->addimage('video_image', [
              'label' => 'Video Preview Image',
              'return_format' => 'id',
              'conditional_logic' => [
                  'field' => 'media_type',
                  'operator' => '==',
                  'value' => 'video',
              ]
          ])
          ->addRadio('video_source', [
              'label' => 'Choose Video Source',
              'conditional_logic' => [
                  'field' => 'media_type',
                  'operator' => '==',
                  'value' => 'video',
              ]
          ])
          ->addChoices([
              'local' => 'Upload',
              'video_url_youtube' => 'Youtube',
              'video_url_vimeo' => 'Vimeo Url',
          ])
          ->addFile('video_mp4', [
              'label' => 'Upload video (mp4)',
              'conditional_logic' => [
                  'field' => 'video_source',
                  'operator' => '==',
                  'value' => 'local',
              ]
          ])
          ->addUrl('video_link', [
              'label' => 'Video Url',
              'conditional_logic' => [
                  [
                      [
                          'field' => 'video_source',
                          'operator'  =>  '==',
                          'value' => 'video_url_youtube',
                      ],
                  ],
                  [
                      [
                          'field' => 'video_source',
                          'operator'  =>  '==',
                          'value' => 'video_url_vimeo',
                      ],
                  ],
              ]
          ]);

        // $block
        //     ->addRepeater('fp_tabs', [
        //         'min' => 1,
        //         'button_label' => 'Add Tab',
        //         'layout' => 'block',
        //         'collapsed' => 'fp_tab_title'
        //     ])
        //     ->addText('fp_tab_title',[
        //         'label'=> 'Tab title'
        //     ])
        //     ->addRelationship('featured_products', [
        //         'label' => 'Choose Products',
        //         'post_type' => ['products'],
        //         'filters' => [
        //             0 => 'search',
        //         ],
        //     ])
        //     ->endRepeater()
        //     ->addLink('fp_tab_btn', [
        //         'label' => 'Button',
        //         'return_format' => 'array'
        //     ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}