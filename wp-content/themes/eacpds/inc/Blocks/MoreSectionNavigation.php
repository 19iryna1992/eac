<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class MoreSectionNavigation extends Block
{
    protected string $name = 'more-section-navigation';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('More in this Section Navigation'),
            'description' => __('Add ‘More in this Section’ Navigation'),
            'category' => 'formatting',
            'icon' => 'screenoptions',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'more-in-this-section-navigation.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addRepeater('nav_blocks', [
                'min' => 1,
                'button_label' => 'Add Block',
                'layout' => 'block',
            ])
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->addLink('url',[
                'label'=>'Url (optional)',
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}