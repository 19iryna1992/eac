<?php


namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class Table extends Block
{

    protected string $name = 'table';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Table'),
            'description' => __('Add Table'),
            'category' => 'formatting',
            'icon' => 'editor-table',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'blog-subscription.jpg'
                    ]
                ],
            ],
        ]);

    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('table_title',[
                'label'=>'Table title'
            ])
            // Count column

            ->addSelect('table_count_columns', [
                'default' => 'four'
            ])
            ->addChoices([
                'four' => 'Four',
                'five' => 'Five',
            ])


            // Popular

            ->addTrueFalse('popular_column_second', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTrueFalse('popular_column_third', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTrueFalse('popular_column_fourth', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTrueFalse('popular_column_fifth', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])

            // Titles

            ->addText('title_table_second', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('title_table_third', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('title_table_fourth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('title_table_fifth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])

            //Descriptions

            ->addTextarea('description_table_second', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTextarea('description_table_third', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTextarea('description_table_fourth', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTextarea('description_table_fifth', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])


            // Price

            ->addText('price_table_second', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('price_table_third', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('price_table_fourth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('price_table_fifth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])

            // Price Label

            ->addText('price_label_second', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('price_label_third', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('price_label_fourth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('price_label_fifth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])

            // Column Title

            ->addText('column_title_first', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('column_title_second', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('column_title_third', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('column_title_fourth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('column_title_fifth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ]);

        // Repeater Rows

        $block
            ->addRepeater('table_row', [
                'label' => 'Table Row',
                'button_label' => 'Add Column',
                'layout' => 'block',
            ])
            ->addSelect('type_cell_first', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_second', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_third', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_fourth', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_fifth', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addText('cell_text_first', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_second', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_third', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_fourth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_fifth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->endRepeater()

            // Buttons

            ->addLink('table_btn_second', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('table_btn_third', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('table_btn_fourth', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('table_btn_fifth', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}