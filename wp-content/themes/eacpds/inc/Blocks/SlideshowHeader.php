<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class SlideshowHeader extends Block
{
    protected string $name = 'slideshow-header';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Slideshow Header'),
            'description' => __('Add Slideshow Header'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'slideshow-header.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addRepeater('slides', [
                'min' => 1,
                'button_label' => 'Add Slide',
                'layout' => 'block',
            ])
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addText('highlight_words', [
                'label' => 'Highlight words',
                'instructions' => '(Please enter words and separate them with a comma )',
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addLink('slide_btn', [
                'label' => 'Button',
                'return_format' => 'array'
            ])
            ->addSelect('media_type', [
                'label' => 'Media',
                'default_value' => 'image',
            ])
            ->addChoices([
                'image' => 'Image',
                'video' => 'Video',
            ])
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'image',
                ]
            ])
            ->addimage('video_image', [
                'label' => 'Video Preview Image',
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'video',
                ]
            ])
            ->addRadio('video_source', [
                'label' => 'Choose Video Source',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'video',
                ]
            ])
            ->addChoices([
                'local' => 'Upload',
                'video_url_youtube' => 'Youtube',
                'video_url_vimeo' => 'Vimeo Url',
            ])
            ->addFile('video_mp4', [
                'label' => 'Upload video (mp4)',
                'conditional_logic' => [
                    'field' => 'video_source',
                    'operator' => '==',
                    'value' => 'local',
                ]
            ])
            ->addUrl('video_link', [
                'label' => 'Video Url',
                'conditional_logic' => [
                    [
                        [
                            'field' => 'video_source',
                            'operator'  =>  '==',
                            'value' => 'video_url_youtube',
                        ],
                    ],
                    [
                        [
                            'field' => 'video_source',
                            'operator'  =>  '==',
                            'value' => 'video_url_vimeo',
                        ],
                    ],
                ]
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}