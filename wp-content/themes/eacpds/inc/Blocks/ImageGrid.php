<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class ImageGrid extends Block
{
    protected string $name = 'image-grid';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Image Grid'),
            'description' => __('Add Image Grid'),
            'category' => 'formatting',
            'icon' => 'images-alt',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'image-grid.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addImage('main_section_image', [
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->addText('section_title', [
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->addTextarea('section_description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRepeater('images', ['min' => 1, 'layout' => 'block', 'button_label' => 'Add item'])
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '15%',
                ]
            ])
            ->addText('image_title',[
                'label' => 'Hover State Title',
                'wrapper' => [
                    'width' => '30%',
                ]
            ])
            ->addWysiwyg('image_description', [
                'label' => 'Hover State Description',
                'media_upload' => 0,
                'wrapper' => [
                    'width' => '55%',
                ],
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}