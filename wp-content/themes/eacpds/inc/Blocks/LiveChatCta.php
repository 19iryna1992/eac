<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class LiveChatCta extends Block
{
    protected string $name = 'live-chat-cta';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Live Chat / Call Us CTA'),
            'description' => __('Add Live Chat / Call Us CTA'),
            'category' => 'formatting',
            'icon' => 'format-status',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'live-chat-call-us-cta.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addImage('lc_icon', [
                'label' => 'Live Chat Icon',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addImage('cta_icon', [
                'label' => 'CTA Icon',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('lc_title', [
                'label' => 'Live Chat Title',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('cta_title', [
                'label' => 'CTA Title',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addTextarea('lc_description', [
                'label' => 'Live Chat Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addTextarea('cta_description', [
                'label' => 'CTA Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('lc_btn', [
                'label' => 'Live Chat Button',
                'wrapper' => [
                    'width' => '50%',
                ],
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}