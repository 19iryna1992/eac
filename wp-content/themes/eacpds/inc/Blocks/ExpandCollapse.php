<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class ExpandCollapse extends Block
{
    protected string $name = 'expand-collapse';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Expand/Collapse Section'),
            'description' => __('Add Expand/Collapse Section'),
            'category' => 'formatting',
            'icon' => 'menu-alt3',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'expand-collapse-section.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('collapse_title', [
                'label' => 'Title'
            ])
            ->addText('collapse_subtitle', [
                'label' => 'Subtitle'
            ])
            ->addTextarea('collapse_description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRepeater('collapses', ['min' => 1, 'layout' => 'block', 'collapsed' => 'title', 'button_label' => 'Add item'])
            ->addText('title')
            ->addWysiwyg('collapses_content', [
                'label' => 'Content',
                'media_upload' => 0,
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->addimage('collapses_image', [
                'label' => 'Image',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '50%',
                ]
            ])
            ->addLink('collapses_link', [
                'label' => 'Link',
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}