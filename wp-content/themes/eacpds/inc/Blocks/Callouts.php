<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Callouts extends Block
{
    protected string $name = 'callouts';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Content Callouts'),
            'description' => __('Add Content Callouts'),
            'category' => 'formatting',
            'icon' => 'feedback',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'content-callouts.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addTrueFalse('text_only', [
                'label' => 'Text Only',
                'default_value' => 0,
                'wrapper' => [
                    'width' => '30%',
                ]
            ])
            ->addSelect('orientation', [
                'label' => 'Orientation',
                'default_value' => 'left',
                'wrapper' => [
                    'width' => '30%',
                ],
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->addChoices([
                'left' => 'Left',
                'flex-row-reverse' => 'Right Image',
            ])
            ->addTrueFalse('use_bg', [
                'label' => 'Background Color',
                'wrapper' => [
                    'width' => '30%',
                ]
            ])
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '30%',
                ],
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->addText('title', [
                'label' => 'Title',
                'wrapper' => [
                    'width' => '70%',
                ],
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addLink('button', [
                'label' => 'Button',
                'return_format' => 'array',
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}