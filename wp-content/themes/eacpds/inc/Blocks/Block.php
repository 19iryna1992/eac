<?php


namespace Inc\Blocks;

use Inc\Traits\Singleton;

class Block
{
    use Singleton;

    /**
     * Acf block name.
     *
     * @since 0.1.0
     * @var string $name
     */
    protected string $name = '';
    protected $acf;


    public function __construct($settings)
    {
        $this->registerBlock($settings);
        acf_add_local_field_group($this->registerFields());
    }

    protected function registerBlock($settings)
    {
        $settings['render_callback'] = [$this, 'renderBlockCallback'];
        acf_register_block($settings);
    }


    public function renderBlockCallback($block)
    {
        if (isset( $block['data']['preview_image_hover_editor'] )) {
            echo '<img src="'. $block['data']['preview_image_hover_editor'] .'" style="width:100%; height:auto;">';
        }else{
            $this->acf = get_fields();
            $slug = str_replace('acf/', '', $block['name']);
            if (file_exists(get_theme_file_path("/template-parts/blocks/{$slug}.php"))) {
                include(get_theme_file_path("/template-parts/blocks/{$slug}.php"));
            }
        }

    }


    protected function registerFields(): array
    {
        return [];
    }
}
