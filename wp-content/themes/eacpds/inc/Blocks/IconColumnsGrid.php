<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class IconColumnsGrid extends Block
{
    protected string $name = 'icon-columns-grid';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Icon Columns Grid'),
            'description' => __('Add Icon Columns Grid'),
            'category' => 'formatting',
            'icon' => 'list-view',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'icon-columns-grid.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addWysiwyg('description', [
                'label' => 'Description',
                'media_upload' => 0
            ])
            ->addRepeater('columns', [
                'min' => 1,
                'button_label' => 'Add Column',
                'layout' => 'block',
            ])
            ->addimage('icon', [
                'label' => 'Icon',
                'return_format' => 'id',
            ])
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}