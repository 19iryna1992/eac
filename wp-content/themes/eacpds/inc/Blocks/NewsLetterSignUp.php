<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class NewsLetterSignUp extends Block
{
    protected string $name = 'newsletter-signup';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Newsletter Signup'),
            'description' => __('Add Newsletter Signup'),
            'category' => 'formatting',
            'icon' => 'email-alt',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'newsletter.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addImage('nl_bg_img', [
                'label' => 'BG Image',
            ])
            ->addText('nl_title', [
                'label' => 'Title'
            ])
            ->addTextarea('nl_description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addText('nl_form_shortcode', [
                'label' => 'Form Shortcode'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}