<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class FeaturedProducts extends Block
{
    protected string $name = 'featured-products';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Featured Products'),
            'description' => __('Add Featured Products'),
            'category' => 'formatting',
            'icon' => 'products',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'featured-products.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('section_title',[
                'label'=> 'Title'
            ])
            ->addTextarea('section_description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ]);

        $block
            ->addRepeater('fp_tabs', [
                'min' => 1,
                'button_label' => 'Add Tab',
                'layout' => 'block',
                'collapsed' => 'fp_tab_title'
            ])
            ->addText('fp_tab_title',[
                'label'=> 'Tab title'
            ])
            ->addRelationship('featured_products', [
                'label' => 'Choose Products',
                'post_type' => ['products'],
                'filters' => [
                    0 => 'search',
                ],
            ])
            ->endRepeater()
            ->addLink('fp_tab_btn', [
                'label' => 'Button',
                'return_format' => 'array'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}