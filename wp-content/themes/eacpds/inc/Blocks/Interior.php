<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Interior extends Block
{
    protected string $name = 'interior-page-header';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Interior Page Header'),
            'description' => __('Add Interior Page Header'),
            'category' => 'formatting',
            'icon' => 'analytics',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'interior-page-header.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addSelect('media_type', [
                'label' => 'Media',
                'default_value' => 'image',
            ])
            ->addChoices([
                'image' => 'Image',
                'video' => 'Video',
            ]);
        $block
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'image',
                ]
            ]);
        $block
            ->addimage('video_image', [
                'label' => 'Video Preview Image',
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'video',
                ]
            ]);
        $block
            ->addRadio('video_source', [
                'label' => 'Choose Video Source',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'video',
                ]
            ])
            ->addChoices([
                'local' => 'Upload',
                'video_url_youtube' => 'Youtube',
                'video_url_vimeo' => 'Vimeo Url',
            ]);
        $block
            ->addFile('video_mp4', [
                'label' => 'Upload video (mp4)',
                'conditional_logic' => [
                    'field' => 'video_source',
                    'operator' => '==',
                    'value' => 'local',
                ]
            ]);
        $block
            ->addUrl('video_link', [
                'label' => 'Video Url',
                'conditional_logic' => [
                    [
                        [
                            'field' => 'video_source',
                            'operator'  =>  '==',
                            'value' => 'video_url_youtube',
                        ],
                    ],
                    [
                        [
                            'field' => 'video_source',
                            'operator'  =>  '==',
                            'value' => 'video_url_vimeo',
                        ],
                    ],
                ]
            ]);
        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addText('marquee_title', [
                'label' => 'Marquee Text'
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}