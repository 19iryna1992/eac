<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class PostTypesTabs extends Block
{
    protected string $name = 'post-types-tabs';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Post Types Tabs'),
            'description' => __('Add Post Types Tabs'),
            'category' => 'formatting',
            'icon' => 'table-row-after',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'featured-products-services-solutions-tabs.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);

        $block
            ->addRepeater('tabs', [
                'min' => 1,
                'button_label' => 'Add Tab',
                'layout' => 'block',
                'collapsed' => 'tab_title'
            ])
            ->addText('tab_title')
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRelationship('post_types', [
                'label' => 'Choose Posts and Post Type',
                'post_type' => ['products', 'services', 'solutions'],
                'filters' => [
                    0 => 'search',
                    1 => 'post_type',
                ],
            ])
            ->addLink('tab_btn', [
                'label' => 'Button',
                'return_format' => 'array'
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}