<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Statistics extends Block
{
    protected string $name = 'statistics';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Statistics '),
            'description' => __('Add Statistics'),
            'category' => 'formatting',
            'icon' => 'chart-bar',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'statistics.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addWysiwyg('statistics_description', [
                'label' => 'Description',
                'media_upload' => 0,
            ])
            ->addSelect('statistic_variations', [
                'label' => 'Variations',
                'default_value' => 'large',
            ])
            ->addChoices([
                'large'=>'Large Background Number',
                'columns' => 'Columns grid',
                'rows' => 'Rows',
            ])
            ->addRepeater('blocks', ['min' => 1, 'layout' => 'block', 'collapsed' => 'number', 'button_label' => 'Add item'])
            ->addText('number',[
                'label'=>'Number',
                'conditional_logic' => [
                    'field' => 'statistic_variations',
                    'operator' => '!=',
                    'value' => 'large',
                ]
            ])

            ->addText('bg_before_number_symbol',[
                'label'=>'BG Before Number Symbol',
                'wrapper' => [
                    'width' => '33%',
                ],
                'conditional_logic' => [
                    'field' => 'statistic_variations',
                    'operator' => '!=',
                    'value' => 'large',
                ]
            ])
            ->addText('bg_number',[
                'label'=>'BG Number',
                'wrapper' => [
                    'width' => '33%',
                ],
                'conditional_logic' => [
                    'field' => 'statistic_variations',
                    'operator' => '!=',
                    'value' => 'large',
                ]
            ])
            ->addText('bg_after_number_symbol',[
                'label'=>'BG After Number Symbol',
                'wrapper' => [
                    'width' => '33%',
                ],
                'conditional_logic' => [
                    'field' => 'statistic_variations',
                    'operator' => '!=',
                    'value' => 'large',
                ]
            ])

            ->addText('title')
            ->addWysiwyg('statistics_content', [
                'label' => 'Content',
                'media_upload' => 0,
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}