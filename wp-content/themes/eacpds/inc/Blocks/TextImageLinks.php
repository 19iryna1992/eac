<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class TextImageLinks extends Block
{
    protected string $name = 'text-image-links';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('More Info Text + Image Links'),
            'description' => __('Add More Info Text + Image Links'),
            'category' => 'formatting',
            'icon' => 'media-spreadsheet',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'more-info-text-image-links.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addSelect('cards_variation', [
                'label' => 'Variations',
                'dafault_value'=>'overlap_card_style',
            ])
            ->addChoices([
                'overlap_card_style' => 'Overlap card style',
                'related_documents_style' => 'Related documents style',
            ])
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addRepeater('blocks', ['min' => 1, 'layout' => 'block', 'collapsed' => 'title', 'button_label' => 'Add item'])
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id'
            ])
            ->addText('title')
            ->addText('subtitle')
            ->addWysiwyg('collapses_content', [
                'label' => 'Content',
                'media_upload' => 0,
            ])
            ->addLink('button')
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}