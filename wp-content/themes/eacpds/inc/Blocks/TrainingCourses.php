<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class TrainingCourses extends Block
{
    protected string $name = 'training-courses';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Training Courses Tabs'),
            'description' => __('Add Training Courses Tabs'),
            'category' => 'formatting',
            'icon' => 'table-row-after',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'training-courses.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);

        $block
            ->addRepeater('tabs', [
                'min' => 1,
                'button_label' => 'Add Tab',
                'layout' => 'block',
                'collapsed' => 'tab_title'
            ])
            ->addText('tab_title')
            ->addTrueFalse('empty_tab', [
                'label' => 'Empty Tab',
                'default_value' => 0
            ])
            ->addRelationship('post_types', [
                'label' => 'Training Courses',
                'post_type' => ['training-calendar'],
                'filters' => [
                    0 => 'search',
                ],
                'conditional_logic' => [
                    'field' => 'empty_tab',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])

            ->addText('empty_tab_text', [
                'label' => 'Text',
                'conditional_logic' => [
                    'field' => 'empty_tab',
                    'operator' => '==',
                    'value' => 1,
                ],
                'wrapper' => [
                    'width' => '50%',
                ],
            ])

            ->addLink('empty_tab_button',[
                'label' => 'Button',
                'conditional_logic' => [
                    'field' => 'empty_tab',
                    'operator' => '==',
                    'value' => 1,
                ],
                'wrapper' => [
                    'width' => '20%',
                ],
            ])

            ->addText('related_title', [
                'label' => 'Title',
                'conditional_logic' => [
                    'field' => 'empty_tab',
                    'operator' => '==',
                    'value' => 1,
                ]
            ])
            ->addRepeater('blocks', [
                'min' => 1,
                'layout' => 'block',
                'collapsed' => 'title',
                'button_label' => 'Add item',
                'conditional_logic' => [
                    'field' => 'empty_tab',
                    'operator' => '==',
                    'value' => 1,
                ]
            ])
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id'
            ])
            ->addText('title')
            ->addText('subtitle')
            ->addWysiwyg('collapses_content', [
                'label' => 'Content',
                'media_upload' => 0,
            ])
            ->addLink('button')
            ->endRepeater()
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}