<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class FeaturedContentTabCards extends Block
{
    protected string $name = 'featured-content-tab-cards';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Featured Content Tab Cards'),
            'description' => __('Add Featured Content Tab Cards'),
            'category' => 'formatting',
            'icon' => 'table-row-after',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'featured-content-tab-cards.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Section Title'
            ])
            ->addRepeater('tabs', [
                'min' => 1,
                'collapsed' => 'tab_title',
                'button_label' => 'Add tab',
                'layout' => 'block',
            ])
            ->addText('tab_title', [
                'label' => 'Tab Title',
            ])
            ->addTrueFalse('text_only', [
                'label' => 'Text Only',
                'default_value' => 0,
                'wrapper' => [
                    'width' => '30%',
                ]
            ])
            ->addSelect('orientation', [
                'label' => 'Orientation',
                'default_value' => 'left',
                'wrapper' => [
                    'width' => '30%',
                ],
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->addChoices([
                'left' => 'Left',
                'flex-row-reverse' => 'Right Image',
            ])
            ->addTrueFalse('use_bg', [
                'label' => 'Background Color',
                'wrapper' => [
                    'width' => '30%',
                ]
            ])
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'wrapper' => [
                    'width' => '30%',
                ],
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->addText('title', [
                'label' => 'Title',
                'wrapper' => [
                    'width' => '70%',
                ],
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addLink('button', [
                'label' => 'Button',
                'return_format' => 'array',
                'conditional_logic' => [
                    'field' => 'text_only',
                    'operator' => '==',
                    'value' => 0,
                ]
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}