<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class ContactForm extends Block
{
    protected string $name = 'contact-form';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Contact Form'),
            'description' => __('Add Contact Form'),
            'category' => 'formatting',
            'icon' => 'welcome-widgets-menus',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'contact-form.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addImage('cf_icon_img', [
                'label' => 'Icon',
                'return_format'=>'id'
            ])
            ->addText('cf_title', [
                'label' => 'Title'
            ])
            ->addTextarea('cf_description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addText('cf_form_shortcode', [
                'label' => 'Form Shortcode'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}