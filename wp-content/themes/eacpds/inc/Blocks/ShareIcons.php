<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class ShareIcons extends Block
{
    protected string $name = 'share-icons';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Share Icons'),
            'description' => __('Add Icons'),
            'category' => 'formatting',
            'icon' => 'share',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'share-buttons.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block
            ->addText('social_icons_title', [
                'label' => 'Social Icons Title'
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}