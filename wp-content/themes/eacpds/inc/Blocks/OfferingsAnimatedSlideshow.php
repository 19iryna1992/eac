<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class OfferingsAnimatedSlideshow extends Block
{
    protected string $name = 'offerings-animated-slideshow';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Offerings Animated Slideshow'),
            'description' => __('Offerings Animated Slideshow'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'offerings-animates-slideshow.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('main_title', [
                'label' => 'Section Title'
            ])
            ->addTextarea('main_description', [
                'label' => 'Section Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRepeater('slides', [
                'min' => 1,
                'button_label' => 'Add Slide',
                'layout' => 'block',
            ])
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id',
            ])
            ->addText('marquee_text', [
                'label' => 'Marquee Text'
            ])
            ->addLink('slide_btn', [
                'label' => 'Button',
                'return_format' => 'array'
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}