<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Button extends Block
{
    protected string $name = 'button';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Button'),
            'description' => __('Add Button'),
            'category' => 'formatting',
            'icon' => 'button',
        ]);
    }

    public function getBtn()
    {
        return isset($this->acf['link']) ? $this->acf['link'] : null;
    }

    public function getBtnAction()
    {
        return isset($this->acf['action']) ? $this->acf['action'] : null;
    }

    public function getBtnFile()
    {
        $btn = [];
        $btn['title'] = isset($this->acf['file_title']) ? $this->acf['file_title'] : $this->acf['file']['title'];
        $btn['url'] = $this->acf['file']['url'];
        return $btn;
    }

    public function getBtnClass()
    {
        $class = '';

        switch ($this->acf['design']) {
            case 'solid':
                $class .= 'bg-blu text-white hover:opacity-80';
                break;
            case 'outlined':
                $class .= 'bg-white text-blu hover:bg-blu hover:text-white';
        }

        return $class;
    }

    public function getBtnRel()
    {
        return isset($this->acf['rel']) && $this->acf['rel'] ? 'rel=' . $this->acf['rel'] : null;
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);

        $block
            ->addSelect('design', [
                'default' => 'solid'
            ])
            ->addChoices([
                'solid' => 'Solid',
                'outlined' => 'Outlined'
            ]);

        $block
            ->addSelect('action', [
                'default' => 'link'
            ])
            ->addChoices([
                'link' => 'Link',
                'pdf' => 'PDF'
            ]);

        $block
            ->addLink('link', [
                'conditional_logic' => [
                    'field' => 'action',
                    'operator' => '==',
                    'value' => 'link',
                ]
            ]);

        $block
            ->addText('rel', [
                'conditional_logic' => [
                    'field' => 'action',
                    'operator' => '==',
                    'value' => 'link',
                ]
            ]);
        $block
            ->addText('file_title', [
                'default' => 'Call to action',
                'conditional_logic' => [
                    'field' => 'action',
                    'operator' => '==',
                    'value' => 'pdf',
                ]
            ]);
        $block
            ->addFile('file', [
                'conditional_logic' => [
                    'field' => 'action',
                    'operator' => '==',
                    'value' => 'pdf',
                ]
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}
