<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class FAQS extends Block
{
    protected string $name = 'faqs';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('FAQs'),
            'description' => __('Add FAQs'),
            'category' => 'formatting',
            'icon' => 'editor-help',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'faq.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('faqs_title',[
                'label'=> 'Title'
            ])
            ->addText('faqs_subtitle',[
                'label'=> 'Subtitle'
            ])
            ->addTextarea('faqs_description',[
                'label'=> 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRelationship('faqs', [
                'label' => 'Faqs',
                'post_type' => ['faqs'],
                'filters' => [
                    0 => 'search'
                ],
            ])
            ->addLink('faqs_link', [
                'label' => 'Button',
                'return_format' => 'array'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}