<?php


namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;
class EventsList extends Block
{
    protected string $name = 'events-list';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Events List'),
            'description' => __('Add Event'),
            'category' => 'formatting',
            'icon' => 'tickets-alt',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'events.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('events_title',[
                'label'=> 'Title'
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}