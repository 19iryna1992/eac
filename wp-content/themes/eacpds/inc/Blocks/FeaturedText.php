<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class FeaturedText extends Block
{
    protected string $name = 'featured-text';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Featured Text w/ Background Illustrations'),
            'description' => __('Add Featured Text w/ Background Illustrations'),
            'category' => 'formatting',
            'icon' => 'editor-paste-text',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'featured-text-w-background-illustrations.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addImage('illustration_img', [
                'label' => 'Illustration image',
                'return_format' => 'id',
            ])
            ->addSelect('illustration_position', [
                'label' => 'Illustration position',
                'default_value' => 'right',
            ])
            ->addChoices([
                'right' => 'Right',
                'left' => 'Left',
            ])
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addSelect('description_size', [
                'label' => 'Description size',
                'default_value' => 'text-h4 leading-18',
                'wrapper' => [
                    'width' => '20%',
                ]
            ])
            ->addChoices([
                'text-h4 leading-18' => 'Small',
                'text-md md:text-h3-lg font-bold leading-12 md:leading-16' => 'Big',
            ])
            ->addWysiwyg('description', [
                'label' => 'Description',
                'media_upload' => 0,
                'wrapper' => [
                    'width' => '80%',
                ],
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}