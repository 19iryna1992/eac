<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class SimpleText extends Block
{
    protected string $name = 'simple-text';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Simple Text'),
            'description' => __('Add Text Block'),
            'category' => 'formatting',
            'icon' => 'editor-paste-text',
            'supports' => [
                'anchor' => false,
                'align' => false,
                'mode' => true,
            ]
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addWysiwyg('simple_text', [
                'label' => 'Text Block',
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}