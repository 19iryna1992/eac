<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class AnimatedIconColumns extends Block
{
    protected string $name = 'icon-columns';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Text + Animated Icon Columns'),
            'description' => __('Add Text + Animated Icon Columns'),
            'category' => 'formatting',
            'icon' => 'cover-image',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'text-animated-icon-columns.jpg'
                    ]
                ],
            ],
        ]);
    }

    public function getRowCount()
    {
        return count($this->acf['columns']);
    }

    public function getGridClass()
    {
        $class = '';

        if($this->getRowCount() > 4){
            $class .= '-mx-20'; // 5 columns and more
        }else{
            $class .= '-mx-15'; // 4 columns or less
        }

        return $class;
    }

    public function getColumnClass()
    {
        $class = '';

        if($this->getRowCount() > 4){
            $class .= 'w-full md:w-1/3 xl:w-1/5 p-20'; // 5 columns and more
        }else{
            $class .= 'w-full md:w-1/2 xl:w-1/4 px-15 py-20'; // 4 columns or less
        }

        return $class;
    }

    public function getImageClass(){
        $class = '';

        if($this->getRowCount() > 4){
            $class .= ' md:py-40'; // 5 columns and more
        }else{
            $class .= 'md:px-25 md:py-70 md:py-80'; // 4 columns or less
        }

        return $class;
    }

    public function getImageSize(){
        $size = '';

        if($this->getRowCount() > 4){
            $size .= 'thumbnail'; // 5 columns and more
        }else{
            $size .= 'icon-big'; // 4 columns or less
        }

        return $size;
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addWysiwyg('description', [
                'label' => 'Description',
                'media_upload' => 0
            ])
            ->addRepeater('columns', [
                'min' => 1,
                'button_label' => 'Add Column',
                'layout' => 'block',
            ])
            ->addimage('icon', [
                'label' => 'Icon',
                'return_format' => 'id',
            ])
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addWysiwyg('description', [
                'label' => 'Description',
                'media_upload' => 0,
            ])
            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}