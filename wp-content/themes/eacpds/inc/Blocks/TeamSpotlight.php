<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class TeamSpotlight extends Block
{
    protected string $name = 'team-spotlight';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Team Spotlight'),
            'description' => __('Add Team Spotlight'),
            'category' => 'formatting',
            'icon' => 'groups',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'team-spotlight.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('teams_title',[
                'label'=> 'Title'
            ])
            ->addRelationship('teams', [
                'label' => 'Teams',
                'post_type' => ['members'],
                'filters' => [
                    0 => 'search'
                ],
            ])
          /*  ->addLink('teams_link', [
                'label' => 'Button',
                'return_format' => 'array'
            ])*/;


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}