<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class TextImage extends Block
{
    protected string $name = 'text-image';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Text + Image Section'),
            'description' => __('Add Text + Image Section'),
            'category' => 'formatting',
            'icon' => 'analytics',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'text-image-section.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Section Title'
            ])
            ->addSelect('variations', [
                'label' => 'Choose variations',
                'default_value' => 'media_left_text_right',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addChoices([
                'media_left_text_right' => 'Media Left / Text Right',
                'media_right_text_left_illustration' => 'Media Right / Text Left w/ Background Illustration',
                'small_media_text' => 'Small Media + Text',
                'media_bullets' => 'Media + Bullets',
                'full_width_media' => 'Full Width Media / Text Stack',
            ])
            ->addImage('illustration_image', [
                'label' => 'Illustration Image',
                'conditional_logic' => [
                    'field' => 'variations',
                    'operator' => '==',
                    'value' => 'media_right_text_left_illustration',
                ]
            ])
            ->addSelect('text_size', [
                'label' => 'Text size',
                'default_value' => 'text-sm leading-20',
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addChoices([
                'text-sm leading-20' => 'Small',
                'text-sm lg:text-base leading-16 lg:leading-21' => 'Middle',
                'text-sm lg:text-h4 leading-16 lg:leading-18' => 'Large',
            ])
            ->addSelect('media_type', [
                'label' => 'Media',
                'default_value' => 'image',
            ])
            ->addChoices([
                'image' => 'Image',
                'video' => 'Video',
            ])
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'image',
                ],
                'wrapper' => [
                    'width' => '40%',
                ],
            ])
            ->addText('caption')
            ->addimage('video_image', [
                'label' => 'Video Preview Image',
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'video',
                ]
            ])
            ->addRadio('video_source', [
                'label' => 'Choose Video Source',
                'conditional_logic' => [
                    'field' => 'media_type',
                    'operator' => '==',
                    'value' => 'video',
                ]
            ])
            ->addChoices([
                'local' => 'Upload',
                'video_url_youtube' => 'Youtube',
                'video_url_vimeo' => 'Vimeo Url',
            ])
            ->addFile('video_mp4', [
                'label' => 'Upload video (mp4)',
                'conditional_logic' => [
                    'field' => 'video_source',
                    'operator' => '==',
                    'value' => 'local',
                ]
            ])
            ->addUrl('video_link', [
                'label' => 'Video Url',
                'conditional_logic' => [
                    [
                        [
                            'field' => 'video_source',
                            'operator'  =>  '==',
                            'value' => 'video_url_youtube',
                        ],
                    ],
                    [
                        [
                            'field' => 'video_source',
                            'operator'  =>  '==',
                            'value' => 'video_url_vimeo',
                        ],
                    ],
                ]
            ])

            ->addSelect('content_columns', [
                'label' => 'Content Columns',
                'default_value' => 'one_column',
                'conditional_logic' => [
                    'field' => 'variations',
                    'operator' => '==',
                    'value' => 'small_media_text',
                ],
            ])
            ->addChoices([
                'one_column' => 'One Column',
                'two_column' => 'Two Column',
            ])

            ->addWysiwyg('content', [
                'label' => 'Content',
                'media_upload' => 0,
                'wrapper' => [
                    'width' => '50%',
                ],
                'conditional_logic' => [
                    'field' => 'variations',
                    'operator' => '!=',
                    'value' => 'media_bullets',
                ],

            ])
            ->addWysiwyg('content_two_column', [
                'label' => 'Content',
                'media_upload' => 0,
                'wrapper' => [
                    'width' => '50%',
                ],
                'conditional_logic' => [
                    'field' => 'variations',
                    'operator' => '==',
                    'value' => 'small_media_text',
                ],

            ])

            ->addRepeater('bullets_list', [
                'min' => 1,
                'button_label' => 'Add Item',
                'layout' => 'block',
                'conditional_logic' => [
                    'field' => 'variations',
                    'operator' => '==',
                    'value' => 'media_bullets',
                ]
            ])
            ->addText('title')
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '2',
            ])
            ->endRepeater()
            ->addLink('button', [
                'label' => 'Button'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}