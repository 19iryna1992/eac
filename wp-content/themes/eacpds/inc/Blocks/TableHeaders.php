<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class TableHeaders extends Block
{
    protected string $name = 'table-headers';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Table Headers'),
            'description' => __('Add Table Headers'),
            'category' => 'formatting',
            'icon' => 'align-center',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'table-headers.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('t_headers_title', [
                'label' => 'Title'
            ]);

        $block
            ->addRepeater('t_headers', [
                'label' => 'Blocks',
                'button_label' => 'Add Block',
                'layout' => 'block',
                'collapsed' => 'main_title'
            ])
            ->addSelect('t_column_design', [
                'label' => 'Design',
                'default_value' => 'bordered',
            ])
            ->addChoices([
                'bordered' => 'Bordered Block',
                'colored' => 'Colored Header',
            ])
            ->addText('sub_title', [
                'label' => 'Top title',
                'conditional_logic' => [
                    'field' => 't_column_design',
                    'operator' => '==',
                    'value' => 'colored',
                ]
            ])
            ->addText('main_title', [
                'label' => 'Main title'
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addText('price')
            ->addText('price_description')
            ->endRepeater();

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}