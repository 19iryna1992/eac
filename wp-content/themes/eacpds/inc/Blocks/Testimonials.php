<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Testimonials extends Block
{
    protected string $name = 'testimonials';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Testimonials'),
            'description' => __('Add Testimonials'),
            'category' => 'formatting',
            'icon' => 'format-status',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'testimonials.jpg'
                    ]
                ],
            ],
        ]);
    }

    public function getStarRating($rating)
    {
        $stars = '';

        switch ($rating) {
            case 0:
                $stars = '';
                break;
            case 1:
                $stars = '<span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span>';
                break;
            case 2:
                $stars = '<span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span>';
                break;
            case 3:
                $stars = '<span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span>';
                break;
            case 4:
                $stars = '<span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-grey-light opacity-80') . '</span>';
                break;
            case 5:
                $stars = '<span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span><span class="inline-block px-5">' . get_star_icon('text-yellow') . '</span>';
                break;
        }

        return $stars;
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ]);
        $block
            ->addSelect('testimonials_type', [
                'label' => 'Choose Testimonials Type',
                'default_value' => 'small',
            ])
            ->addChoices([
                'small' => 'Small Testimonials',
                'single' => 'Single Testimonials',
                'slideshow' => 'Testimonials Slideshow',
            ]);
        $block
            ->addRelationship('testimonials', [
                'label' => 'Testimonials',
                'post_type' => ['testimonials'],
                'filters' => [
                    0 => 'search'
                ],
            ]);
        $block
            ->addLink('testimonials_btn', [
                'label' => 'Button',
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}