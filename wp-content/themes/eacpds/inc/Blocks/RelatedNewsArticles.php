<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class RelatedNewsArticles extends Block
{
    protected string $name = 'related-news-articles';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Related News/Articles'),
            'description' => __('Add Related News/Articles'),
            'category' => 'formatting',
            'icon' => 'clipboard',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'related-news.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);

        $block
            ->addSelect('articles_block_type', [
                'label' => 'Choose Block Type',
                'default_value' => 'slider',
            ])
            ->addChoices([
                'slider' => 'Slider',
                'columns' => 'Columns',
            ]);
        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addTextarea('description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
                'conditional_logic' => [
                    'field' => 'articles_block_type',
                    'operator' => '==',
                    'value' => 'slider',
                ]
            ]);

        $block
            ->addRelationship('post_types', [
                'label' => 'Choose Posts',
                'post_type' => ['post'],
                'filters' => [
                    0 => 'search',
                ],
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}