<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class CareersFilter extends Block
{
    protected string $name = 'careers-filter';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Careers Filter'),
            'description' => __('Careers Filter'),
            'category' => 'formatting',
            'icon' => 'filter',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'careers.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('careers_filter_title', [
                'label' => 'Careers Filter Title'
            ])
            ->addTextarea('careers_filter_description',[
                'label'=> 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}