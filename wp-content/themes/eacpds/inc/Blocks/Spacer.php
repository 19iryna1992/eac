<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class Spacer extends Block
{
    protected string $name = 'spacer';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Spacer'),
            'description' => __('Add Spacer'),
            'category' => 'formatting',
            'icon' => 'move',
            'supports' => [
                'anchor' => false,
                'align' => false,
                'mode' => true,
            ]
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addSelect('spacer', [
                'label' => 'Spacer height',
                'default_value' => 'h-20',
            ])
            ->addChoices([
                'h-20'=>'20px',
                'h-15 md:h-30'=>'30px',
                'h-20 md:h-40'=>'40px',
                'h-30 md:h-50'=>'50px',
                'h-40 md:h-60'=>'60px',
                'h-60 md:h-100'=>'100px',
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}