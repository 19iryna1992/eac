<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class TimelineSlideshow extends Block
{
    protected string $name = 'timeline-slideshow';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Timeline Slideshow'),
            'description' => __('Add Timeline Slideshow'),
            'category' => 'formatting',
            'icon' => 'clock',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'timeline-slideshow.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('title', [
                'label' => 'Title'
            ])
            ->addWysiwyg('timeline_description', [
                'label' => 'Description',
                'media_upload' => 0,
            ])
            ->addRepeater('blocks', ['min' => 1, 'layout' => 'block', 'collapsed' => 'years', 'button_label' => 'Add item'])
            ->addText('image_label')
            ->addimage('image', [
                'label' => 'Image',
                'return_format' => 'id'
            ])
            ->addText('years')
            ->addText('overlay_years')
            ->addText('title')
            ->addWysiwyg('timeline_content', [
                'label' => 'Content',
                'media_upload' => 0,
            ])
            ->endRepeater()

        ->addLink('timeline_button', [
        'label' => 'Button'
    ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}