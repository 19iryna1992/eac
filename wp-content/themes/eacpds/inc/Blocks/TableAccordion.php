<?php


namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class TableAccordion extends Block
{
    protected string $name = 'table-accordion';

    public function __construct()
    {
        parent::__construct([
            'name' => $this->name,
            'title' => __('Table Accordion'),
            'description' => __('Add Table'),
            'category' => 'formatting',
            'icon' => 'editor-table',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'blog-subscription.jpg'
                    ]
                ],
            ],
        ]);
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);

        $block

            // Count column

         ->addSelect('table_accordion_count_columns', [
                'default' => 'four'
            ])
            ->addChoices([
                'four' => 'Four',
                'five' => 'Five',
            ])


            // Popular

            ->addTrueFalse('popular_accordion_column_first', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTrueFalse('popular_accordion_column_second', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTrueFalse('popular_accordion_column_third', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTrueFalse('popular_accordion_column_fourth', [
                'label' => 'Most Popular',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])


            // Titles

            ->addText('title_accordion_table_first', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('title_accordion_table_second', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('title_accordion_table_third', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('title_accordion_table_fourth', [
                'wrapper' => [
                    'width' => '25%',
                ],
            ])


            //Descriptions

            ->addTextarea('description_accordion_table_first', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTextarea('description_accordion_table_second', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTextarea('description_accordion_table_third', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addTextarea('description_accordion_table_fourth', [
                'new_lines' => 'wpautop',
                'rows' => '3',
                'wrapper' => [
                    'width' => '25%',
                ],
            ]);


        // Repeater

        $block
            ->addRepeater('table_accordion', [
                'label' => 'Table',
                'button_label' => 'Add Table',
                'layout' => 'block',

            ])
            ->addText('table_accordion_btn_label', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            // Price Label

            ->addText('price_accordion_table_second', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('price_accordion_table_third', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('price_accordion_table_fourth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])

            ->addText('price_accordion_table_fifth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])

            // Rows Repeater
            ->addRepeater('table_accordion_row', [
                'label' => 'Table Row',
                'button_label' => 'Add Column',
                'layout' => 'block',
            ])
            ->addSelect('type_cell_first', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_second', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_third', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_fourth', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addSelect('type_cell_fifth', [
                'default' => 'text',
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addChoices([
                'checked' => 'Checked',
                'text' => 'Text',
                'empty' => 'Empty',
            ])
            ->addText('cell_text_first', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_second', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_third', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_fourth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->addText('cell_text_fifth', [
                'wrapper' => [
                    'width' => '20%',
                ],
            ])
            ->endRepeater()


            // Buttons

            ->addLink('table_btn_second', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('table_btn_third', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('table_btn_fourth', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('table_btn_fifth', [
                'label' => 'Button',
                'return_format' => 'array',
                'wrapper' => [
                    'width' => '25%',
                ],
            ])

            ->endRepeater();


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }

}


