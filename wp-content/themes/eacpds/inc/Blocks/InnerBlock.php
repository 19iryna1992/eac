<?php


namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class InnerBlock extends Block
{
    protected string $name = 'inner-block';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Inner Blocks'),
            'description' => __('Add Inner blocks.'),
            'category' => 'formatting',
            'icon' => 'admin-site-alt2',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => false,
                'jsx' => true
            ]
        ]);
    }

    protected function allowedBlocks()
    {
        return [
            'core/paragraph',
            'core/heading',
            'core/list',
            'core/image',
        ];
    }

    public function getBgType()
    {
        return $this->acf['background'];
    }

    public function getBgColor()
    {
        return isset($this->acf['bg_color']) ? $this->acf['bg_color'] : null;
    }

    public function getBgImageID()
    {
        return isset($this->acf['background_image']) ? $this->acf['background_image'] : null;
    }

    public function getBgImagePosition()
    {
        return isset($this->acf['background_position']) ? $this->acf['background_position'] : null;
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);

        $block
            ->addSelect('background')
            ->addChoices([
                'none' => 'None',
                'color' => 'Color',
                'image' => 'Image'
            ]);

        $block
            ->addImage('background_image', [
                'return_format' => 'id',
                'conditional_logic' => [
                    'field' => 'background',
                    'operator' => '==',
                    'value' => 'image',
                ]
            ])
            ->addSelect('background_position', [
                'label' => __('Background Position'),
                'default' => 'bg-center',
                'choices' => [
                    'object-top' => __('Top', 'sage'),
                    'object-center' => __('Center', 'sage'),
                    'object-bottom' => __('Bottom', 'sage'),
                ],
                'conditional_logic' => [
                    'field' => 'background',
                    'operator' => '==',
                    'value' => 'image',
                ]
            ]);

        $block
            ->addSelect('bg_color', [
                'conditional_logic' => [
                    'field' => 'background',
                    'operator' => '==',
                    'value' => 'color',
                ]
            ])
            ->addChoices([
                'bg-white' => 'White',
                'bg-blu' => __('Blu', 'sage'),
                'bg-alr-err' => __('Red', 'sage'),
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));

        return $block->build();
    }
}
