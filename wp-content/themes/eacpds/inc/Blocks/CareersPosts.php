<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class CareersPosts extends Block
{
    protected string $name = 'careers';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Careers'),
            'description' => __('Add Careers posts'),
            'category' => 'formatting',
            'icon' => 'businessperson',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'careers.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('careers_title',[
                'label'=> 'Title'
            ])
            ->addTextarea('careers_description',[
                'label'=> 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addRelationship('careers', [
                'label' => 'Careers',
                'post_type' => ['careers'],
                'filters' => [
                    0 => 'search'
                ],
            ])
            ->addLink('careers_link', [
                'label' => 'Button',
                'return_format' => 'array'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}