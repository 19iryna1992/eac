<?php


namespace Inc\Blocks;


use StoutLogic\AcfBuilder\FieldsBuilder;

class BlogSubscription extends Block
{
    protected string $name = 'blog-subscription';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Blog Subscription'),
            'description' => __('Add Blog Subscription'),
            'category' => 'formatting',
            'icon' => 'email-alt',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'blog-subscription.jpg'
                    ]
                ],
            ],
        ]);
    }


    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);


        $block
            ->addText('bs_title', [
                'label' => 'Title'
            ])
            ->addTextarea('bs_description', [
                'label' => 'Description',
                'new_lines' => 'wpautop',
                'rows' => '4',
            ])
            ->addText('bs_form_shortcode', [
                'label' => 'Form Shortcode'
            ]);


        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));


        return $block->build();
    }
}