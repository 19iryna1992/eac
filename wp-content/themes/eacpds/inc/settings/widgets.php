<?php

if (!defined('ABSPATH')) exit;

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_widgets_init()
{

    $config = [
        'before_widget' => '<div class="c-widget %1$s c-%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h6 class="c-widget__title font-roboto font-bold text-h6 leading-12 tracking-sm  text-white normal-case mb-30 lg:mb-35 JS--widget-title">',
        'after_title' => '</h6>'
    ];
    register_sidebar([
            'name' => __('Footer 1', 'widget'),
            'id' => 'footer-1'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 2', 'widget'),
            'id' => 'footer-2'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 3', 'widget'),
            'id' => 'footer-3'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 4', 'widget'),
            'id' => 'footer-4'
        ] + $config);
    register_sidebar([
            'name' => __('Footer 5', 'widget'),
            'id' => 'footer-5'
        ] + $config);
    register_sidebar([
            'name' => __('Footer Social Menu', 'widget'),
            'id' => 'footer-6'
        ] + $config);
    register_sidebar([
            'name' => __('Footer Contacts', 'widget'),
            'id' => 'footer-7'
        ] + $config);
}

add_action('widgets_init', 'theme_widgets_init');