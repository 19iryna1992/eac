<?php

if (!defined('ABSPATH')) exit;

if (!function_exists('eacpds_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function eacpds_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Chiquita, use a find and replace
         * to change 'chiquita' to the name of your theme in all the template files.
         */
        load_theme_textdomain('eacpds', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');


        // image sizes
        //add_image_size('latest-news-thumb', 176, 176, true);

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        //Image sizes
        add_image_size('cta', 380, 9999);
        add_image_size('logo-nav', 160, 9999);
        add_image_size('team-member', 420, 514, true);
        add_image_size('accordion-image', 340, 9999, true);
        add_image_size('big-image', 720, 9999, true);
        add_image_size('small-media', 562, 9999, true);
        add_image_size('card-overlap-image', 362, 438, true);
        add_image_size('middle-square', 316, 316, true);
        add_image_size('grid-image', 604, 540, true);
        add_image_size('icon-big', 240, 240, true);
        add_image_size('author-avatar', 80, 80, true);
        add_image_size('post-tab', 406, 260, true);
        add_image_size('timeline-img', 480, 400, true);
        add_image_size('interior-img', 480, 600, true);
        add_image_size('header-slider-img', 750, 880, true);
        add_image_size('post-slider-thumb', 520, 434, true);
        add_image_size('post-columns-thumb', 390, 338, true);
        add_image_size('resources-menu-img', 160, 90, true);
        add_image_size('post-author-img', 120, 120, true);

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus([
            'primary-menu' => esc_html__('Primary menu', 'eacpds'),
            'bottom-footer-menu' => esc_html__('Bottom footer menu', 'eacpds'),
        ]);

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('eacpds_custom_background_args', [
            'default-color' => '#ffffff',
            'default-image' => '',
        ]));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', [
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ]);
    }
endif;
add_action('after_setup_theme', 'eacpds_setup');
