<?php

if (!defined('ABSPATH')) exit;

/**
 * Styles and Scripts Loader
 */
class ScriptStyleLoader
{

    const JQUERY = 'jquery';

    const eacpds_JS = 'eacpds_js';

    const eacpds_STYLES = 'eacpds_styles';

    const eacpds_FONTS = 'eacpds_fonts';

    /**
     * Constructor
     */
    function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'styles']);
        add_action('wp_enqueue_scripts', [$this, 'scripts']);
        add_action('enqueue_block_editor_assets', [$this, 'editorStyles']);
        add_action('admin_enqueue_scripts', [$this, 'editorScripts']);
    }

    /**
     * Styles Loader
     */
    function styles()
    {
        //Remove the Gutenberg Block Library CSS
        wp_dequeue_style('wp-block-library');
        wp_dequeue_style('wp-block-library-theme');


        //load fonts (If need more use | seprator )
        //wp_enqueue_style('poppins-fonts', "//fonts.googleapis.com/css2?family=Poppins:wght@400;500;700", '', false, 'screen');

        if (!is_admin()) {
            wp_register_style(self::eacpds_STYLES, mix('/dist/css/style.css'), false, null);
            wp_enqueue_style(self::eacpds_STYLES);
        }
    }

    /**
     * Scripts Loader
     */
    function scripts()
    {
        if (!is_admin()) {
            wp_deregister_script(self::JQUERY);
            wp_register_script(self::JQUERY, mix('/dist/js/app.js'), [], null, true);
            //example: how to register additional js file
            //wp_register_script('some-prefix', mix('/dist/js/separateScriptExample.js'), [], null, true);

            wp_enqueue_script(self::JQUERY);
            //example: activating additional js file on some pages
            //if (is_page_template('page-templates/template-our-works.php') || is_home()) wp_enqueue_script('some-prefix');

            $script_vars = ['templateUrl' => get_stylesheet_directory_uri()];
            wp_localize_script(self::eacpds_JS, 'scriptVars', $script_vars);
        }
    }

    function editorStyles()
    {
        /**
         * Add block editor assets
         */
        wp_enqueue_style('editor-styles', mix('/dist/css/editor.css'), array('wp-editor'), null);
    }

    function editorScripts()
    {
        /**
         * Add block editor scripts
         */
        wp_enqueue_script('editor-scripts', mix('/dist/js/editor.js'), ['wp-blocks', 'wp-dom'], false, true);
    }
}

new ScriptStyleLoader;
