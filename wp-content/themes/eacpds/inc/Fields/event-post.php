<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'event_info',
    [
        'title' => __('Event Info', 'sage'),
        'position' => 'acf_after_title'
    ]
);

$group
    ->addDatePicker('event_date',[
        'label'=>'Event Date',
        'display_format' => 'd/M/Y',
        'return_format' => 'd M Y',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addUrl('event_url', [
        'label' => 'Event URL',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->setLocation('post_type', '==', 'events');

return $group;
