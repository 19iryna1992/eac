<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'course_info',
    [
        'title' => __('Course Info', 'eacpds'),
        'position' => 'acf_after_title'
    ]
);

$group
    ->addDatePicker('course_date',[
        'label'=>'Date',
        'display_format' => 'd/M/Y',
        'return_format' => 'd M Y',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addTimePicker('course_time',[
        'label'=>'Time',
        'display_format' => 'g:i a',
        'return_format' => 'g:i a',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addText('price', [
        'label' => 'Price',
        'prepend' => '$',
    ])
    ->addUrl('register_url', [
        'label' => 'Registration Url'
    ])
    ->setLocation('post_type', '==', 'training-calendar');

return $group;