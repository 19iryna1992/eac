<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;



$group = new FieldsBuilder(
    'widget-social-menu',
    [
        'title' => __('Social menu list', 'md'),
    ]
);

$group
    ->addText('main_title', [
        'label' => 'Title',
    ])
    ->addRepeater('socials_list', [
        'min' => 1,
        'collapsed' => 'icon_title',
        'button_label' => 'Add item',
        'layout' => 'block',
    ])
    ->addText('icon_title', [
        'label' => 'Icon Title',
    ])
    ->addImage('icon', [
        'label' => 'Icon',
        'return_format' => 'id',
    ])
    ->addUrl('link', [
        'label' => 'Url',
    ])
    ->endRepeater()

    ->setLocation('widget', '==', 'social_menu_widget');

return $group;
