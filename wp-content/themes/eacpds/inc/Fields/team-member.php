<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'member_info',
    [
        'title' => __('Member Info', 'sage'),
        'position' => 'acf_after_title'
    ]
);

$group
    ->addText('member_position', [
        'label' => 'Position'
    ])
    ->addUrl('member_linkedin', [
        'label' => 'Linkedin'
    ])
    ->addUrl('member_twitter', [
        'label' => 'Twitter'
    ])
    ->setLocation('post_type', '==', 'members');

return $group;
