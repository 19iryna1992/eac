<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'archive_pages_links',
    [
        'title' => __('Archive pages links', 'eacpds'),
    ]
);

$group
    ->addLink('products_link', [
        'label' => 'Products Page',
    ])
    ->addLink('services_link', [
        'label' => 'Services Page',
    ])
    ->addLink('solutions_link', [
        'label' => 'Solutions Page',
    ])
    ->addLink('careers_link', [
        'label' => 'Careers Page',
    ])
    ->setLocation('options_page', '==', 'archive_page_links');

return $group;
