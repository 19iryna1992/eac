<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;



$group = new FieldsBuilder(
    'tracking',
    [
        'title' => __('Site Tracking Scripts', 'sage'),
    ]
);

$group
    ->addTextArea('global_tracking_metas', [
        'label' => 'Global Tracking Metas'
    ])
    ->addTextArea('global_tracking_head', [
        'label' => 'Head Tracking <strong>Tracking Scripts included in the HEAD</strong>'
    ])
    ->addTextArea('global_tracking_body', [
        'label' => 'Body Tracking <strong>Tracking Pixels included after opening of BODY tag</strong>'
    ])
    ->addTextArea('global_tracking_footer', [
        'label' => 'Footer Tracking <strong>Scripts and Pixels included before closing HTML tag</strong>'
    ])

    ->setLocation('options_page', '==', 'tracking');

return $group;
