<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'menu_items_checkboxes',
    [
        'title' => __('Mega menu', 'sage'),
        'position' => 'normal',
        'style' => 'default',
        //'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'menu_order' => 0,
        'active' => true,
    ]
);

$group
    ->addSelect('mega_menu_types', [
        'label'=>'Choose mega menu',
        'default_value' => 'default_menu',
        'return_format' => 'value',
    ])
    ->addChoices([
        'default_menu' => 'Default Menu',
        'products_menu' => 'Products Menu',
        'services_menu' => 'Services Menu',
        'solutions_menu' => 'Solutions Menu',
        'about_menu' => 'About us Menu',
        'resources_menu' => 'Resources Menu',
    ])
    ->setLocation('nav_menu_item', '==', "location/primary-menu");

return $group;
