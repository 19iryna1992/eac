<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'post_infos',
    [
        'title' => __('Post Additional Info', 'eacpds'),
        'position' => 'side'
    ]
);

$group
    ->addText('read_time', [
        'label' => 'Read Time'
    ])
    ->addRelationship('post_author',[
        'label' => 'Post Author',
        'post_type' => ['members'],
        'filters' => [
            0 => 'search',
        ],
        'max' => '1',
        'return_format' => 'object',
    ])
    ->addUrl('offsite_link')
    ->setLocation('post_type', '==', 'post');

return $group;