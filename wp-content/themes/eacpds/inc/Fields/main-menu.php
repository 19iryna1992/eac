<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'main_menu_settings',
    [
        'title' => __('Main menu', 'sage'),
    ]
);

$group
    ->addTab('Menu Button')
    ->addLink('menu_btn', [
        'label' => 'Menu Button'
    ]);
$group
    ->addTab('Products Mega Menu')
    ->addRepeater('products_menu', [
        'min' => 1,
        'button_label' => 'Add Menu',
        'layout' => 'block',
    ])
    ->addLink('products_primary_menu_item', [
        'label' => 'Primary Item',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addRepeater('products_sub_menu', [
        'button_label' => 'Add Menu Items',
        'layout' => 'block',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addLink('menu_item', [
        'label' => 'Menu Item',
    ])
    ->endRepeater()
    ->endRepeater();
$group
    ->addTab('Services Mega Menu')
    ->addRepeater('services_menu', [
        'button_label' => 'Add Menu',
        'layout' => 'block',
    ])
    ->addLink('services_primary_menu_item', [
        'label' => 'Primary Item',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addRepeater('services_sub_menu', [
        'min' => 1,
        'button_label' => 'Add Menu Items',
        'layout' => 'block',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addLink('menu_item', [
        'label' => 'Menu Item',
    ])
    ->endRepeater()
    ->endRepeater();
$group
    ->addTab('Solutions Mega Menu')
    ->addRepeater('solutions_menu', [
        'button_label' => 'Add Menu',
        'layout' => 'block',
    ])
    ->addLink('solutions_primary_menu_item', [
        'label' => 'Primary Item',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addRepeater('solutions_sub_menu', [
        'min' => 1,
        'button_label' => 'Add Menu Items',
        'layout' => 'block',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addLink('menu_item', [
        'label' => 'Menu Item',
    ])
    ->endRepeater()
    ->endRepeater();
$group
    ->addTab('About us Mega Menu')
    ->addRepeater('about_sub_menu', [
        'button_label' => 'Add Menu Items',
        'layout' => 'block',
    ])
    ->addImage('menu_item_img', [
        'label' => 'Menu Item Img',
        'return_format' => 'id',
        'preview_size' => 'thumbnail',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addLink('menu_item', [
        'label' => 'Menu Item',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->endRepeater();
$group
    ->addTab('Resources Mega Menu')
    ->addRepeater('resources_sub_menu', [
        'button_label' => 'Add Menu Items',
        'layout' => 'block',
    ])
    ->addImage('menu_item_img', [
        'label' => 'Menu Item Img',
        'return_format' => 'id',
        'preview_size' => 'thumbnail',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->addLink('menu_item', [
        'label' => 'Menu Item',
        'wrapper' => [
            'width' => '50%',
        ]
    ])
    ->endRepeater()
    ->setLocation('options_page', '==', 'main_menu');

return $group;
