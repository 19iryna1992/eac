<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'category_image',
    [
        'title' => __('Category Image', 'eacpds'),
    ]
);

$group
    ->addImage('cat_img', [
        'label' => 'Icon'
    ])
    ->setLocation('taxonomy', '==', 'category');

return $group;