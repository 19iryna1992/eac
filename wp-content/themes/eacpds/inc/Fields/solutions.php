<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'solutions_info',
    [
        'title' => __('Solution Info', 'sage'),
        'position' => 'side'
    ]
);

$group
    ->addImage('small_logo', [
        'label' => 'Logo',
        'return_format' => 'id',
    ])
    ->setLocation('post_type', '==', 'solutions');

return $group;
