<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'testimonial_info',
    [
        'title' => __('Testimonial info', 'eacpds'),
        'position' => 'acf_after_title'
    ]
);

$group
    ->addText('role', [
        'label' => 'Role',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addNumber('rating', [
        'label' => 'Rating',
        'wrapper' => [
            'width' => '50%',
        ],
        'default_value' => '',
        'min' => '0',
        'max' => '5',
        'step' => '1',
    ])
    ->addTextarea('quote', [
        'label' => 'Quote',
        'new_lines' => 'wpautop',
        'rows' => '4',
    ])
    ->setLocation('post_type', '==', 'testimonials');

return $group;