<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'product_info',
    [
        'title' => __('Product Info', 'sage'),
        'position' => 'side'
    ]
);

$group
    ->addImage('small_logo', [
        'label' => 'Logo',
        'return_format' => 'id',
    ])
    ->addUrl('product_trial', [
        'Label' => 'Trial url'
    ])
    ->setLocation('post_type', '==', 'products');

return $group;
