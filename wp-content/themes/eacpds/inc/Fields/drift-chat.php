<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;



$group = new FieldsBuilder(
    'drift_chat',
    [
        'title' => __('Drift Chat Scripts', 'sage'),
    ]
);

$group
    ->addTextArea('drift_chat_script', [
        'label' => 'Drift Chat Code'
    ])

    ->setLocation('options_page', '==', 'drift_chat_code');

return $group;
