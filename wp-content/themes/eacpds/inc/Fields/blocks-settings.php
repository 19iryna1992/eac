<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;



$group = new FieldsBuilder(
    'block_settings',
    [
        'title' => __('Gutenberg blocks settings', 'sage'),
    ]
);

$group
    ->addTab('Faqs')
    ->addText('faqs_title',[
        'label'=> 'Title'
    ])
    ->addText('faqs_subtitle',[
        'label'=> 'Subtitle'
    ])
    ->addTextarea('faqs_description',[
        'label'=> 'Description',
        'new_lines' => 'wpautop',
        'rows' => '4',
    ])
    ->addLink('faqs_link', [
        'label' => 'Button',
        'return_format' => 'array'
    ])

    ->addTab('Newsletter signup')
    ->addImage('nl_bg_img', [
        'label' => 'BG Image',
    ])
    ->addText('nl_title', [
        'label' => 'Title'
    ])
    ->addTextarea('nl_description', [
        'label' => 'Description',
        'new_lines' => 'wpautop',
        'rows' => '4',
    ])
    ->addText('nl_form_shortcode', [
        'label' => 'Form Shortcode'
    ])
    ->addTab('Contact Form')
    ->addImage('cf_icon_img', [
        'label' => 'Icon',
        'return_format'=>'id'
    ])
    ->addText('cf_title', [
        'label' => 'Title'
    ])
    ->addTextarea('cf_description', [
        'label' => 'Description',
        'new_lines' => 'wpautop',
        'rows' => '4',
    ])
    ->addText('cf_form_shortcode', [
        'label' => 'Form Shortcode'
    ])
    ->addTab('Live Chat / CTA')
    ->addImage('lc_icon', [
        'label' => 'Live Chat Icon',
        'return_format' => 'id',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addImage('cta_icon', [
        'label' => 'CTA Icon',
        'return_format' => 'id',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addText('lc_title', [
        'label' => 'Live Chat Title',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addText('cta_title', [
        'label' => 'CTA Title',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addTextarea('lc_description', [
        'label' => 'Live Chat Description',
        'new_lines' => 'wpautop',
        'rows' => '4',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addTextarea('cta_description', [
        'label' => 'CTA Description',
        'new_lines' => 'wpautop',
        'rows' => '4',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addText('lc_btn', [
        'label' => 'Live Chat Button',
        'wrapper' => [
            'width' => '50%',
        ],
    ])
    ->addTab('Blog subscription')
    ->addText('bs_title', [
        'label' => 'Title'
    ])
    ->addTextarea('bs_description', [
        'label' => 'Description',
        'new_lines' => 'wpautop',
        'rows' => '4',
    ])
    ->addText('bs_form_shortcode', [
        'label' => 'Form Shortcode'
    ])

    ->setLocation('options_page', '==', 'block_settings');

return $group;
