<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;


$group = new FieldsBuilder(
    'service_info',
    [
        'title' => __('Service Info', 'sage'),
        'position' => 'side'
    ]
);

$group
    ->addImage('small_logo', [
        'label' => 'Logo',
        'return_format' => 'id',
    ])
    ->setLocation('post_type', '==', 'services');

return $group;
