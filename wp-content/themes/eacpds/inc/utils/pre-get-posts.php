<?php
if (!defined('ABSPATH')) exit;


function add_query_vars_filter($vars)
{
    $vars[] = "blog_cat";
    $vars[] = "blog_tags";
    return $vars;
}

//add_filter('query_vars', 'add_query_vars_filter');

function parse_date_range_query_string($query)
{

    $allTagsIds = get_terms([
        'taxonomy' => 'post_tag',
        'fields' => 'ids',
        'hide_empty' => false // need remove
    ]);


    $blogCats = (isset($_GET['category_name']) && $_GET['category_name']) ? implode(', ', $_GET['category_name']) : '';

    $blogTags = (isset($_GET['tag__in']) && $_GET['tag__in']) ? array_map('intval', explode(', ', implode(', ', $_GET['tag__in']))) : $allTagsIds;



    if (!is_admin() && $query->is_home() && $query->is_main_query()) {
        $query->set('category_name', $blogCats);
        $query->set('tag__in', $blogTags);

        if((isset($_GET['category_name']) && $_GET['category_name']) || (isset($_GET['tag__in']) && $_GET['tag__in'])){
            $query->set( 'ignore_sticky_posts', true );
        }else{
            $query->set( 'ignore_sticky_posts', true );
            $query->set( 'post__not_in', get_option('sticky_posts') );
        }
    }
}

add_filter('pre_get_posts', 'parse_date_range_query_string');