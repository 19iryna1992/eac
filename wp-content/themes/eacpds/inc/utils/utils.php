<?php

if (!defined('ABSPATH')) exit;

require_once 'helpers.php';
require_once 'hooks-head.php';
require_once 'hooks-footer.php';
require_once 'hooks-menu.php';
require_once 'formatting.php';
require_once 'colors.php';
require_once 'mix.php';
require_once 'svg-icons.php';
require_once 'block-filters.php';
require_once 'post-tag-hierarchical.php';
require_once 'pre-get-posts.php';
