<?php

if (!defined('ABSPATH')) exit;

/*------------------------------------*\
	Main Menu append mega menu
\*------------------------------------*/
add_filter('walker_nav_menu_start_el', 'append_mega_menu', 15, 2);
function append_mega_menu($item_output, $item) {
    $megaMenuType = get_field('mega_menu_types',$item);
    // if there's no content, just return the <a> directly
    if ( $megaMenuType === 'products_menu') {
        $mega_menu =  load_template_part('template-parts/components/mega-menus/mega-menu-products');
        return $item_output.$mega_menu;
    }

    if ( $megaMenuType === 'services_menu') {
        $mega_menu =  load_template_part('template-parts/components/mega-menus/mega-menu-services');
        return $item_output.$mega_menu;
    }

    if ( $megaMenuType === 'solutions_menu') {
        $mega_menu =  load_template_part('template-parts/components/mega-menus/mega-menu-solutions');
        return $item_output.$mega_menu;
    }

    if ( $megaMenuType === 'about_menu') {
        $mega_menu =  load_template_part('template-parts/components/mega-menus/mega-menu-about');
        return $item_output.$mega_menu;
    }

    if ( $megaMenuType === 'resources_menu') {
        $mega_menu =  load_template_part('template-parts/components/mega-menus/mega-menu-resources');
        return $item_output.$mega_menu;
    }

    return $item_output;
}


/*------------------------------------*\
	Menu additional class
\*------------------------------------*/

add_filter('wp_nav_menu_objects', 'eacpds_wp_nav_menu_objects', 10, 2);

function eacpds_wp_nav_menu_objects($items, $args)
{
    // loop
    foreach ($items as &$item) {
        // vars
        $megaMenuType = get_field('mega_menu_types',$item);
        if ($megaMenuType && $megaMenuType !== 'default_menu') {
            //add Icon and class
            $item->title = $item->title.'<span class="menu-item__icon JS--submenu-open"></span>';
            array_push($item->classes, 'JS--mega-menu-item has-mega-menu');
        }
    }
    // return
    return $items;
}


function load_template_part($template_name, $part_name = null)
{
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}