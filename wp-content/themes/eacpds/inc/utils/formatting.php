<?php

if (!defined('ABSPATH')) exit;

/**
 *  eacpds_line_wrap ()
 *  Gets line breaks from a field and wraps
 *  them in span or list.
 *
 * @param string $type Markup wrapping lines
 * @return  $output
 * @example
 *           eacpds_line_wrap($fieldname, 'span')
 */
function eacpds_line_wrap($textarea, $type = "list")
{
    $lines = explode("\n", $textarea);
    $output = '';

    if (!empty($lines)) {
        foreach ($lines as $line) {
            if ($type == 'list') {
                $output .= '<li>' . trim($line) . '</li>';
            } elseif ($type == 'span') {
                $output .= '<span>' . trim($line) . ' ' . '</span>';
            }
        }
    }
    return $output;
}


add_filter('navigation_markup_template', 'eacpds_navigation_template', 10, 2);
function eacpds_navigation_template($template, $class)
{

    return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}


/**
 *
 * Yoast Breadcrumbs filter
 *
 */

add_filter('wpseo_breadcrumb_links', 'eacpds_yoast_seo_breadcrumb');

function eacpds_yoast_seo_breadcrumb($links)
{
    $servicesArchive = get_field('services_link', 'option');
    $solutionsArchive = get_field('solutions_link', 'option');
    $careersArchive = get_field('careers_link', 'option');
    $productsArchive = get_field('products_link', 'option');
    global $post;

    if (is_singular('services') && $servicesArchive) {
        $breadcrumb = array(
            'url' => $servicesArchive['url'],
            'text' => $servicesArchive['title'],
        );

        array_unshift($links, $breadcrumb);
    }

    if (is_singular('solutions') && $solutionsArchive) {
        $breadcrumb = array(
            'url' => $solutionsArchive['url'],
            'text' => $solutionsArchive['title'],
        );

        array_unshift($links, $breadcrumb);
    }

    if (is_singular('careers') && $careersArchive) {
        $breadcrumb = array(
            'url' => $careersArchive['url'],
            'text' => $careersArchive['title'],
        );

        array_unshift($links, $breadcrumb);
    }

    if (is_singular('products') && $productsArchive) {
        $breadcrumb = array(
            'url' => $productsArchive['url'],
            'text' => $productsArchive['title'],
        );

        array_unshift($links, $breadcrumb);
    }


    return $links;
}

/**
 *
 * Custom breadcrumbs separator
 *
 */
function eacpds_breadcrumb_separator($breadcrumbs_sep)
{

    $breadcrumbs_sep = '<span class="inline-block mx-5"><svg width="8" height="13" viewBox="0 0 8 13" class="inline-block mb-2" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M5.48584 7.24506L0.781051 11.7866L2.03813 13L8 7.24506L2.03813 1.49011L0.78105 2.70356L5.48584 7.24506Z" fill="#ffffff"/>
</svg></span>';
    return $breadcrumbs_sep;
}

;

// add the filter
add_filter('wpseo_breadcrumb_separator', 'eacpds_breadcrumb_separator', 10, 1);