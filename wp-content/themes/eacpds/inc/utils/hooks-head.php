<?php

if (!defined('ABSPATH')) exit;

/**
 * Global Variables for Javascript stuff.
 */
add_action('wp_head', function () {
    global $wp_query;
    $json = array(
        'admin_ajax' => admin_url('admin-ajax.php'),
        'themeUrl' => get_bloginfo('template_directory'),
        'siteUrl' => get_home_url(),
        'page' => get_query_var('paged') ? get_query_var('paged') : 1,
        'max_page' => $wp_query->max_num_pages,
    );
    ?>
    <script>
        var appLocations = <?php echo json_encode($json, JSON_PRETTY_PRINT); ?>;
    </script>
    <?php
});


/**
 * Apply Global / Page Tracking To Header
 */
add_action('wp_head', function () {
    $global_tracking_meta = (class_exists('ACF')) ? get_field('global_tracking_metas', 'option') : null;
    $global_tracking_head = (class_exists('ACF')) ? get_field('global_tracking_head', 'option') : null;
    $drift_chat = (class_exists('ACF')) ? get_field('drift_chat_script', 'option') : null;

    if ($global_tracking_meta) echo $global_tracking_meta;
    if ($global_tracking_head) echo $global_tracking_head;
    if ($drift_chat) echo $drift_chat;
});

add_action('wp_body_open', function () {
    $global_tracking_body = (class_exists('ACF')) ? get_field('global_tracking_body', 'option') : null;
    if ($global_tracking_body) echo $global_tracking_body;
});
