<?php

if (!defined('ABSPATH')) exit;


/**
 * Disable Font Size selection
 */
add_theme_support('disable-custom-font-sizes');
add_theme_support('disable-custom-colors');
add_theme_support('editor-color-palette');

/**
 * Remove core block patterns
 */
remove_theme_support('core-block-patterns');

/**
 * Custom Theme Colors
 */
add_theme_support('editor-color-palette', [
    [
        'name' => 'White',
        'slug' => 'white',
        'color' => '#fff',
    ],
]);

/**
 * Custom font sizes
 */
add_theme_support(
    'editor-font-sizes',
    [
        [
            'name' => __('Medium', 'eacpds'),
            'shortName' => __('M', 'eacpds'),
            'size' => 20,
            'slug' => 'medium'
        ],
        [
            'name' => __('Large', 'eacpds'),
            'shortName' => __('L', 'eacpds'),
            'size' => 24,
            'slug' => 'large'
        ],
        [
            'name' => __('Huge', 'eacpds'),
            'shortName' => __('XL', 'eacpds'),
            'size' => 36,
            'slug' => 'huge'
        ],
    ]
);


/**
 * Whitelist of blocks
 */
add_filter('allowed_block_types_all', function ($allowed_block_types, $post) {

    return [
        //'acf/inner-block',
        'core/paragraph',
        'core/heading',
        'core/block',
        'acf/faqs',
        'acf/expand-collapse',
        'acf/interior-page-header',
        'acf/slideshow-header',
        'acf/logos',
        'acf/featured-text',
        'acf/icon-columns',
        'acf/callouts',
        'acf/icon-columns-grid',
        'acf/text-image',
        'acf/text-image-links',
        'acf/timeline-slideshow',
        'acf/statistics',
        'acf/offerings-animated-slideshow',
        'acf/team-spotlight',
        'acf/featured-content-tab-cards',
        'acf/spacer',
        'acf/testimonials',
        'acf/more-section-navigation',
        'acf/related-news-articles',
        'acf/post-types-tabs',
        'acf/newsletter-signup',
        'acf/contact-form',
        'acf/live-chat-cta',
        'acf/blog-subscription',
        'acf/image-grid',
        'acf/careers',
        'acf/featured-products',
        'acf/events-list',
        'acf/product-trials-list',
        'acf/training-courses',
        'acf/featured-video',
        'acf/table',
        'acf/table-accordion',
        'acf/table-headers',
        'acf/share-icons',
        'acf/post-author',
        'acf/simple-text',
        'acf/buttons',
        'acf/careers-filter',
    ];
}, 99, 2);

/**
 * Wrap in container class
 */
add_filter('render_block', function ($block_content, $block) {
    $containerised_blocks = ['core/paragraph', 'core/heading'];
    if (in_array($block['blockName'], $containerised_blocks)) {
        $content = '<div class="container">';
        $content .= '<div class="c-wysiwyg">';
        $content .= $block_content;
        $content .= '</div>';
        $content .= '</div>';
        return $content;
    }

    return $block_content;
}, 10, 2);
