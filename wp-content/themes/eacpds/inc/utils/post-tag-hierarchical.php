<?php

if (!defined('ABSPATH')) exit;

function wd_hierarchical_tags_register() {

    // Maintain the built-in rewrite functionality of WordPress tags

    global $wp_rewrite;

    $rewrite =  array(
        'hierarchical'              => false, // Maintains tag permalink structure
        'slug'                      => get_option('tag_base') ? get_option('tag_base') : 'tag',
        'with_front'                => ! get_option('tag_base') || $wp_rewrite->using_index_permalinks(),
        'ep_mask'                   => EP_TAGS,
    );

    // Redefine tag labels (or leave them the same)

    $labels = array(
        'name'                       => _x( 'Tags', 'Taxonomy General Name', 'eacpds' ),
        'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'eacpds' ),
        'menu_name'                  => __( 'Tags', 'eacpds' ),
        'all_items'                  => __( 'All Tags', 'eacpds' ),
        'parent_item'                => __( 'Parent Tag', 'eacpds' ),
        'parent_item_colon'          => __( 'Parent Tag:', 'eacpds' ),
        'new_item_name'              => __( 'New Tag Name', 'eacpds' ),
        'add_new_item'               => __( 'Add New Tag', 'eacpds' ),
        'edit_item'                  => __( 'Edit Tag', 'eacpds' ),
        'update_item'                => __( 'Update Tag', 'eacpds' ),
        'view_item'                  => __( 'View Tag', 'eacpds' ),
        'separate_items_with_commas' => __( 'Separate tags with commas', 'eacpds' ),
        'add_or_remove_items'        => __( 'Add or remove tags', 'eacpds' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'eacpds' ),
        'popular_items'              => __( 'Popular Tags', 'eacpds' ),
        'search_items'               => __( 'Search Tags', 'eacpds' ),
        'not_found'                  => __( 'Not Found', 'eacpds' ),
    );

    // Override structure of built-in WordPress tags

    register_taxonomy( 'post_tag', 'post', array(
        'hierarchical'              => true, // Was false, now set to true
        'query_var'                 => 'tag',
        'show_in_rest' => true,
        'labels'                    => $labels,
        'rewrite'                   => $rewrite,
        'public'                    => true,
        'show_ui'                   => true,
        'show_admin_column'         => true,
        '_builtin'                  => true,
    ) );

}
add_action('init', 'wd_hierarchical_tags_register');