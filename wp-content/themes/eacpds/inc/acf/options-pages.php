<?php

if (!defined('ABSPATH')) exit;

/**
 * Create Global Elements Menu Item
 */
if (function_exists('acf_add_options_page')) {

	# Site Globals (Parent)
	$site_globals = acf_add_options_page(array(
		'page_title' => 'Theme Settings',
		'menu_title' => 'Theme Settings',
		'icon_url' => 'dashicons-admin-generic',
		'redirect' => true
	));

    # Menu settings
    $page_main_menu = acf_add_options_page(array(
        'page_title' => 'Main menu',
        'menu_title' => 'Main menu',
        'menu_slug' => 'main_menu',
        'parent_slug' => $site_globals['menu_slug'],
    ));

    # Gutenberg blocks additional settings
    $page_blocks = acf_add_options_page(array(
        'page_title' => 'Blocks settings',
        'menu_title' => 'Blocks settings',
        'menu_slug' => 'block_settings',
        'parent_slug' => $site_globals['menu_slug'],
    ));

    # Archive pages links
    $page_archives_links = acf_add_options_page(array(
        'page_title' => 'Archive pages links',
        'menu_title' => 'Archive pages links',
        'menu_slug' => 'archive_page_links',
        'parent_slug' => $site_globals['menu_slug'],
    ));

    # Drift Chat
    $page_drift_chat = acf_add_options_page(array(
        'page_title' => 'Drift Chat Code',
        'menu_title' => 'Drift Chat Code',
        'menu_slug' => 'drift_chat_code',
        'parent_slug' => $site_globals['menu_slug'],
    ));

	# JS Tracking
	$page_tracking_scripts = acf_add_options_page(array(
		'page_title' => 'Tracking Scripts',
		'menu_title' => 'Tracking Scripts',
		'menu_slug' => 'tracking',
		'parent_slug' => $site_globals['menu_slug'],
	));
}
