<?php

/**
 *  Admin Dash View
 *
 *
 *  @version    1.0
 *  @see        admin-dash.php
 *  @see        admin/admin-theme/assets (for styles)
 */

if (!defined('ABSPATH')) exit;

# Wp admin bootstrap
require_once(ABSPATH . 'wp-load.php');
require_once(ABSPATH . 'wp-admin/admin.php');
require_once(ABSPATH . 'wp-admin/admin-header.php');

?>

<section class="dash">

  <header class="dash-header">
    <h1 class="dash-header__title"><?php _e('Welcome to your Site', 'eacpds'); ?></h1>
    <p class="dash-header__text"><?php _e('From here you can create and manage the font-end experience.', 'eacpds'); ?></p>
  </header>

  <section class="dash-cards">

    <article class="dash-card">
      <a class="dash-card__link" href="<?php echo admin_url('edit.php?post_type=page'); ?>">
        <div class="dash-card__content">
          <i class="dash-card__icon icon-file-empty"></i>

          <h3 class="dash-card__title"><?php _e('Manage Pages', 'eacpds'); ?></h3>

          <p class="dash-card__text"><?php _e('Add new pages, or manage editing', 'eacpds'); ?></p>
        </div>
      </a>
    </article>

    <article class="dash-card">
      <a class="dash-card__link" href="<?php echo admin_url('edit.php'); ?>">
        <div class="dash-card__content">
          <i class="dash-card__icon icon-tag"></i>

          <h3 class="dash-card__title"><?php _e('Articles', 'eacpds'); ?></h3>

          <p class="dash-card__text"><?php _e('Add new posts / news stories', 'eacpds'); ?></p>
        </div>
      </a>
    </article>

    <article class="dash-card">
      <a class="dash-card__link" href="<?php echo admin_url('nav-menus.php'); ?>">
        <div class="dash-card__content">
          <i class="dash-card__icon icon-link"></i>
          <h3 class="dash-card__title"><?php _e('Menus', 'eacpds'); ?></h3>
          <p class="dash-card__text"><?php _e('Edit menus', 'eacpds'); ?></p>
        </div>
      </a>
    </article>

    <article class="dash-card">
      <a class="dash-card__link" href="<?php echo admin_url('options-general.php'); ?>">
        <div class="dash-card__content">
          <i class="dash-card__icon icon-cog"></i>
          <h3 class="dash-card__title"><?php _e('Settings', 'eacpds'); ?></h3>
          <p class="dash-card__text"><?php _e('Edit settings', 'eacpds'); ?></p>
        </div>
      </a>
    </article>

  </section>
</section>
<?php
