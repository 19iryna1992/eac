<?php


namespace Inc\Taxonomies;


use Illuminate\Support\Str;
use Inc\Traits\Singleton;

class Taxonomy
{
    use Singleton;
    /**
     * The taxonomy name
     *
     * @var
     */
    public $name;

    /**
     * The taxonomy attributes
     *
     * @var array
     */
    public $attributes = [
        'public' => true,
        'show_in_rest' => true,
        'show_ui' => true,
        'show_admin_column' => true
    ];

    /**
     * Taxonomy constructor.
     *
     * @param  array  $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge($this->attributes, [
            'label' => __(Str::title(str_replace('_', ' ', $this->name)), 'eacpds')
        ]);

        if (!empty($args)) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}
