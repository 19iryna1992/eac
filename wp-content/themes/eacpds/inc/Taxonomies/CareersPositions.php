<?php


namespace Inc\Taxonomies;


class CareersPositions extends Taxonomy
{
    /**
     * The taxonomy name
     *
     * @var string
     */
    public $name = 'positions';

    /**
     * The taxonomy attributes
     *
     * @var array
     */
    public $attributes = [
        'public' => true,
        'show_in_rest' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var'    => true,
        'hierarchical' => true,
        'rewrite' => [
            'slug' => 'positions',
            'with_front' => true
        ]
    ];
}