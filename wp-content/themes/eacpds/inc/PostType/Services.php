<?php


namespace Inc\PostType;


class Services extends PostType
{
    const NAME = 'services';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'menu_icon'     => 'dashicons-admin-generic',
        'supports' => ['title', 'editor', 'thumbnail','excerpt', 'revisions'],
        'rewrite' => [
            'slug' => 'services',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Service', 'Services')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}