<?php


namespace Inc\PostType;


use Inc\Taxonomies\CareersLocation;
use Inc\Taxonomies\CareersPositions;

class Careers extends PostType
{
    const NAME = 'careers';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-businessman',
        'supports' => ['title', 'editor', 'thumbnail', 'excerpt', 'revisions'],
        'rewrite' => [
            'slug' => 'careers',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [
        CareersLocation::class,
        CareersPositions::class
];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Career', 'Careers')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}