<?php


namespace Inc\PostType;


class Solutions extends PostType
{
    const NAME = 'solutions';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'menu_icon'     => 'dashicons-cloud-saved',
        'supports' => ['title', 'editor', 'thumbnail','excerpt', 'revisions'],
        'rewrite' => [
            'slug' => 'solutions',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Solution', 'Solutions')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}