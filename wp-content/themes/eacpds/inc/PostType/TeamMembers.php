<?php


namespace Inc\PostType;


class TeamMembers extends PostType
{
    const NAME = 'members';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-buddicons-buddypress-logo',
        'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
        'rewrite' => [
            'slug' => 'members',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Team Member', 'Team Members')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}