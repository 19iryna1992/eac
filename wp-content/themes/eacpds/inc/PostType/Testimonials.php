<?php


namespace Inc\PostType;


class Testimonials extends PostType
{
    const NAME = 'testimonials';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-format-status',
        'supports' => ['title', 'thumbnail', 'revisions'],
        'rewrite' => [
            'slug' => 'testimonials',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Testimonial', 'Testimonials')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}