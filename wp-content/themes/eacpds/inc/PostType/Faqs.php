<?php

namespace Inc\PostType;

class Faqs extends PostType
{
    const NAME = 'faqs';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'query_var' => true,
        'menu_icon'     => 'dashicons-editor-help',
        'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
        'rewrite' => [
            'slug' => 'faqs',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('FAQ', 'FAQs')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}