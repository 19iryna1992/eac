<?php


namespace Inc\PostType;


class Products extends PostType
{
    const NAME = 'products';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'menu_icon'     => 'dashicons-cart',
        'supports' => ['title', 'editor', 'thumbnail','excerpt', 'revisions'],
        'rewrite' => [
            'slug' => 'products',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Product', 'Products')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}