<?php


namespace Inc\PostType;


use Inc\Traits\Singleton;

class PostType
{
    use Singleton;

    /**
     * The post type name
     **/
    const NAME = '';

    /**
     * The post type attributes
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The taxonomies to be registered
     *
     * @var array
     */
    protected $taxonomies = [];

    public function setLabels($singular = 'Example', $plural = 'Examples')
    {
        $p_lower = strtolower($plural);
        $s_lower = strtolower($singular);
        return [
            'name' => $plural,
            'singular_name' => $singular,
            'add_new_item' => "New $singular",
            'edit_item' => "Edit $singular",
            'view_item' => "View $singular",
            'view_items' => "View $plural",
            'search_items' => "Search $plural",
            'not_found' => "No $p_lower found",
            'not_found_in_trash' => "No $p_lower found in trash",
            'parent_item_colon' => "Parent $singular",
            'all_items' => "All $plural",
            'archives' => "$singular Archives",
            'attributes' => "$singular Attributes",
            'insert_into_item' => "Insert into $s_lower",
            'uploaded_to_this_item' => "Uploaded to this $s_lower",
        ];
    }

    /**
     * Registers a post type
     *
     * @return \WP_Post_Type
     * @throws \Exception
     */
    public function register(): \WP_Post_Type
    {
        if (empty((string)self::getName())) {
            throw new \Exception('Either remove the register method or add the post type name!');
        }

        if (!empty((array)$this->taxonomies)) {
            collect($this->taxonomies)->each(function (string $taxonomy) {
                $tax = new $taxonomy;
                register_taxonomy((string)$tax->name, (string)self::getName(), (array)$tax->attributes);
            });
        }

        $post_type = register_post_type((string)self::getName(), (array)$this->attributes);
        return $post_type;
    }

    public static function getName()
    {
        return static::NAME;
    }
}
