<?php


namespace Inc\PostType;


use Inc\Taxonomies\TrainingCalendarCategory;

class TrainingCalendar extends PostType
{
    const NAME = 'training-calendar';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-calendar-alt',
        'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
        'rewrite' => [
            'slug' => 'training-calendar',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [
        TrainingCalendarCategory::class
];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Training Calendar', 'Training Calendar')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}