<?php

namespace Inc\PostType;

class Events extends PostType
{
    const NAME = 'events';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $attributes = [
        'has_archive' => false,
        'hierarchical' => false,
        'public' => true,
        'query_var' => true,
        'menu_icon'     => 'dashicons-tickets-alt',
        'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
        'rewrite' => [
            'slug' => 'events',
            'with_front' => false
        ]
    ];

    /**
     * Taxonomies
     *
     * @var array
     */
    protected $taxonomies = [

    ];

    /**
     * Course constructor.
     *
     * @param array $args
     */
    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Event', 'Events')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}