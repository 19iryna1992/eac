<?php
add_action('wp_ajax_career_posts', 'filter_career_posts');
add_action('wp_ajax_nopriv_career_posts', 'filter_career_posts');
function filter_career_posts()
{
    global $post;

    $posts_offset = filter_input(INPUT_POST, 'offset', FILTER_VALIDATE_INT);
    $locations = (isset($_POST['locations'])) ? $_POST['locations'] : '';
    $positions = (isset($_POST['positions'])) ? $_POST['positions'] : '';


    $post_type = 'careers';
    $count_careers = wp_count_posts('careers');
    $posts_per_page = 8;
    $tax_query = array('relation' => 'AND');

    if($locations){
        $tax_query[] = array(
            array(
                'taxonomy' => 'locations',
                'field' => 'id',
                'terms' => $locations,
                'operator' => 'IN'
            )
        );
    }

    if($positions){
        $tax_query[] = array(
            array(
                'taxonomy' => 'positions',
                'field' => 'id',
                'terms' => $positions,
                'operator' => 'IN'
            )
        );
    }


    $careers_args = array(
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => $posts_per_page,
        'offset' => $posts_offset,
        'no_found_rows' => true
    );

    if($locations || $positions){
        $careers_posts = new WP_Query($args = array_merge($careers_args, array('tax_query' => $tax_query)));
        $total_posts = $careers_posts->post_count;
    }else{
        $careers_posts = new WP_Query($careers_args);
        $total_posts = $count_careers->publish;
    }

    //$total_posts = $count_careers->publish;
    $load_button = ($total_posts > $posts_offset + $posts_per_page);



    if ($careers_posts->have_posts()) :
        ob_start();
        while ($careers_posts->have_posts()) : $careers_posts->the_post();
            get_template_part('template-parts/components/career-card');
        endwhile;
        $result = ob_get_contents();
        ob_end_clean();
        wp_send_json_success(['content' => $result, 'total_posts' => $total_posts, 'load_button' => $load_button]);
        wp_reset_postdata();
    else :
        wp_send_json_error();
    endif;
    die();
}