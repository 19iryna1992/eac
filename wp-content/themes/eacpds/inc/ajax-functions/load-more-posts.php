<?php

add_action('wp_ajax_load_more_posts', 'load_more_posts');
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');

function load_more_posts()
{
    $post_type = $_POST['post_type'];

    $args = array(
        'post_type' => array($post_type),
        'posts_per_page' => 4,
        'paged' => $_POST['page'] + 1,
    );

    if ($post_type == 'events'):
        $today = date('Ymd');
        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => 4,
            'meta_key' => 'event_date',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'event_date',
                    'compare' => '>=',
                    'value' => $today,
                )
            ),
            'paged' => $_POST['page'] + 1,
        );

    endif;

    $query = new WP_Query($args);

    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post(); ?>
            <div class=" JS--posts--item">
                <?php get_template_part('template-parts/components/event-card') ?>
            </div>
        <?php endwhile;
        wp_reset_postdata();
    endif;
    wp_die();

}




