<?php
if (!defined('ABSPATH')) exit;
/**
 * Change the text for the character limit.
 *
 * @link https://wpforms.com/developers/how-to-change-the-limit-character-validation-text/
 *
 */
function wpf_dev_frontend_words( $strings ) {

    // val_limit_words when using words
    // val_limit_characters when using characters

    $strings['val_limit_words'] = '{count}/{limit}';
    return $strings;
}
add_filter( 'wpforms_frontend_strings' , 'wpf_dev_frontend_words', 10, 1 );

/**
 * Change the text for the character limit.
 *
 * @link https://wpforms.com/developers/how-to-change-the-limit-character-validation-text/
 *
 */
function wpf_dev_frontend_character( $strings ) {

    // val_limit_words when using words
    // val_limit_characters when using characters

    $strings['val_limit_characters'] = '{count}/{limit}';
    return $strings;
}
add_filter( 'wpforms_frontend_strings' , 'wpf_dev_frontend_character', 10, 1 );