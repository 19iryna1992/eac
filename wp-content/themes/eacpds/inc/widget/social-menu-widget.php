<?php
function social_menu_load_widget()
{
    register_widget('SocialMenuWidget');
}

add_action('widgets_init', 'social_menu_load_widget');

class SocialMenuWidget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            'social_menu_widget',

            __('Socials Menu Widget', 'widget'),

            array('description' => __('Socials Menu Widget', 'widget'),)
        );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        $widget_id = 'widget_' . $args['widget_id'];

        echo $args['before_widget'];
        if (!empty($title))

            ?>


        <?php if (have_rows('socials_list', $widget_id)):
        $title = get_field('main_title', $widget_id);
        ?>
        <div class="flex flex-wrap">
            <h6 class="font-roboto font-bold text-h6 tracking-sm lg:mt-5 text-white mr-60 mobile-lg:mb-30"><?php echo $title; ?></h6>
            <ul class="flex items-center space-x-40">
                <?php while (have_rows('socials_list', $widget_id)) : the_row();
                    $soc_title = get_sub_field('icon_title', $widget_id);
                    $soc_icon = get_sub_field('icon', $widget_id);
                    $soc_link = get_sub_field('link', $widget_id);
                    ?>

                    <li class="c-socials-menu__item">
                        <a href="<?php echo $soc_link; ?>" class="inline-block hover:opacity-70" target="_blank">
                            <?php echo wp_get_attachment_image($soc_icon, 'full', false, ['class' => 'mx-auto min-h-30']) ?>
                            <small class="block font-roboto font-mediumtext-extra-xs leading-16 mt-10"><?php echo $soc_title; ?></small>
                        </a>
                    </li>

                <?php endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>


        <?php echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            // $title = $instance['title'];
        } else {
            // $title = __('New title', 'wpb_widget_domain');
        }

        ?>

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}

