<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 */

get_header();

?>

<main id="main" role="main" tabindex="-1">

    <?php if (have_posts()) : ?>
        <div class="container">
            <h1 class="pb-20"><?php printf(esc_html__( 'Results for "%s"', 'eacpds' ), '<span class="search-term text-blue-dark">' . esc_html( get_search_query() ) . '</span>'); ?></h1>
            <?php while (have_posts()) :the_post(); ?>

                <article>
                    <div class="c-wysiwyg"> 
                        <h2 class="text-blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <div>
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                </article>

            <?php endwhile; ?>
        </div>

    <?php else : ?>
        <div class="container">
            <div class="c-wysiwyg"> 
                <h2>Nothing Found</h2>
            </div>
        </div>
    <?php endif; ?>


</main><!-- #main -->

<?php get_footer(); ?>
