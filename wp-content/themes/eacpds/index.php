<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package eacpds
 */

if (!defined('ABSPATH')) exit;

get_header();

?>
    <main id="main" role="main" tabindex="-1">
        <div class="JS-blog-filter-overlay"></div>
        <?php get_template_part('template-parts/components/resources-filter/filter'); ?>
        <?php if ((isset($_GET['category_name']) && $_GET['category_name']) || (isset($_GET['tag__in']) && $_GET['tag__in'])) : ?>
        <?php else : ?>
            <?php get_template_part('template-parts/components/resources-filter/sticky-posts'); ?>
        <?php endif; ?>
        <?php if (have_posts()) : ?>
            <div class="my-40">
                <div class="container">
                    <div class="flex flex-wrap -mx-20">
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="mb-40 sm:mb-10 md:mb-0 px-10 md:p-20 w-full sm:w-1/2 lg:w-1/3">
                                <?php get_template_part('template-parts/components/resources-post/regular'); ?>
                            </div>
                        <?php endwhile; // End of the loop.?>
                    </div>
                    <div class="my-40 md:mt-50 md:mb-100 text-center">
                        <?php the_posts_pagination([
                            'prev_text' => __('Prev', 'eacpds'),
                        ]); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </main>
<?php get_footer(); ?>