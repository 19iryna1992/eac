// webpack.mix.js

let mix = require('laravel-mix');
const local = 'http://eacpds.local.com';

mix.js('src/js/app.js', 'js')
    .js('src/js/editor.js', 'js')
    .sass('src/scss/style.scss', 'css')
    .sass('src/scss/editor.scss', 'css')
    .setPublicPath('./dist')
    .options({
        processCssUrls: false,
        postCss: [
            require('tailwindcss')
        ],
    })
    .browserSync({
        proxy: local,
        injectChanges: true,
        port: 8001,
        files: [
            './dist/css/*.css',
            './dist/js/*.js',
            './*.php',
            './templates/*.php',
            './template-parts/**/*.php',
            './template-parts/**/**/*.php',
        ],
    })
    .autoload({
        jquery: ['$', 'window.jQuery']
    })
    .version();