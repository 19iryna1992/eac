<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package eacpds
 */

if (!defined('ABSPATH')) exit;

get_header(); ?>


    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/components/navigation/breadcrumbs'); ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>

    </main>


<?php get_footer(); ?>