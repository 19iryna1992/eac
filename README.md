# Project setup
- *Note:* This theme requires ACF PRO Plugin 5.9.*
- *Note:* Move .gitignore file to the core project folder
- *Note:* This theme use StoutLogic/acf-builder, fields examples you can find <a href="https://github.com/StoutLogic/acf-builder/wiki/field-types" target="_blank">here</a>

## Installation
- *Note:* All commands described below must be run in the theme folder
- Open style.css and replace names of theme, prefixes, domains in files of theme (Ctrl+Shift+F in VS Code find and replace)
 ### Install composer
  ```
   composer install
  ```
### Install npm packages
  ```
   npm install
  ```
  - *Note:* Make sure your terminal is located on the root of the theme before calling install
- `npm run watch` (for watchers)

## Project deployment
- `npm run prod`